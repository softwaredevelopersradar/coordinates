﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibraryPeleng
{
    public class Peleng
    {
// ***********************************************************************
// Расчет траектории по пеленгу
// Нахождение ИРИ методом триангуляции
// ***********************************************************************

// ПЕРЕМЕННЫЕ VARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVAR

// Пеленг PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP
	   // Флаг НУ
 	   private int fl_first_Pel;
      
	   // Вспомогательные флаги
	   private int fl1_Pel;

	   // Индекс фиктивной точки (1,2, ...)
       private uint IndFP_Pel;
	   // Индекс i-й фиктивной точки в массивах(0,...)
       private uint IndMas_Pel; 

       // Расстояние по поверхности земли от точки стояния пеленгатора до текущей 
       // фиктивной точки 
       private double Mi_Pel;
       // Расстояние по прямой от точки стояния пеленгатора до пересечения прямой
       // (проходящей через текущую фиктивную точку и начало координат)и плоскости,
       // касательной в точке стояния пеленгатора
       private double Di_Pel;

	   // Радиус Земли (шар)
	   private double REarth_Pel;
	   // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
	   private double REarthP_Pel;
	   // Радиус Земли  в (i-1)-й фиктивной точке с учетом текущей широты 
	   //double REarthF_Pel;

	   // Координаты фиктивной точки в СК пеленгатора (СКП)
	   private double Xi_Pel;
	   private double Yi_Pel;
	   private double Zi_Pel;

	   // Координаты фиктивной точки в СК1 (Поворот СКП на угол=широте пеленгатора)
	   private double Xi0_Pel;
	   private double Yi0_Pel;
	   private double Zi0_Pel;

	   // Координаты фиктивной точки в опорной геоцентрической СК
	   // (Поворот СК1 на угол=долготе пеленгатора)
	   private double XiG_Pel;
	   private double YiG_Pel;
	   private double ZiG_Pel;

	   // Угловые координаты фиктивной точки в опорной геоцентрической СК
	   private double Ri_Pel;
	   private double LATi_Pel;
	   private double LONGi_Pel;

       // Координаты точки пересечения прямой (проходящей через текущую фиктивную
	   // точку и начало координат) и плоскости касательной в точке стояния 
	   // пеленгатора в опорной геоцентрической СК
	   private double XiG1_Pel;
	   private double YiG1_Pel;
	   private double ZiG1_Pel;
	   private double Ri1_Pel;

// PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP Пеленг

// Триангуляция TriangTriangTriangTriangTriangTriangTriangTriangTriangTrian
	   // Флаг НУ
 	   private int fl_first_Tr;

	   // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
	   private double REarthP1_Tr;
	   private double REarthP2_Tr;

      // Расчетные коэффициенты
      private double K1_Tr;
      private double K2_Tr;
      private double K3_Tr;

	  // Коэффициенты канонического уравнения для плоскости пеленгования ИРИ
	  // i-ым пеленгатором 
	  private double A1_Tr; // Для пеленгатора1
	  private double T1_Tr;
	  private double C1_Tr;
	  private double A2_Tr; // Для пеленгатора2
	  private double T2_Tr;
	  private double C2_Tr;

	  // Координаты искомого ИРИ в опорной геоцентрической СК
      private double XIRI1_Tr;
      private double XIRI2_Tr;
      private double YIRI1_Tr;
      private double YIRI2_Tr;
      private double ZIRI1_Tr;
      private double ZIRI2_Tr;

      // Координаты середины отрезка, соединяющего N-ые фиктивные точки
	  private double XM_Tr;
	  private double YM_Tr;
	  private double ZM_Tr;

      // Расстояние до ПП
	  private double L1_Tr;
	  private double L2_Tr;

// TriangTriangTriangTriangTriangTriangTriangTriangTriangTrian Триангуляция

// VARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVAR ПЕРЕМЕННЫЕ

// Конструктор ***********************************************************
// Конструктор

 public Peleng()
 {

// Пеленг PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP
// Флаг НУ
 	   fl_first_Pel=0;

   	   // Вспомогательные флаги
	   fl1_Pel=0;

	   // Индекс фиктивной точки (1,2, ...)
       IndFP_Pel=1;
	   // Индекс i-й фиктивной точки в массивах(0,...)
       IndMas_Pel=0; 

       // Расстояние по поверхности земли от точки стояния пеленгатора до текущей 
       // фиктивной точки 
       Mi_Pel=0;
       // Расстояние по прямой от точки стояния пеленгатора до пересечения прямой
       // (проходящей через текущую фиктивную точку и начало координат)и плоскости,
       // касательной в точке стояния пеленгатора
       Di_Pel=0;

	   // Радиус Земли (шар)
	   REarth_Pel=6378245;
	   // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
	   REarthP_Pel=0;
	   // Радиус Земли  в (i-1)-й фиктивной точке с учетом текущей широты 
	   //REarthF_Pel=0;

	   // Координаты фиктивной точки в СК пеленгатора
	   Xi_Pel=0;
	   Yi_Pel=0;
	   Zi_Pel=0;

	   // Координаты фиктивной точки в СК1 (Поворот СКП на угол=широте пеленгатора)
	   Xi0_Pel=0;
	   Yi0_Pel=0;
	   Zi0_Pel=0;

	   // Координаты фиктивной точки в опорной геоцентрической СК
	   // (Поворот СК1 на угол=долготе пеленгатора)
	   XiG_Pel=0;
	   YiG_Pel=0;
	   ZiG_Pel=0;

	   // Угловые координаты фиктивной точки в опорной геоцентрической СК
	   Ri_Pel=0;
	   LATi_Pel=0;
	   LONGi_Pel=0;

       // Координаты точки пересечения прямой (проходящей через текущую фиктивную
	   // точку и начало координат) и плоскости касательной в точке стояния 
	   // пеленгатора в опорной геоцентрической СК
	   XiG1_Pel=0;
	   YiG1_Pel=0;
	   ZiG1_Pel=0;
	   Ri1_Pel=0;

// PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP Пеленг

// Триангуляция TriangTriangTriangTriangTriangTriangTriangTriangTriangTrian
       // Флаг НУ
 	   fl_first_Tr=0;

	   // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
	   REarthP1_Tr=0;
	   REarthP2_Tr=0;

      // Расчетные коэффициенты
      K1_Tr=0;
      K2_Tr=0;
      K3_Tr=0;

	  // Коэффициенты канонического уравнения для плоскости пеленгования ИРИ
	  // i-ым пеленгатором 
	  A1_Tr=0; // Для пеленгатора1
	  T1_Tr=0;
	  C1_Tr=0;
	  A2_Tr=0; // Для пеленгатора2
	  T2_Tr=0;
	  C2_Tr=0;

	  // Координаты искомого ИРИ в опорной геоцентрической СК
      XIRI1_Tr=0;
      XIRI2_Tr=0;
      YIRI1_Tr=0;
      YIRI2_Tr=0;
      ZIRI1_Tr=0;
      ZIRI2_Tr=0;

      // Координаты середины отрезка, соединяющего N-ые фиктивные точки
	  XM_Tr=0;
	  YM_Tr=0;
	  ZM_Tr=0;

	  L1_Tr=0;
	  L2_Tr=0;

// TriangTriangTriangTriangTriangTriangTriangTriangTriangTrian Триангуляция

 } // Конструктор Peleng
// *********************************************************** Конструктор

// ***********************************************************************
    // Module
// ***********************************************************************
    public double module(double a)                   
     { 
        if (a >= 0) return (a);
        else        return (-a);
     }
// ***********************************************************************

// ***********************************************************************
// Определение координаты середины отрезка
// ***********************************************************************

 public void DefMiddle_Pel(
                     double X1,
                     double X2,
                     ref double XM

		                   )
	{

    // -------------------------------------------------------------------
    if(X1==0)
	    XM = X2/2;
    // -------------------------------------------------------------------
    else if(X2==0)
	    XM = X1/2;
    // -------------------------------------------------------------------
    else if((X1>0)&&(X2>0))
	{
      if(X2>(X1))
	    XM = X1+((X2-(X1))/2);
	  else if(X1>(X2))
	    XM = X2+((X1-(X2))/2);
	  else
	    XM = X1/2;
	}
    // -------------------------------------------------------------------
    else if((X1<0)&&(X2<0))
	{
      if(X2<(X1))
	    XM = X1-((module(X2)-module(X1))/2);
	  else if(X1<(X2))
	    XM = X2-((module(X1)-module(X2))/2);
	  else
	    XM = X1/2;
	}
    // -------------------------------------------------------------------
    else if((X1>0)&&(X2<0))
	{
      if(module(X1)>module(X2))
	    XM = (module(X2)+module(X1))/2;
	  else if(module(X1)<module(X2))
	    XM = -(module(X2)+module(X1))/2;
	  else
	    XM = 0;
	}
    // -------------------------------------------------------------------
    else 
	{
      if(module(X2)>module(X1))
	    XM = (module(X2)+module(X1))/2;
	  else if(module(X2)<module(X1))
	    XM = -(module(X2)+module(X1))/2;
	  else
	    XM = 0;
	}
    // -------------------------------------------------------------------

  } // Функция DefMiddle_Pel
// ***********************************************************************

// ***********************************************************************
// Отрисовка траектории N фиктивных точек по пеленгу
// Input: Theta_Pel,Mmax_Pel,NumberFikt_Pel
// Output: mas_Pel
// ***********************************************************************
    public void f_Peleng
        (

		     // Пеленг
             double Theta_Pel,     
			 // Max дальность отображения пеленга
             double Mmax_Pel,     
			 // Количество фиктивных точек в плоскости пеленгования пеленгатора
			 uint NumbFikt_Pel,
			 // Широта и долгота стояния пеленгатора
			 double LatP_Pel,
			 double LongP_Pel,

			 // Координаты пеленгатора в опорной геоцентрической СК
			 ref double XP_Pel,
             ref double YP_Pel,
             ref double ZP_Pel,

             // Координаты N-й фиктивной точки в опорной геоцентрической СК
             ref double XFN_Pel,
             ref double YFN_Pel,
             ref double ZFN_Pel,

			// Координаты фиктивных точек
             ref double [] mas_Pel,        
             ref double [] mas_Pel_XYZ        

			 // ForOtl
			 // Time of model
			 //double *pT,
			 // Discrete of time
	         //double *pdt,
             //double *pmas_Otl_Pel        


        )
    {


// Initial conditions ****************************************************
// Initial conditions

     if(fl_first_Pel==0)
      {

        // ...............................................................
	    // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 

        //REarthP_Pel = REarth_Pel*(1.-0.003352*sin(*pLatP_Pel)*sin(*pLatP_Pel));
        REarthP_Pel = REarth_Pel; // Шар
        // ...............................................................

        fl_first_Pel = 1;
        // ...............................................................

       // Координаты пеленгатора в опорной геоцентрической СК
        XP_Pel = REarthP_Pel * Math.Cos(LatP_Pel) * Math.Cos(LongP_Pel);
        YP_Pel = REarthP_Pel * Math.Cos(LatP_Pel) * Math.Sin(LongP_Pel);
        ZP_Pel = REarthP_Pel * Math.Sin(LatP_Pel);

      }
// **************************************************** Initial conditions

while(IndMas_Pel<(NumbFikt_Pel))
{

// Mi ********************************************************************
// Расстояние по поверхности земли от точки стояния пеленгатора до текущей 
// фиктивной точки (Mi-дуга болшьшого круга с центральным углом Mi/Rз радиан)

Mi_Pel = (((double)IndFP_Pel)*Mmax_Pel)/((double)(NumbFikt_Pel)); 
// ******************************************************************** Mi

// Di ********************************************************************
// Расстояние по прямой от точки стояния пеленгатора до пересечения прямой
// (проходящей через текущую фиктивную точку и начало координат)и плоскости,
// касательной в точке стояния пеленгатора

Di_Pel = REarthP_Pel*Math.Tan(Mi_Pel/REarthP_Pel);
// ******************************************************************** Di

// XiYiZi ****************************************************************
// Координаты точки пересечения прямой (проходящей через текущую фиктивную
// точку и начало координат) и плоскости касательной в точке стояния 
// пеленгатора в СК пеленгатора

Xi_Pel = REarthP_Pel;
Yi_Pel = Di_Pel * Math.Sin(Theta_Pel);
Zi_Pel = Di_Pel * Math.Cos(Theta_Pel);
// **************************************************************** XiYiZi

// Xi0Yi0Zi0 *************************************************************
// Координаты точки пересечения прямой (проходящей через текущую фиктивную
// точку и начало координат) и плоскости касательной в точке стояния 
// пеленгатора в СК1 (Поворот СКП на угол=широте пеленгатора)

Xi0_Pel = Xi_Pel * Math.Cos(LatP_Pel) - Zi_Pel * Math.Sin(LatP_Pel);
Yi0_Pel = Yi_Pel;
Zi0_Pel = Xi_Pel * Math.Sin(LatP_Pel) + Zi_Pel * Math.Cos(LatP_Pel);

// ************************************************************* Xi0Yi0Zi0

// XiG1YiG1ZiG1 **********************************************************
// Координаты точки пересечения прямой (проходящей через текущую фиктивную
// точку и начало координат) и плоскости касательной в точке стояния 
// пеленгатора в опорной геоцентрической СК (Поворот СК1 на угол=долготе 
// пеленгатора)

XiG1_Pel = Xi0_Pel * Math.Cos(LongP_Pel) - Yi0_Pel * Math.Sin(LongP_Pel);
YiG1_Pel = Xi0_Pel * Math.Sin(LongP_Pel) + Yi0_Pel * Math.Cos(LongP_Pel);
ZiG1_Pel = Zi0_Pel;

// ********************************************************** XiG1YiG1ZiG1

// RiLATiLONGi ***********************************************************
// Угловые координаты фиктивной точки в опорной геоцентрической СК

Ri1_Pel = Math.Sqrt(XiG1_Pel*XiG1_Pel+YiG1_Pel*YiG1_Pel+ZiG1_Pel*ZiG1_Pel);

LATi_Pel = Math.Asin(ZiG1_Pel / Ri1_Pel);

if((XiG1_Pel<0)&&(YiG1_Pel>0))
    LONGi_Pel = Math.PI - Math.Asin(YiG1_Pel / (Ri1_Pel * Math.Cos(LATi_Pel)));
else if((XiG1_Pel<0)&&(YiG1_Pel<0))
    LONGi_Pel = -Math.PI - Math.Asin(YiG1_Pel / (Ri1_Pel * Math.Cos(LATi_Pel)));
else
    LONGi_Pel = Math.Asin(YiG1_Pel / (Ri1_Pel * Math.Cos(LATi_Pel)));

XiG_Pel = REarthP_Pel * Math.Cos(LATi_Pel) * Math.Cos(LONGi_Pel);
YiG_Pel = REarthP_Pel * Math.Cos(LATi_Pel) * Math.Sin(LONGi_Pel);
ZiG_Pel = REarthP_Pel * Math.Sin(LATi_Pel);
Ri_Pel = Math.Sqrt(XiG_Pel * XiG_Pel + YiG_Pel * YiG_Pel + ZiG_Pel * ZiG_Pel);

// *********************************************************** RiLATiLONGi

// Массивы ***************************************************************
// Занесение в массивы

// Ri,LATi,LONGi
//*(pmas_Pel+(IndMas_Pel*3)) = Ri_Pel;
//*(pmas_Pel+(IndMas_Pel*3+1)) = LATi_Pel;
//*(pmas_Pel+(IndMas_Pel*3+2)) = LONGi_Pel;
mas_Pel[IndMas_Pel * 3] = Ri_Pel;
mas_Pel[IndMas_Pel * 3 + 1] = LATi_Pel;
mas_Pel[IndMas_Pel * 3 + 2] = LONGi_Pel;

// XYZ в опорной геоцентрической СК
//*(pmas_Pel_XYZ+(IndMas_Pel*3)) = XiG_Pel;
//*(pmas_Pel_XYZ+(IndMas_Pel*3+1)) = YiG_Pel;
//*(pmas_Pel_XYZ+(IndMas_Pel*3+2)) = ZiG_Pel;
mas_Pel_XYZ[IndMas_Pel * 3] = XiG_Pel;
mas_Pel_XYZ[IndMas_Pel * 3 + 1] = YiG_Pel;
mas_Pel_XYZ[IndMas_Pel * 3 + 2] = ZiG_Pel;

// *************************************************************** Массивы

// Вывод для отладки ****************************************************

//*(pmas_Otl_Pel+IndMas_Pel) = Di_Pel;

// **************************************************** Вывод для отладки

// ***********************************************************************
// Переход к следующей фиктивной точке

IndFP_Pel+=1;
IndMas_Pel+=1;
// ***********************************************************************

}; // WHILE

XFN_Pel = XiG_Pel;
YFN_Pel = YiG_Pel;
ZFN_Pel = ZiG_Pel;

 } // Функция f_Peleng
//************************************************************************

// ***********************************************************************
// Расчет координат точки пересечения двух пеленгов
// Input: XP_Tr,YP_Tr,ZP_Tr,XFN_Tr,YFN_Tr,ZFN_Tr
// Output: XIRI_Tr,YIRI_Tr,ZIRI_Tr,LatIRI_Tr,LongIRI_Tr
// ***********************************************************************

    public void f_Triang

        (
			// Координаты пеленгатора в опорной геоцентрической СК
			double XP1_Tr,
			double YP1_Tr,
			double ZP1_Tr,
			double XP2_Tr,
			double YP2_Tr,
			double ZP2_Tr,

			// Координаты N-й фиктивной точки в опорной геоцентрической СК
			double XFN1_Tr,
			double YFN1_Tr,
			double ZFN1_Tr,
			double XFN2_Tr,
			double YFN2_Tr,
			double ZFN2_Tr,

			// Координаты искомого ИРИ в опорной геоцентрической СК
			ref double XIRI_Tr,
			ref double YIRI_Tr,
			ref double ZIRI_Tr,
			// Угловые координаты искомого ИРИ в опорной геоцентрической СК
			ref double RIRI_Tr,
			ref double LatIRI_Tr,
			ref double LongIRI_Tr

        )
    {


// Initial conditions ****************************************************
// Initial conditions

     if(fl_first_Tr==0)
      {

        // ...............................................................

        fl_first_Pel = 1;
        // ...............................................................

      }
// **************************************************** Initial conditions

// A,T,C *****************************************************************
// Расчет коэффициентов пллоскости пеленгования

 A1_Tr = YP1_Tr*(ZFN1_Tr)-ZP1_Tr*(YFN1_Tr);
 T1_Tr = ZP1_Tr*(XFN1_Tr)-XP1_Tr*(ZFN1_Tr);
 C1_Tr = XP1_Tr*(YFN1_Tr)-YP1_Tr*(XFN1_Tr);

 A2_Tr = YP2_Tr*(ZFN2_Tr)-ZP2_Tr*(YFN2_Tr);
 T2_Tr = ZP2_Tr*(XFN2_Tr)-XP2_Tr*(ZFN2_Tr);
 C2_Tr = XP2_Tr*(YFN2_Tr)-YP2_Tr*(XFN2_Tr);

// ***************************************************************** A,T,C

// C1!=0 11111111111111111111111111111111111111111111111111111111111111111
if(C1_Tr!=0)
{
// .......................................................................
 K1_Tr = -(A1_Tr/C1_Tr); 
 K2_Tr = -(T1_Tr/C1_Tr); 
 K3_Tr = (C1_Tr*A2_Tr-A1_Tr*C2_Tr)/(T1_Tr*C2_Tr-C1_Tr*T2_Tr);
// .......................................................................
 // Координаты искомого ИРИ в опорной геоцентрической СК

 XIRI1_Tr = REarth_Pel/Math.Sqrt(1+K3_Tr*K3_Tr+(K1_Tr+K2_Tr*K3_Tr)*(K1_Tr+K2_Tr*K3_Tr));
 XIRI2_Tr = -REarth_Pel/Math.Sqrt(1+K3_Tr*K3_Tr+(K1_Tr+K2_Tr*K3_Tr)*(K1_Tr+K2_Tr*K3_Tr));

 YIRI1_Tr = K3_Tr*XIRI1_Tr;
 YIRI2_Tr = K3_Tr*XIRI2_Tr;

 ZIRI1_Tr = K1_Tr*XIRI1_Tr+K2_Tr*YIRI1_Tr;
 ZIRI2_Tr = K1_Tr*XIRI2_Tr+K2_Tr*YIRI2_Tr;
// .......................................................................

} // C1!=0
// 11111111111111111111111111111111111111111111111111111111111111111 C1!=0

// C1=0 222222222222222222222222222222222222222222222222222222222222222222
else
{
// .......................................................................
 K3_Tr = -A1_Tr/T1_Tr;
 K2_Tr = (A1_Tr+K3_Tr*T1_Tr)/C2_Tr; 
// .......................................................................
 // Координаты искомого ИРИ в опорной геоцентрической СК

 XIRI1_Tr = REarth_Pel/Math.Sqrt(1+K2_Tr*K2_Tr+K3_Tr*K3_Tr);
 XIRI2_Tr = -REarth_Pel/Math.Sqrt(1+K2_Tr*K2_Tr+K3_Tr*K3_Tr);

 YIRI1_Tr = K3_Tr*XIRI1_Tr;
 YIRI2_Tr = K3_Tr*XIRI2_Tr;

 ZIRI1_Tr = K2_Tr*XIRI1_Tr;
 ZIRI2_Tr = K2_Tr*XIRI2_Tr;
// .......................................................................

} // C1=0
// 222222222222222222222222222222222222222222222222222222222222222222 C1=0

// XIRI_Tr,YIRI_Tr,ZIRI_Tr ***********************************************
// Критерий выбора координат искомого ИРИ в опорной геоцентрической СК
// (минимальное расстояние до ПП)

/*
 L1_Tr =((*pXFN1_Tr-*pXFN2_Tr)/2.-XIRI1_Tr)*((*pXFN1_Tr-*pXFN2_Tr)/2.-XIRI1_Tr)+
	    ((*pYFN1_Tr-*pYFN2_Tr)/2.-YIRI1_Tr)*((*pYFN1_Tr-*pYFN2_Tr)/2.-YIRI1_Tr)+
		((*pZFN1_Tr-*pZFN2_Tr)/2.-ZIRI1_Tr)*((*pZFN1_Tr-*pZFN2_Tr)/2.-ZIRI1_Tr);

 L2_Tr =((*pXFN1_Tr-*pXFN2_Tr)/2.-XIRI2_Tr)*((*pXFN1_Tr-*pXFN2_Tr)/2.-XIRI2_Tr)+
	    ((*pYFN1_Tr-*pYFN2_Tr)/2.-YIRI2_Tr)*((*pYFN1_Tr-*pYFN2_Tr)/2.-YIRI2_Tr)+
		((*pZFN1_Tr-*pZFN2_Tr)/2.-ZIRI2_Tr)*((*pZFN1_Tr-*pZFN2_Tr)/2.-ZIRI2_Tr);

*/

// Координаты середины отрезка, соединяющего N-ые фиктивные точки
 DefMiddle_Pel(XFN1_Tr,XFN2_Tr,ref XM_Tr);
 DefMiddle_Pel(YFN1_Tr,YFN2_Tr,ref YM_Tr);
 DefMiddle_Pel(ZFN1_Tr,ZFN2_Tr,ref ZM_Tr);

 L1_Tr = Math.Sqrt((XM_Tr-XIRI1_Tr)*(XM_Tr-XIRI1_Tr)+
	               (YM_Tr-YIRI1_Tr)*(YM_Tr-YIRI1_Tr)+
			       (ZM_Tr-ZIRI1_Tr)*(ZM_Tr-ZIRI1_Tr));

 L2_Tr = Math.Sqrt((XM_Tr-XIRI2_Tr)*(XM_Tr-XIRI2_Tr)+
	               (YM_Tr-YIRI2_Tr)*(YM_Tr-YIRI2_Tr)+
			       (ZM_Tr-ZIRI2_Tr)*(ZM_Tr-ZIRI2_Tr));

 if(L1_Tr<L2_Tr)
 {
   XIRI_Tr = XIRI1_Tr;
   YIRI_Tr = YIRI1_Tr;
   ZIRI_Tr = ZIRI1_Tr;
 }
 else
 {
   XIRI_Tr = XIRI2_Tr;
   YIRI_Tr = YIRI2_Tr;
   ZIRI_Tr = ZIRI2_Tr;
 }
// *********************************************** XIRI_Tr,YIRI_Tr,ZIRI_Tr

// RIRI_Tr,LatIRI_Tr,LongIRI_Tr ******************************************
// Угловые координаты искомого ИРИ в опорной геоцентрической СК

 // !!! Д.б. *pRIRI_Tr==REarth_Pel, т.е. r=Rз
 RIRI_Tr = Math.Sqrt(XIRI_Tr*XIRI_Tr+YIRI_Tr*YIRI_Tr+ZIRI_Tr*ZIRI_Tr);

 LatIRI_Tr = Math.Asin(ZIRI_Tr/RIRI_Tr);

 if((XIRI_Tr<0)&&(YIRI_Tr>0))
  LongIRI_Tr = Math.PI-Math.Asin(YIRI_Tr/(RIRI_Tr*Math.Cos(LatIRI_Tr)));
 else if((XIRI_Tr<0)&&(YIRI_Tr<0))
  LongIRI_Tr = -Math.PI-Math.Asin(YIRI_Tr/(RIRI_Tr*Math.Cos(LatIRI_Tr)));
 else
  LongIRI_Tr = Math.Asin(YIRI_Tr/(RIRI_Tr*Math.Cos(LatIRI_Tr)));

// ****************************************** RIRI_Tr,LatIRI_Tr,LongIRI_Tr


    } // Функция f_Triang
//**************************************************************************



    } // class Peleng

} // namespace
