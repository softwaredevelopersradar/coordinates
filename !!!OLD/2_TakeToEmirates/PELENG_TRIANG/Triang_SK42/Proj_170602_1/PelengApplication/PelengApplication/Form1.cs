﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;
using ClassLibraryPeleng;


namespace PelengApplication
{
// ***********************************************************************

public partial class Form1 : Form
{


    // !!! Просто для передачи в конструктор
    public string str_tmp;

    // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
    public string flnameXYZ;
    public string flnameLatLong;
    public string flnameXYZ1;
    public string flnameXYZ2;
    public string flname_SelR_Read;
    public string flname_SelR_Write;


    public double stemp1_traj;
    public double stemp2_traj;
    public double stemp3_traj;

    public double dshislo;
    public long ishislo;


    public string flnameIn_Tst;


    // Peleng PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP
    // FOR PELENG

    public double[] mas_Otl_Pel = new double[10000];
    //Координаты фиктивных точек
    public double[] mas_Pel = new double[10000];     // R,Широта,долгота
    public double[] mas_Pel_XYZ = new double[10000]; // XYZ в опорной геоцентрической СК

    // Otl -> для отладки
    public uint flagF2_Pel;
    public uint flagF3_Pel;
    public uint Ind_Otl_Pel;
    public uint i1;

    // Число фиктивных точек в плоскости пеленгования пеленгатора 
    public uint NumbFikt_Pel;

    // Индексы в массивах (0, ...)
    public uint IndFikt_Pel;
    public uint IndFikt_XYZ_Pel;

    // Текущее время моделирования
    public double time_tek_Pel;
    // Общее время моделирования
    public double T_Pel;
    // Дискрет времени
    public double dt_Pel;

    // Пеленг(рад)
    public double Theta_Pel;
    // Max дальность отображения пеленга
    public double Mmax_Pel;

    // Широта и долгота стояния пеленгатора
    public double LatP_Pel;
    public double LongP_Pel;

    // Координаты пеленгатора в опорной геоцентрической СК
    public double XP_Pel;
    public double YP_Pel;
    public double ZP_Pel;

    // Координаты N-й фиктивной точки в опорной геоцентрической СК
    public double XFN_Pel;
    public double YFN_Pel;
    public double ZFN_Pel;
    // PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP Peleng

    // Триангуляция triang triang triang triang triang triang triang triang tr 
    // For triangulation

    public uint flagF3_Tr;

    // Число фиктивных точек в плоскости пеленгования пеленгатора 
    public uint NumbFikt_Tr;

    // Пеленг(рад)
    public double Theta1_Tr;
    public double Theta2_Tr;

    // Max дальность отображения пеленга
    public double Mmax1_Tr;
    public double Mmax2_Tr;

    // Широта и долгота стояния пеленгатора
    public double LatP1_Tr;
    public double LongP1_Tr;
    public double LatP2_Tr;
    public double LongP2_Tr;

    // Индексы в массивах (0, ...)
    public uint IndFikt1_Tr;
    public uint IndFikt2_Tr;
    public uint IndFikt_XYZ1_Tr;
    public uint IndFikt_XYZ2_Tr;

    //Координаты фиктивных точек
    public double[] mas1_Tr = new double[10000]; // R,Широта,долгота    
    public double[] mas2_Tr = new double[10000]; // R,Широта,долгота    
    public double[] mas1_Tr_XYZ = new double[10000]; // XYZ в опорной геоцентрической СК    
    public double[] mas2_Tr_XYZ = new double[10000]; // XYZ в опорной геоцентрической СК    

    // Координаты пеленгатора в опорной геоцентрической СК
    public double XP1_Tr;
    public double YP1_Tr;
    public double ZP1_Tr;
    public double XP2_Tr;
    public double YP2_Tr;
    public double ZP2_Tr;

    // Координаты N-й фиктивной точки в опорной геоцентрической СК
    public double XFN1_Tr;
    public double YFN1_Tr;
    public double ZFN1_Tr;
    public double XFN2_Tr;
    public double YFN2_Tr;
    public double ZFN2_Tr;

    // Координаты искомого ИРИ в опорной геоцентрической СК
    public double XIRI_Tr;
    public double YIRI_Tr;
    public double ZIRI_Tr;
    // Угловые координаты искомого ИРИ в опорной геоцентрической СК
    public double RIRI_Tr;
    public double LatIRI_Tr;
    public double LongIRI_Tr;

    // Индексы в массивах (0, ...)
    public uint IndFikt_Tr;


    double X_IRI_42;
    double Y_IRI_42;

    int ttt;

    public double X1_42_Tr;
    public double Y1_42_Tr;
    public double X2_42_Tr;
    public double Y2_42_Tr;


    // Otl
    // Дискрет времени
    //double dt_Tr;        
    //double mas_Otl_Tr[10000];     

    // triang triang triang triang triang triang triang triang tr Триангуляция

    // Пересчет_координат ****************************************************

    // .......................................................................
    // Геодезические координаты для выбранного эллипсоида 
    // (при нажатии кнопки переводятся в радианы)
    public double Lat_Coord;
    public double Long_Coord;
    public double H_Coord;

    // Прямоугольные пространственные координаты для выбранного эллипсоида
    public double X_Coord;
    public double Y_Coord;
    public double Z_Coord;
    // .......................................................................
    // Для преобразования геодезических координат из WGS84 -> СК42(Красовский)
    // и обратно

    public double Lat_Coord_8442;
    public double Long_Coord_8442;
    public double H_Coord_8442;


    // Выходные параметры 
    public  double Lat_Coord_Vyx_8442;   // широта
    public  double Long_Coord_Vyx_8442;  // долгота
    public double Lat_Coord_Vyx_4284;    // широта
    public double Long_Coord_Vyx_4284;   // долгота
    public double H_Coord_Vyx_8442;      // высота
    public double H_Coord_Vyx_4284;      // высота

    // Приращения по долготе,широте,высоте при пересчете координат
    public double dLong_Coord;
    public double dLat_Coord;
    public double dH_Coord;

    // .......................................................................
    // Преобразование геодезических координат (широта, долгота, высота) 
    // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
    // проекции Гаусса-Крюгера и обратно

    public double Lat_Coord_Kr;
    public double Long_Coord_Kr;
    public double H_Coord_Kr;

    public double X_Coord_Kr;
    public double Y_Coord_Kr;

    public double Pi_main;
    // .......................................................................
    // DATUM

    public double dX_Coord;
    public double dY_Coord;
    public double dZ_Coord;
    // .......................................................................
    // Calculator

    public double Grad_Dbl_Coord;
    public int Grad_I_Coord;
    public int Min_Coord;
    public double Sec_Coord;
    // .......................................................................

    // **************************************************** Пересчет_координат

    // TEST ******************************************************************

    // Массив для чтения в формате: N,X,Y,F
    public double[] mas_Tst = new double[40000];

    public uint i_Tst;
    public uint i1_Tst;
    public uint i2_Tst;
    public uint i3_Tst;

    // Выходной массив
    public double[] mas_Out_Tst = new double[40000];
    // Массив в формате: 
    // число УС/РС,
    // число ист-ов в 1-ом УС/РС,
    // число ист-ов во 2-ом УС/РС, и т.д.
    public double[] mas_RN_Tst = new double[40000];


    // dXY
    public double dXY_Tst;
    // dF
    public double dF1_Tst;
    public double dF2_Tst;
    public double dF3_Tst;
    // dPel
    public double dPel_Tst;

    // Количество источников
    public uint NS_Tst;
    // ****************************************************************** TEST

    // Инициализация РЭС *****************************************************

    // Частота
    public double InpF;
    // Код модуляции
    public int InpKodMod;
    // Параметр модуляции
    public double InpParMod;
    public double InpParMod1;
    // Имя РЭС
    public string OutName;

    // Флаг ввода имени файла
    public int flName;
    // ***************************************************** Инициализация РЭС


    // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR  Переменные

    // Конструктор *********************************************************** 
    //  Конструктор

 public Form1()
   {
     InitializeComponent();


     // !!! Просто для передачи в конструктор
     str_tmp="";


     // Пеленг PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP

     // .......................................................................
     // !!! Задаваемые параметры

     // LatP_Pel,LongP_Pel -> Широта и долгота стояния пеленгатора (rad)
     // NumbFikt_Pel -> Число фиктивных точек в плоскости пеленгования пеленгатора
     // Theta_Pel -> Пеленг (rad)
     // Mmax_Pel -> Max дальность отображения пеленга (m)
     // .......................................................................
     // Вариант1

     //LatP_Pel=(10.*M_PI)/180.;   
     //LongP_Pel=(20.*M_PI)/180.;
     //NumbFikt_Pel=100;
     //Theta_Pel=(30.*M_PI)/180.; 
     //Mmax_Pel= 3000; 
     // .......................................................................
     // Вариант2

     //LatP_Pel=(0.1*M_PI)/180.;   
     //LongP_Pel=(0.1*M_PI)/180.;
     //NumbFikt_Pel=1000;
     //Theta_Pel=(89.9*M_PI)/180.; 
     //Mmax_Pel= 9000000; 
     // .......................................................................
     // Вариант3

     //LatP_Pel=(0.1*M_PI)/180.;   
     //LongP_Pel=(0.1*M_PI)/180.;
     //NumbFikt_Pel=1000;
     //Theta_Pel=(0.1*M_PI)/180.; 
     //Mmax_Pel= 9000000; 
     // .......................................................................
     // Вариант5

     //LatP_Pel=(89.*M_PI)/180.;   
     //LongP_Pel=(0.1*M_PI)/180.;
     //NumbFikt_Pel=1000;
     //Theta_Pel=(179*M_PI)/180.; 
     //Mmax_Pel= 9000000; 
     // .......................................................................
     // Вариант6

     //LatP_Pel = (0.1 * Math.PI) / 180;
     //LongP_Pel = (83 * Math.PI) / 180;
     //NumbFikt_Pel = 1000;
     //Theta_Pel = (271 * Math.PI) / 180;
     //Mmax_Pel = 9000000;
     // .......................................................................

     // Дискрет времени моделирования
     dt_Pel = 0.1;

     // Индексы в массивах (0, ...)
     IndFikt_Pel = 0;
     IndFikt_XYZ_Pel = 0;

     //Координаты фиктивных точек
     Array.Clear(mas_Pel, 0, 10000);
     Array.Clear(mas_Pel_XYZ, 0, 10000);

     // Текущее время моделирования
     time_tek_Pel = 0;
     // Общее время моделирования
     T_Pel = 0;

     flagF2_Pel = 0;
     flagF3_Pel = 0;

     // Координаты пеленгатора в опорной геоцентрической СК
     XP_Pel = 0;
     YP_Pel = 0;
     ZP_Pel = 0;

     // Координаты N-й фиктивной точки в опорной геоцентрической СК
     XFN_Pel = 0;
     YFN_Pel = 0;
     ZFN_Pel = 0;

     // Otl
     Array.Clear(mas_Otl_Pel, 0, 10000);

     // PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP Пеленг

     // Триангуляция Triang Triang Triang Triang Triang Triang Triang Triang Tr

     // ..................................................................
     // !!! Задаваемые параметры

     // LatP_Pel,LongP_Pel -> Широта и долгота стояния пеленгатора (rad)
     // NumbFikt_Tr -> Число фиктивных точек в плоскости пеленгования пеленгатора
     // Theta_Pel -> Пеленг (rad)
     // Mmax_Pel -> Max дальность отображения пеленга (m)
     // ..................................................................
     // Вариант1

     //NumbFikt_Tr = 1000;
     //Mmax1_Tr = 9000000;
     //Mmax2_Tr = 9000000;
     //Theta1_Tr = (179 * Math.PI) / 180;
     //Theta2_Tr = (271 * Math.PI) / 180;
     //LatP1_Tr = (89 * Math.PI) / 180;
     //LongP1_Tr = (0.1 * Math.PI) / 180;
     //LatP2_Tr = (0.1 * Math.PI) / 180;
     //LongP2_Tr = (83 * Math.PI) / 180;
     // ..................................................................

     // Индексы в массивах (0, ...)
     IndFikt1_Tr = 0;
     IndFikt2_Tr = 0;
     IndFikt_XYZ1_Tr = 0;
     IndFikt_XYZ2_Tr = 0;

     //Координаты фиктивных точек
     Array.Clear(mas1_Tr, 0, 10000);
     Array.Clear(mas2_Tr, 0, 10000);
     Array.Clear(mas1_Tr_XYZ, 0, 10000);
     Array.Clear(mas2_Tr_XYZ, 0, 10000);

     // Координаты пеленгатора в опорной геоцентрической СК
     XP1_Tr = 0;
     YP1_Tr = 0;
     ZP1_Tr = 0;
     XP2_Tr = 0;
     YP2_Tr = 0;
     ZP2_Tr = 0;

     // Координаты N-й фиктивной точки в опорной геоцентрической СК
     XFN1_Tr = 0;
     YFN1_Tr = 0;
     ZFN1_Tr = 0;
     XFN2_Tr = 0;
     YFN2_Tr = 0;
     ZFN2_Tr = 0;

     // Координаты искомого ИРИ в опорной геоцентрической СК
     XIRI_Tr = 0;
     YIRI_Tr = 0;
     ZIRI_Tr = 0;
     // Угловые координаты искомого ИРИ в опорной геоцентрической СК
     RIRI_Tr = 0;
     LatIRI_Tr = 0;
     LongIRI_Tr = 0;

     flagF3_Tr = 0;

     // Индексы в массивах (0, ...)
     IndFikt_Tr = 0;

     X_IRI_42=0;
     Y_IRI_42=0;

     ttt = 0;

    X1_42_Tr=0;
    Y1_42_Tr=0;
    X2_42_Tr=0;
    Y2_42_Tr=0;


       // Otl
       // Дискрет времени моделирования
       //dt_Tr=0.1;   
       // Общее время моделирования
       //T_Tr=0;
       //Array.Clear(mas_Otl_Tr, 0, 10000);

       // Triang Triang Triang Triang Triang Triang Triang Triang Tr Триангуляция

    // Пересчет_координат ****************************************************

     // .......................................................................
    // Геодезические координаты для выбранного эллипсоида 
    // (при нажатии кнопки переводятся в радианы)
    Lat_Coord=0;
    Long_Coord=0;
    H_Coord=0;

    // Прямоугольные пространственные координаты для выбранного эллипсоида
    X_Coord=0;
    Y_Coord=0;
    Z_Coord=0;
    // .......................................................................
    // Для преобразования геодезических координат из WGS84 -> СК42(Красовский)
    // и обратно

    Lat_Coord_8442=0;
    Long_Coord_8442=0;
    H_Coord_8442=0;

    // Выходные параметры 
    Lat_Coord_Vyx_8442=0;   // широта
    Long_Coord_Vyx_8442=0;  // долгота
    Lat_Coord_Vyx_4284=0;   // широта
    Long_Coord_Vyx_4284=0;  // долгота

    H_Coord_Vyx_8442=0;     // высота
    H_Coord_Vyx_4284 = 0;     // высота

    // Приращения по долготе, широте, высоте при пересчете координат
    dLong_Coord=0;
    dLat_Coord=0;
    dH_Coord = 0;

    // .......................................................................
    // Преобразование геодезических координат (широта, долгота, высота) 
    // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
    // проекции Гаусса-Крюгера и обратно

    Lat_Coord_Kr=0;
    Long_Coord_Kr=0;
    H_Coord_Kr=0;

    X_Coord_Kr=0;
    Y_Coord_Kr=0;
    // .......................................................................
    // DATUM

    // ГОСТ 51794_2008
    dX_Coord=25;
    dY_Coord=-141;
    dZ_Coord=-80;
    // .......................................................................
    Pi_main = 3.14159265358979; 
    // .......................................................................
    // Calculator

    Grad_Dbl_Coord=0;
    Grad_I_Coord=0;
    Min_Coord=0;
    Sec_Coord=0;
    // .......................................................................


    // **************************************************** Пересчет_координат

    // TEST ******************************************************************

    // Массив для чтения в формате: N,X,Y,F
    Array.Clear(mas_Tst, 0, 40000);

    // Выходной массив
    Array.Clear(mas_Out_Tst, 0, 40000);

    // Массив в формате: 
    // число УС/РС,
    // число ист-ов в 1-ом УС/РС,
    // число ист-ов во 2-ом УС/РС, и т.д.
    Array.Clear(mas_RN_Tst, 0, 40000);

    i_Tst=0;
    i1_Tst = 0;
    i2_Tst = 0;
    i3_Tst = 0;

    // dXY
    dXY_Tst=0;
    // dF
    dF1_Tst=0;
    dF2_Tst=0;
    dF3_Tst=0;
    // dPel
    dPel_Tst=0;

    // Количество источников
    NS_Tst=0;

    // ****************************************************************** TEST

   // Инициализация РЭС *****************************************************

    // Частота
    InpF=0;
    // Код модуляции
    InpKodMod=0;
    // Параметр модуляции
    InpParMod=0;
    InpParMod1 = 0;
    // Имя РЭС
    OutName="";

    // Флаг ввода имени файла
    flName=0;

    // ***************************************************** Инициализация РЭС


   } // Конструктор
 // *********************************************************** Конструктор

// P/P Button1 ************************************************************
// Обработчик кнопки Button1 -> Пеленг

 private void button1_Click(object sender, EventArgs e)
  {


      // ......................................................................
      // Number of points
      NumbFikt_Pel = Convert.ToUInt32(textBox1.Text);

      // rad
      //LatP_Pel = (Convert.ToDouble(textBox2.Text) * Math.PI) / 180;
      //LongP_Pel = (Convert.ToDouble(textBox3.Text) * Math.PI) / 180;
      //Theta_Pel = (Convert.ToDouble(textBox4.Text) * Math.PI) / 180;

      // град->перевод в рад - внутри функции
      LatP_Pel = Convert.ToDouble(textBox2.Text);
      LongP_Pel = Convert.ToDouble(textBox3.Text);
      Theta_Pel = Convert.ToDouble(textBox4.Text);

      // км->перевод в m - внутри функции
      Mmax_Pel = Convert.ToDouble(textBox5.Text);
      // ......................................................................

      Peleng objPeleng = new Peleng(str_tmp);

      // Peleng ****************************************************************

      // -----------------------------------------------------------------------

          objPeleng.f_Peleng(

                       // Пеленг(град)
                       Theta_Pel,
                       // Max дальность отображения пеленга(км)
                       Mmax_Pel,
                       // Количество фиктивных точек в плоскости пеленгования пеленгатора
                       NumbFikt_Pel,
                       // Широта и долгота стояния пеленгатора(град)
                       LatP_Pel,
                       LongP_Pel,

                       // Координаты пеленгатора в опорной геоцентрической СК(км)
                       ref XP_Pel,
                       ref YP_Pel,
                       ref ZP_Pel,

                       // Координаты N-й фиктивной точки в опорной геоцентрической СК(км)
                       ref XFN_Pel,
                       ref YFN_Pel,
                       ref ZFN_Pel,

                       // Координаты фиктивных точек
                       ref mas_Pel,    // R(км),Lat(град),Long(град)
                       ref mas_Pel_XYZ // км

                       // ForOtl
                       //&mas_Otl_Pel[0]
                       //mas_Otl_Pel


                       );
       // ----------------------------------------------------------------------
       //WRITE XYZ

       MessageBox.Show("Enter filename for XYZ");

        if(
            (saveFileDialog1.ShowDialog()==System.Windows.Forms.DialogResult.OK)&&
            (saveFileDialog1.FileName.Length>0)
            )

        flnameXYZ=saveFileDialog1.FileName;

        // Открыть двоичный файл для записи
        // Для отображения в 3D
        //BinaryWriter bWriter = new BinaryWriter(new FileStream("XYZ.bin", FileMode.OpenOrCreate));
        BinaryWriter bWriter = new BinaryWriter(new FileStream(flnameXYZ, FileMode.OpenOrCreate));
       // ----------------------------------------------------------------------
        //WRITE Latitude and Longitude

        MessageBox.Show("Enter filename for Latitude and Longitude");

        if (
            (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) &&
            (saveFileDialog1.FileName.Length > 0)
            )

            flnameLatLong = saveFileDialog1.FileName;

        // Открыть двоичный файл для записи
        BinaryWriter bWriter1 = new BinaryWriter(new FileStream(flnameLatLong, FileMode.OpenOrCreate));
       // ----------------------------------------------------------------------

       // Открыть двоичные файлы для записи

       // Для отображения в 3D
       //BinaryWriter bWriter = new BinaryWriter(new FileStream("XYZ.bin", FileMode.OpenOrCreate));

       // Широта, долгота
       //BinaryWriter bWriter1 = new BinaryWriter(new FileStream("LatLong.bin", FileMode.OpenOrCreate));

      // -----------------------------------------------------------------------
      // Запись в двоичный файл

      while(IndFikt_Pel<(NumbFikt_Pel*3))
      {
      // .......................................................................
      // Декартовы координаты

        // m
        //stemp1_traj = mas_Pel_XYZ[IndFikt_XYZ_Pel]/1000;//X
        //stemp2_traj = mas_Pel_XYZ[IndFikt_XYZ_Pel+1]/1000;//Y
        //stemp3_traj = mas_Pel_XYZ[IndFikt_XYZ_Pel+2]/1000;//Z
        // km
        stemp1_traj = mas_Pel_XYZ[IndFikt_XYZ_Pel];//X
        stemp2_traj = mas_Pel_XYZ[IndFikt_XYZ_Pel + 1];//Y
        stemp3_traj = mas_Pel_XYZ[IndFikt_XYZ_Pel + 2];//Z


        // !!! Такая запись нужна для отображения в 3D
        bWriter.Write(stemp3_traj); 
        bWriter.Write(stemp1_traj);
        bWriter.Write(stemp2_traj);
      // .......................................................................
      // Широта, долгота, grad

        //stemp1_traj = (mas_Pel[IndFikt_Pel + 1]*180)/Math.PI ;   // rad->grad
        //stemp2_traj = (mas_Pel[IndFikt_Pel + 2] * 180) / Math.PI;
        stemp1_traj = mas_Pel[IndFikt_Pel + 1];
        stemp2_traj = mas_Pel[IndFikt_Pel + 2];

        bWriter1.Write(stemp1_traj);
        bWriter1.Write(stemp2_traj);
      // .......................................................................
         IndFikt_Pel+=3;
         IndFikt_XYZ_Pel+=3;
      // .......................................................................

      }; // WHILE

      // .......................................................................
       bWriter.Close();
       bWriter1.Close();
      // .......................................................................
       IndFikt_Pel = 0;
       IndFikt_XYZ_Pel = 0;
      // .......................................................................

//      } //flagF3_Pel==0

      // **************************************************************** Peleng
       MessageBox.Show("Calculations completed");

  }  // P/P Button1
 // *************************************************************** Button1

 // P/P Button2 ************************************************************
 // TRIANGULATION

 private void button2_Click(object sender, EventArgs e)
 {

     // ......................................................................
     // Number of points

     NumbFikt_Tr = Convert.ToUInt32(textBox1.Text);
     // ......................................................................
     // Triangulation

     // Emitter locator 1
     // rad
     //LatP1_Tr = (Convert.ToDouble(textBox6.Text) * Math.PI) / 180;
     //LongP1_Tr = (Convert.ToDouble(textBox7.Text) * Math.PI) / 180;
     //Theta1_Tr = (Convert.ToDouble(textBox8.Text) * Math.PI) / 180;

     // град->перевод в рад - внутри функции
     LatP1_Tr = Convert.ToDouble(textBox6.Text);
     LongP1_Tr = Convert.ToDouble(textBox7.Text);
     Theta1_Tr = Convert.ToDouble(textBox8.Text);

     // км-> перевод в м - внутри функции
     Mmax1_Tr = Convert.ToDouble(textBox9.Text);

     // Emitter locator 2
     // rad
     //LatP2_Tr = (Convert.ToDouble(textBox13.Text) * Math.PI) / 180;
     //LongP2_Tr = (Convert.ToDouble(textBox12.Text) * Math.PI) / 180;
     //Theta2_Tr = (Convert.ToDouble(textBox11.Text) * Math.PI) / 180;

     // град->перевод в рад - внутри функции
     LatP2_Tr = Convert.ToDouble(textBox13.Text);
     LongP2_Tr = Convert.ToDouble(textBox12.Text);
     Theta2_Tr = Convert.ToDouble(textBox11.Text);

     // км-> перевод в м - внутри функции
     Mmax2_Tr = Convert.ToDouble(textBox10.Text);

     // ......................................................................

     Peleng objPeleng1 = new Peleng(str_tmp);
     Peleng objPeleng2 = new Peleng(str_tmp);
     Peleng objPeleng3 = new Peleng(str_tmp);
     Peleng objPeleng4 = new Peleng(str_tmp);
     Peleng objPeleng5 = new Peleng(str_tmp);


     // TRIANGUL **************************************************************
     // Метод триангуляции

         // -----------------------------------------------------------------------
         // Вычисление координат пеленгатора1 и N-й фиктивной точки для пеленгатора1
         // в опорной геоцентрической СК

/*
         objPeleng1.f_Peleng(

                        // Пеленг(град)
                        Theta1_Tr,
                        // Max дальность отображения пеленга(км)
                        Mmax1_Tr,
                         // Количество фиктивных точек в плоскости пеленгования пеленгатора
                        NumbFikt_Tr,
                         // Широта и долгота стояния пеленгатора(град)
                        LatP1_Tr,
                        LongP1_Tr,

                        // Координаты пеленгатора в опорной геоцентрической СК(км)
                        ref XP1_Tr,
                        ref YP1_Tr,
                        ref ZP1_Tr,

                        // Координаты N-й фиктивной точки в опорной геоцентрической СК(км)
                        ref XFN1_Tr,
                        ref YFN1_Tr,
                        ref ZFN1_Tr,

                        // Координаты фиктивных точек
                        ref mas1_Tr,    // R(км),Lat(град),Long(град)
                        ref mas1_Tr_XYZ // км

                                 );
*/

         // -----------------------------------------------------------------------
         // Вычисление координат пеленгатора2 и N-й фиктивной точки для пеленгатора2
         // в опорной геоцентрической СК

/*
         objPeleng2.f_Peleng(

                        // Пеленг(град)
                        Theta2_Tr,
                        // Max дальность отображения пеленга(км)
                        Mmax2_Tr,
                        // Количество фиктивных точек в плоскости пеленгования пеленгатора
                        NumbFikt_Tr,
                        // Широта и долгота стояния пеленгатора(град)
                        LatP2_Tr,
                        LongP2_Tr,

                        // Координаты пеленгатора в опорной геоцентрической СК(км)
                        ref XP2_Tr,
                        ref YP2_Tr,
                        ref ZP2_Tr,

                        // Координаты N-й фиктивной точки в опорной геоцентрической СК(км)
                        ref XFN2_Tr,
                        ref YFN2_Tr,
                        ref ZFN2_Tr,

                        // Координаты фиктивных точек
                        ref mas2_Tr,     // R(км),Lat(град),Long(град)
                        ref mas2_Tr_XYZ  // км

                                 );
*/
         // -----------------------------------------------------------------------
         // Расчет координат точки пересечения двух пеленгов
/*
         objPeleng3.f_Triang(

                   // Координаты пеленгатора в опорной геоцентрической СК(км)
                   XP1_Tr,
                   YP1_Tr,
                   ZP1_Tr,
                   XP2_Tr,
                   YP2_Tr,
                   ZP2_Tr,

                   // Координаты N-й фиктивной точки в опорной геоцентрической СК(км)
                   XFN1_Tr,
                   YFN1_Tr,
                   ZFN1_Tr,
                   XFN2_Tr,
                   YFN2_Tr,
                   ZFN2_Tr,

                   // Координаты искомого ИРИ в опорной геоцентрической СК(км)
                   ref XIRI_Tr,
                   ref YIRI_Tr,
                   ref ZIRI_Tr,

                   // Угловые координаты искомого ИРИ в опорной геоцентрической СК(град)
                   ref RIRI_Tr,   // км
                   ref LatIRI_Tr,
                   ref LongIRI_Tr

                                 );
*/
         // -----------------------------------------------------------------------

         // -----------------------------------------------------------------------
         // Расчет координат точки пересечения двух пеленгов

         objPeleng4.f_Triang1(

                         // Широта и долгота стояния пеленгатора(град)
                        LatP1_Tr,
                        LongP1_Tr,
                        LatP2_Tr,
                        LongP2_Tr,
                        // Пеленг(град)
                        Theta1_Tr,
                        Theta2_Tr,
                        // Max дальность отображения пеленга(км)
                        Mmax1_Tr,

                       // Координаты искомого ИРИ в опорной геоцентрической СК(км)
                       ref XIRI_Tr,
                       ref YIRI_Tr,
                       ref ZIRI_Tr,

                        // Угловые координаты искомого ИРИ в опорной геоцентрической СК(град)
                        ref RIRI_Tr,   // км
                        ref LatIRI_Tr,
                        ref LongIRI_Tr,

                        ref mas1_Tr,     // R(км),Lat(град),Long(град)
                        ref mas1_Tr_XYZ,  // км
                        ref mas2_Tr,     // R(км),Lat(град),Long(град)
                        ref mas2_Tr_XYZ  // км


                                 );



/*
         ttt=objPeleng5.f_Triang2(

                         6124.4516669,
                         5439.4442469,
                         6105.6763098,
                         5620.6994306,
                         60,
                         310,
                         200,

                          // Координаты искомого ИРИ (км)
                         ref X_IRI_42,
                         ref Y_IRI_42

                                 );
*/

         // -----------------------------------------------------------------------





         // Вывод координат искомого ИРИ

         //textBox14.Text = Convert.ToString(XIRI_Tr);     // X, m
         //textBox15.Text = Convert.ToString(YIRI_Tr);     // Y, m
         //textBox16.Text = Convert.ToString(ZIRI_Tr);     // Z, m
         //textBox17.Text = Convert.ToString((LatIRI_Tr*180)/Math.PI);   // Lat, grad
         //textBox18.Text = Convert.ToString((LongIRI_Tr * 180) / Math.PI);  // Long, grad


         ishislo = (long)(XIRI_Tr * 10000000);
         dshislo = ((double)ishislo) / 10000000;
         textBox14.Text = Convert.ToString(dshislo);     // X, km
         ishislo = (long)(YIRI_Tr * 10000000);
         dshislo = ((double)ishislo) / 10000000;
         textBox15.Text = Convert.ToString(dshislo);     // Y, km
         ishislo = (long)(ZIRI_Tr * 10000000);
         dshislo = ((double)ishislo) / 10000000;
         textBox16.Text = Convert.ToString(dshislo);     // Z, km

         //ishislo = (long)(((LatIRI_Tr * 180) / Math.PI)*10000000); // rad->grad
         //dshislo = ((double)ishislo)/10000000;
         //textBox17.Text = Convert.ToString(dshislo);   // Lat, grad
         //ishislo = (long)(((LongIRI_Tr * 180) / Math.PI) * 10000000);
         //dshislo = ((double)ishislo) / 10000000;
         //textBox18.Text = Convert.ToString(dshislo);  // Long, grad

         // grad
         ishislo = (long)(LatIRI_Tr  * 10000000); 
         dshislo = ((double)ishislo) / 10000000;
         textBox17.Text = Convert.ToString(dshislo);   // Lat, grad
         ishislo = (long)(LongIRI_Tr * 10000000);
         dshislo = ((double)ishislo) / 10000000;
         textBox18.Text = Convert.ToString(dshislo);  // Long, grad


         // -----------------------------------------------------------------------


         // FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
         // Запись в файлы

         // ----------------------------------------------------------------------
         //WRITE XYZ1

         MessageBox.Show("Enter filename for X1Y1Z1");

         if (
             (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) &&
             (saveFileDialog1.FileName.Length > 0)
             )

             flnameXYZ1 = saveFileDialog1.FileName;

         // Открыть двоичный файл для записи
         // Для отображения в 3D
         BinaryWriter bWriter2 = new BinaryWriter(new FileStream(flnameXYZ1, FileMode.OpenOrCreate));
         // ----------------------------------------------------------------------
         //WRITE XYZ2

         MessageBox.Show("Enter filename for X2Y2Z2");

         if (
             (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) &&
             (saveFileDialog1.FileName.Length > 0)
             )

             flnameXYZ2 = saveFileDialog1.FileName;

         // Открыть двоичный файл для записи
         // Для отображения в 3D
         BinaryWriter bWriter3 = new BinaryWriter(new FileStream(flnameXYZ2, FileMode.OpenOrCreate));
         // ----------------------------------------------------------------------
         
         // -----------------------------------------------------------------------
         // Открыть двоичные файлы для записи

         // Для отображения в 3D
         //BinaryWriter bWriter2 = new BinaryWriter(new FileStream("XYZ1.bin", FileMode.OpenOrCreate));
         //BinaryWriter bWriter3 = new BinaryWriter(new FileStream("XYZ2.bin", FileMode.OpenOrCreate));
         // -----------------------------------------------------------------------

         while (IndFikt_Tr < (NumbFikt_Tr * 3))
         {

             // .......................................................................
             // Декартовы координаты, Пеленгатор1

             // m->km
             //stemp1_traj = mas1_Tr_XYZ[IndFikt_Tr] / 1000;    //X
             //stemp2_traj = mas1_Tr_XYZ[IndFikt_Tr + 1] / 1000;//Y
             //stemp3_traj = mas1_Tr_XYZ[IndFikt_Tr + 2] / 1000;//Z

             // km
             stemp1_traj = mas1_Tr_XYZ[IndFikt_Tr];    //X
             stemp2_traj = mas1_Tr_XYZ[IndFikt_Tr + 1];//Y
             stemp3_traj = mas1_Tr_XYZ[IndFikt_Tr + 2];//Z

             // !!! Такая запись нужна для отображения в 3D
             bWriter2.Write(stemp3_traj);
             bWriter2.Write(stemp1_traj);
             bWriter2.Write(stemp2_traj);
             // .......................................................................
             // Декартовы координаты, Пеленгатор2

             // m->km
             //stemp1_traj = mas2_Tr_XYZ[IndFikt_Tr] / 1000;    //X
             //stemp2_traj = mas2_Tr_XYZ[IndFikt_Tr + 1] / 1000;//Y
             //stemp3_traj = mas2_Tr_XYZ[IndFikt_Tr + 2] / 1000;//Z

             // km
             stemp1_traj = mas2_Tr_XYZ[IndFikt_Tr];    //X
             stemp2_traj = mas2_Tr_XYZ[IndFikt_Tr + 1];//Y
             stemp3_traj = mas2_Tr_XYZ[IndFikt_Tr + 2];//Z

             // !!! Такая запись нужна для отображения в 3D
             bWriter3.Write(stemp3_traj);
             bWriter3.Write(stemp1_traj);
             bWriter3.Write(stemp2_traj);
             // .......................................................................
             IndFikt_Tr += 3;
             // .......................................................................

         }; // WHILE
         // .......................................................................

         bWriter2.Close();
         bWriter3.Close();
         // .......................................................................
         IndFikt_Tr = 0;
         // .......................................................................

         // FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF


//     } //flagF3_Tr==0

     // ************************************************************************ 
     //MessageBox.Show((string)listBox1.SelectedItem);
     MessageBox.Show("Calculations completed");

     // ************************************************************** TRIANGUL


 } // P/P button2_Click
// *************************************************************** Button2


 // TriangSK42 ****************************************************************
 private void button20_Click(object sender, EventArgs e)
 {

     // град->перевод в рад - внутри функции
     Theta1_Tr = Convert.ToDouble(textBox8.Text);
     // км-> перевод в м - внутри функции
     Mmax1_Tr = Convert.ToDouble(textBox9.Text);
     Theta2_Tr = Convert.ToDouble(textBox11.Text);

     X1_42_Tr = Convert.ToDouble(textBox52.Text);
     Y1_42_Tr = Convert.ToDouble(textBox53.Text);
     X2_42_Tr = Convert.ToDouble(textBox54.Text);
     Y2_42_Tr = Convert.ToDouble(textBox55.Text);

     // ......................................................................

     Peleng objPeleng5 = new Peleng(str_tmp);


     // TRIANGUL **************************************************************
     // Метод триангуляции


              ttt=objPeleng5.f_Triang2(


                             // km
                             X1_42_Tr,
                             Y1_42_Tr,
                             X2_42_Tr,
                             Y2_42_Tr,
                             // Пеленг(град)
                             Theta1_Tr,
                             Theta2_Tr,
                             Mmax1_Tr,


                              //6124.4516669,
                              //5439.4442469,
                              //6105.6763098,
                              //5620.6994306,
                              //60,
                              //310,
                              //200,

                               // Координаты искомого ИРИ (км)
                              ref X_IRI_42,
                              ref Y_IRI_42

                                      );
     // -----------------------------------------------------------------------
     // Вывод координат искомого ИРИ


     ishislo = (long)(X_IRI_42 * 10000000);
     dshislo = ((double)ishislo) / 10000000;
     textBox56.Text = Convert.ToString(dshislo);     // X, km
     ishislo = (long)(Y_IRI_42 * 10000000);
     dshislo = ((double)ishislo) / 10000000;
     textBox57.Text = Convert.ToString(dshislo);     // Y, km

     // ************************************************************** TRIANGUL


 }  // P/P Button20

 // **************************************************************** TriangSK42


// P/P Form1_Load *********************************************************

 private void Form1_Load(object sender, EventArgs e)
 {
 // ......................................................................
 // Number of points
    
     textBox1.Text = "1000"; // N
 // ......................................................................
 //Peleng

     textBox2.Text = "0,1";     // Lat,grad
     textBox3.Text = "80";      // Long, grad
     textBox4.Text = "270";     // Theta, grad
     textBox5.Text = "9000"; //Mmax,km
 // ......................................................................
 // Triangulation

/*
     // Emitter locator 1
     textBox6.Text = "89";       // Lat,grad
     textBox7.Text = "0,1";      // Long, grad
     textBox8.Text = "179";      // Theta, grad
     textBox9.Text = "9000000";  //Mmax,m

     // Emitter locator 2
     textBox13.Text = "0,1";       // Lat,grad
     textBox12.Text = "83";        // Long, grad
     textBox11.Text = "271";       // Theta, grad
     textBox10.Text = "9000000";   //Mmax,m
 */


     // Emitter locator 1
     textBox6.Text = "0";       // Lat,grad
     textBox7.Text = "0";      // Long, grad
     textBox8.Text = "0";      // Theta, grad
     textBox9.Text = "9000";  //Mmax,km

     // Emitter locator 2
     textBox13.Text = "0";       // Lat,grad
     textBox12.Text = "90";        // Long, grad
     textBox11.Text = "315";       // Theta, grad
     textBox10.Text = "9000";   //Mmax,km


     // sk42
     textBox52.Text = "6124,4516669";   //Mmax,km
     textBox53.Text = "5439,4442469";   //Mmax,km
     textBox54.Text = "6105,6763098";   //Mmax,km
     textBox55.Text = "5620,6994306";   //Mmax,km

 // ......................................................................
 // Coordinates

     // DATUM
     //textBox35.Text = "25";       // dX,m
     //textBox36.Text = "-141";     // dY,m
     //textBox37.Text = "-80";      // dZ,m

     textBox35.Text = Convert.ToString(dX_Coord);     // dX, m
     textBox36.Text = Convert.ToString(dY_Coord);     // dY, m
     textBox37.Text = Convert.ToString(dZ_Coord);     // dZ, m
 // ......................................................................
 // TEST

     textBox42.Text = "50";     // dXН, m
     textBox43.Text = "10";     // dF1, Гц
     textBox44.Text = "50";     // dF2, Гц
     textBox45.Text = "100";    // dF3, Гц
     textBox46.Text = "5";      // dPel,Град

 // ......................................................................
 // Идентификация РЭС

     textBox47.Text = "0";
     textBox48.Text = "0";
     textBox49.Text = "0";
     textBox50.Text = "";
     textBox51.Text = "0";      

 // ......................................................................
 // Number of points

     NumbFikt_Pel = Convert.ToUInt32(textBox1.Text);
     NumbFikt_Tr = Convert.ToUInt32(textBox1.Text);
 // ......................................................................
 //Peleng

     // rad
     //LatP_Pel = (Convert.ToDouble(textBox2.Text) * Math.PI)/180;
     //LongP_Pel = (Convert.ToDouble(textBox3.Text) * Math.PI) / 180;
     //Theta_Pel = (Convert.ToDouble(textBox4.Text) * Math.PI) / 180;
     //Mmax_Pel = Convert.ToDouble(textBox5.Text);
 // ......................................................................
 // Triangulation

     // Emitter locator 1
     // rad
     //LatP1_Tr = (Convert.ToDouble(textBox6.Text) * Math.PI) / 180;
     //LongP1_Tr = (Convert.ToDouble(textBox7.Text) * Math.PI) / 180;
     //Theta1_Tr = (Convert.ToDouble(textBox8.Text) * Math.PI) / 180;
     //Mmax1_Tr = Convert.ToDouble(textBox9.Text);

     // Emitter locator 2
     // rad
     //LatP2_Tr = (Convert.ToDouble(textBox13.Text) * Math.PI) / 180;
     //LongP2_Tr = (Convert.ToDouble(textBox12.Text) * Math.PI) / 180;
     //Theta2_Tr = (Convert.ToDouble(textBox11.Text) * Math.PI) / 180;
     //Mmax2_Tr = Convert.ToDouble(textBox10.Text);
     // ......................................................................

 } // P/P Form1_load

// ************************************************************ Form1_Load

// P/P Buttyon3:WGS84->SK42 ***********************************************
 // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)

 private void button3_Click(object sender, EventArgs e)
 {

// ..........................................................................
// DATUM

 // m (ГОСТ 51794-2008)
 dX_Coord = Convert.ToDouble(textBox35.Text);
 dY_Coord = Convert.ToDouble(textBox36.Text);
 dZ_Coord = Convert.ToDouble(textBox37.Text);
// ..........................................................................

// ..........................................................................
// Входные параметры -> град,km

    // Перевод в рад - внутри функции
    //Lat_Coord_8442 = (Convert.ToDouble(textBox19.Text) * Math.PI) / 180;
    //Long_Coord_8442 = (Convert.ToDouble(textBox20.Text) * Math.PI) / 180;
    Lat_Coord_8442 = Convert.ToDouble(textBox19.Text);
    Long_Coord_8442 = Convert.ToDouble(textBox20.Text);

    // km (перевод в м - внутри функции)
    H_Coord_8442 = Convert.ToDouble(textBox21.Text);
// ..........................................................................
    Peleng objPeleng5 = new Peleng(str_tmp);
// ..........................................................................

// ..........................................................................
    // !!! Используется DATUM ГОСТ 51794-2008 :
    // dX_Coord=25m  dY_Coord=-141m  dZ_Coord=-80m
    // !!! Задать в Public
    // Для изменения DATUM необходимо вызвать функциюf_Change_DATUM

    // Входные параметры:
    // New_dX_Coord,New_dY_Coord,New_dZ_Coord - новый датум (м)

    // Выходные параметры:
    // dX_Coord,dY_Coord,dZ_Coord - !!! это д.б.глобальные переменные, в которых
    // хранится DATUM (м) -> в них занесется новые значения датума


// если хотим зменить DATUM 
/*    
        objPeleng5.f_Change_DATUM
            (

                // Входные параметры (m)
                23.92,     //(ГОСТ 51794-2001)
                -141.27,
                -80.91,
                //28, // WGS84 NIMA 2000
                //-130,
                //-95,

                // Выходные параметры (m)
                ref dX_Coord,
                ref dY_Coord,
                ref dZ_Coord

            );
 
  
         textBox35.Text = Convert.ToString(dX_Coord);     // dX, m
         textBox36.Text = Convert.ToString(dY_Coord);     // dY, m
         textBox37.Text = Convert.ToString(dZ_Coord);     // dZ, m
 
*/
    
// ..........................................................................


// H ************************************************************************
/*
     // Расчет dH
     objPeleng5.f_WGS84_Alt(

                    // Входные параметры (при нажатии кнопки переводятся в радианы)
                    Lat_Coord_8442,   // широта
                    Long_Coord_8442,  // долгота
                    H_Coord_8442,     // высота

                    // DATUM
                    dX_Coord,
                    dY_Coord,
                    dZ_Coord,

                    // Выходные параметры (m)
                    ref dH_Coord      

                             );

     // Расчет H
     objPeleng5.f_WGS84_SK42_Alt(

                    // Входные параметры 
                    H_Coord_8442,     // высота
                    dH_Coord,

                    // Выходные параметры (m)
                    ref H_Coord_Vyx_8442

                             );
*/
// ************************************************************************* H

// dLong ********************************************************************
// Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
// (преобразования Молоденского), угл.сек

    objPeleng5.f_dLong
        (

            // Входные параметры (град,км)
            Lat_Coord_8442,   // широта
            Long_Coord_8442,  // долгота
            H_Coord_8442,     // высота

            // DATUM,m
            dX_Coord,
            dY_Coord,
            dZ_Coord,

            ref dLong_Coord   // приращение по долготе, угл.сек

        );

// ******************************************************************** dLong

// dLat *********************************************************************
// Расчет приращения по широте при преобразованиях координат WGS84<->SK42
// (преобразования Молоденского), угл.сек

    objPeleng5.f_dLat
        (

            // Входные параметры (град,км)
            Lat_Coord_8442,   // широта
            Long_Coord_8442,  // долгота
            H_Coord_8442,     // высота

            // DATUM,m
            dX_Coord,
            dY_Coord,
            dZ_Coord,

            ref dLat_Coord        // приращение по долготе, угл.сек

        );

// ********************************************************************* dLat

// Lat,Long *****************************************************************
// Преобразования широты и долготы при пересчете WGS84->SK42

 objPeleng5.f_WGS84_SK42_Lat_Long
        (

            // Входные параметры (град,км)
            Lat_Coord_8442,   // широта
            Long_Coord_8442,  // долгота
            H_Coord_8442,     // высота
            dLat_Coord,       // приращение по долготе, угл.сек
            dLong_Coord,      // приращение по долготе, угл.сек

            // Выходные параметры (grad)
            ref Lat_Coord_Vyx_8442,   // широта
            ref Long_Coord_Vyx_8442   // долгота

        );

// ***************************************************************** Lat,Long

// Отображение **************************************************************

 // !!! Широта и долгота уже пересчитаны в град
 ////textBox24.Text = Convert.ToString(H_Coord_Vyx_8442);        // H, m
 //textBox24.Text = Convert.ToString(H_Coord_8442);        // H, m
 //textBox22.Text = Convert.ToString(Lat_Coord_Vyx_8442);     // Latitude, grad
 //textBox23.Text = Convert.ToString(Long_Coord_Vyx_8442);    // Latitude, grad

 ishislo = (long)(H_Coord_8442 * 10000000);
 dshislo = ((double)ishislo) / 10000000;
 textBox24.Text = Convert.ToString(dshislo);        // H, km
 ishislo = (long)(Lat_Coord_Vyx_8442 * 10000000);
 dshislo = ((double)ishislo) / 10000000;
 textBox22.Text = Convert.ToString(dshislo);     // Latitude, grad
 ishislo = (long)(Long_Coord_Vyx_8442 * 10000000);
 dshislo = ((double)ishislo) / 10000000;
 textBox23.Text = Convert.ToString(dshislo);    // Latitude, grad


// ************************************************************** Отображение

 } // P/P Button3

// ****************************************************************** Button3

 // P/P Buttyon4: SK42->WGS84 ***********************************************
 // Перевод координат эллипсоид Красовского(Пулково-42)->WGS84 (эллипсоид)

 private void button4_Click(object sender, EventArgs e)
 {

     // ..........................................................................
     // DATUM

     // m
     dX_Coord = Convert.ToDouble(textBox35.Text);
     dY_Coord = Convert.ToDouble(textBox36.Text);
     dZ_Coord = Convert.ToDouble(textBox37.Text);
     // ..........................................................................

     // ......................................................................
     // Входные параметры -> (град)

     // Перевод в рад - внутри функции
     //Lat_Coord_8442 = (Convert.ToDouble(textBox19.Text) * Math.PI) / 180;
     //Long_Coord_8442 = (Convert.ToDouble(textBox20.Text) * Math.PI) / 180;
     Lat_Coord_8442 = Convert.ToDouble(textBox19.Text);
     Long_Coord_8442 = Convert.ToDouble(textBox20.Text) ;

     // km
     H_Coord_8442 = Convert.ToDouble(textBox21.Text);
     // ......................................................................
     Peleng objPeleng6 = new Peleng(str_tmp);
     // ......................................................................


 // H ************************************************************************
/*
     // Расчет dH
     objPeleng6.f_WGS84_Alt(

                    // Входные параметры (при нажатии кнопки переводятся в радианы)
                    Lat_Coord_8442,   // широта
                    Long_Coord_8442,  // долгота
                    H_Coord_8442,     // высота

                    // DATUM
                    dX_Coord,
                    dY_Coord,
                    dZ_Coord,

                    // Выходные параметры (m)
                    ref dH_Coord

                             );

     // Расчет H
     objPeleng6.f_SK42_WGS84_Alt(

                    // Входные параметры 
                    H_Coord_8442,     // высота
                    dH_Coord,

                    // Выходные параметры (m)
                    ref H_Coord_Vyx_4284

                             );
*/
// ************************************************************************** H 

  // dLong ********************************************************************
  // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
  // (преобразования Молоденского)

     objPeleng6.f_dLong
         (

             // Входные параметры (grad,km)
             Lat_Coord_8442,   // широта
             Long_Coord_8442,  // долгота
             H_Coord_8442,     // высота

             // DATUM,m
             dX_Coord,
             dY_Coord,
             dZ_Coord,

             ref dLong_Coord   // приращение по долготе, угл.с

         );

  // ******************************************************************** dLong

  // dLat *********************************************************************
  // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
  // (преобразования Молоденского)

     objPeleng6.f_dLat
         (

             // Входные параметры (grad,km)
             Lat_Coord_8442,   // широта
             Long_Coord_8442,  // долгота
             H_Coord_8442,     // высота

             // DATUM,m
             dX_Coord,
             dY_Coord,
             dZ_Coord,

             ref dLat_Coord    // приращение по долготе, угл.с

         );

  // ********************************************************************* dLat

  // Lat,Long *****************************************************************
  // Преобразования широты и долготы при пересчете SK42->WGS84

     objPeleng6.f_SK42_WGS84_Lat_Long
            (

                // Входные параметры (grad,km)
                Lat_Coord_8442,   // широта
                Long_Coord_8442,  // долгота
                H_Coord_8442,     // высота
                dLat_Coord,    // приращение по долготе, угл.с
                dLong_Coord,   // приращение по долготе, угл.с

                // Выходные параметры  (grad)
                ref Lat_Coord_Vyx_4284,   // широта
                ref Long_Coord_Vyx_4284  // долгота

            );

  // ***************************************************************** Lat,Long

  // Отображение **************************************************************

     // !!! Широта и долгота уже пересчитаны в град
     ////textBox24.Text = Convert.ToString(H_Coord_Vyx_4284);            // H, m
     //textBox24.Text = Convert.ToString(H_Coord_8442);            // H, m
     //textBox22.Text = Convert.ToString(Lat_Coord_Vyx_4284);     // Latitude, grad
     //textBox23.Text = Convert.ToString(Long_Coord_Vyx_4284);    // Latitude, grad


     ishislo = (long)(H_Coord_8442 * 10000000);
     dshislo = ((double)ishislo) / 10000000;
     textBox24.Text = Convert.ToString(dshislo);            // H, km
     ishislo = (long)(Lat_Coord_Vyx_4284 * 10000000);
     dshislo = ((double)ishislo) / 10000000;
     textBox22.Text = Convert.ToString(dshislo);     // Latitude, grad
     ishislo = (long)(Long_Coord_Vyx_4284 * 10000000);
     dshislo = ((double)ishislo) / 10000000;
     textBox23.Text = Convert.ToString(dshislo);    // Latitude, grad

  // ************************************************************** Отображение

 
 } // P/P Button4

 // *********************************************************** SK42->WGS84

// P/P Button8: BLH->XYZ_84 ************************************************
// Преобразование геодезических координат в пространственные прямоугольные 
// координаты для эллипсоида WGS84

 private void button8_Click(object sender, EventArgs e)
 {

// ..........................................................................
// Входные параметры -> grad,km

  // Перевод в рад - внутри функции
  //Lat_Coord = (Convert.ToDouble(textBox28.Text) * Math.PI) / 180;
  //Long_Coord = (Convert.ToDouble(textBox29.Text) * Math.PI) / 180;
  Lat_Coord = Convert.ToDouble(textBox28.Text);
  Long_Coord = Convert.ToDouble(textBox29.Text);

  H_Coord = Convert.ToDouble(textBox30.Text);  // km
// ..........................................................................
  Peleng objPeleng7 = new Peleng(str_tmp);
// ..........................................................................

// XYZ **********************************************************************

  objPeleng7.f_BLH_XYZ_84
         (

             // Входные параметры (grad,km)
             Lat_Coord,   // широта
             Long_Coord,  // долгота
             H_Coord,     // высота

             // Выходные параметры,km
             ref X_Coord,   
             ref Y_Coord,   
             ref Z_Coord  

         );

// ********************************************************************** XYZ

// Отображение **************************************************************

  // X,Y,Z -> km
  //textBox25.Text = Convert.ToString(X_Coord);            
  //textBox26.Text = Convert.ToString(Y_Coord);     
  //textBox27.Text = Convert.ToString(Z_Coord);


  ishislo = (long)(X_Coord * 10000000);
  dshislo = ((double)ishislo) / 10000000;
  textBox25.Text = Convert.ToString(dshislo);
  ishislo = (long)(Y_Coord * 10000000);
  dshislo = ((double)ishislo) / 10000000;
  textBox26.Text = Convert.ToString(dshislo);
  ishislo = (long)(Z_Coord * 10000000);
  dshislo = ((double)ishislo) / 10000000;
  textBox27.Text = Convert.ToString(dshislo);

// ************************************************************** Отображение


 } // P/P Button8

// ************************************************ P/P Button8: BLH->XYZ_84

// P/P Button10: BLH->XYZ_42 ***********************************************
// Преобразование геодезических координат в пространственные прямоугольные 
// координаты для эллипсоида Красовского (SK42)

 private void button10_Click(object sender, EventArgs e)
 {

     // ..........................................................................
     // Входные параметры -> grad,km

     // Перевод в рад - внутри функции
     //Lat_Coord = (Convert.ToDouble(textBox28.Text) * Math.PI) / 180;
     //Long_Coord = (Convert.ToDouble(textBox29.Text) * Math.PI) / 180;
     Lat_Coord = Convert.ToDouble(textBox28.Text);
     Long_Coord = Convert.ToDouble(textBox29.Text);

     H_Coord = Convert.ToDouble(textBox30.Text);  // km
     // ..........................................................................
     Peleng objPeleng8 = new Peleng(str_tmp);
     // ..........................................................................

     // XYZ **********************************************************************

     objPeleng8.f_BLH_XYZ_42
            (

                // Входные параметры (grad,km)
                Lat_Coord,   // широта
                Long_Coord,  // долгота
                H_Coord,     // высота

                // Выходные параметры,km
                ref X_Coord,
                ref Y_Coord,
                ref Z_Coord

            );

     // ********************************************************************** XYZ

     // Отображение **************************************************************

     // X,Y,Z -> km
     //textBox25.Text = Convert.ToString(X_Coord);
     //textBox26.Text = Convert.ToString(Y_Coord);
     //textBox27.Text = Convert.ToString(Z_Coord);

     ishislo = (long)(X_Coord * 10000000);
     dshislo = ((double)ishislo) / 10000000;
     textBox25.Text = Convert.ToString(dshislo);
     ishislo = (long)(Y_Coord * 10000000);
     dshislo = ((double)ishislo) / 10000000;
     textBox26.Text = Convert.ToString(dshislo);
     ishislo = (long)(Z_Coord * 10000000);
     dshislo = ((double)ishislo) / 10000000;
     textBox27.Text = Convert.ToString(dshislo);

     // ************************************************************** Отображение

 } // P/P Button10

 // *********************************************** P/P Button10: BLH->XYZ_42

 // P/P Button7: XYZ->BLH_84 ************************************************
 // Преобразование пространственных прямоугольных координат в геодезические
 // координаты для эллипсоида WGS84

 private void button7_Click(object sender, EventArgs e)
 {

     // ..........................................................................
     // Входные параметры -> km

     X_Coord = Convert.ToDouble(textBox25.Text);
     Y_Coord = Convert.ToDouble(textBox26.Text);
     Z_Coord = Convert.ToDouble(textBox27.Text);  
     // ..........................................................................
     Peleng objPeleng8 = new Peleng(str_tmp);
     // ..........................................................................

     // XYZ **********************************************************************

     objPeleng8.f_XYZ_BLH_84
            (

                // Входные параметры: прямоугольные пространственные координаты
                // для выбранного эллипсоида (км)
                X_Coord,   
                Y_Coord,  
                Z_Coord,     

                // Выходные параметры,grad,km
                ref Lat_Coord,
                ref Long_Coord,
                ref H_Coord

            );

     // ********************************************************************** XYZ

     // Отображение **************************************************************

     // Lat,Long -> grad
     //Lat_Coord = (Lat_Coord*180)/Math.PI;
     //Long_Coord = (Long_Coord * 180) / Math.PI;

     //textBox28.Text = Convert.ToString(Lat_Coord);
     //textBox29.Text = Convert.ToString(Long_Coord);
     //textBox30.Text = Convert.ToString(H_Coord);    // km

     ishislo = (long)(Lat_Coord * 10000000);
     dshislo = ((double)ishislo) / 10000000;
     textBox28.Text = Convert.ToString(dshislo);
     ishislo = (long)(Long_Coord * 10000000);
     dshislo = ((double)ishislo) / 10000000;
     textBox29.Text = Convert.ToString(dshislo);
     ishislo = (long)(H_Coord * 10000000);
     dshislo = ((double)ishislo) / 10000000;
     textBox30.Text = Convert.ToString(dshislo);

     // ************************************************************** Отображение

 } // P/P Button7

 // ************************************************ P/P Button7: XYZ->BLH_84

 // P/P Button9: XYZ->BLH_42 ************************************************
 // Преобразование пространственных прямоугольных координат в геодезические
 // координаты для эллипсоида Красовского (SK42)

 private void button9_Click(object sender, EventArgs e)
 {

     // ..........................................................................
     // Входные параметры -> km

     X_Coord = Convert.ToDouble(textBox25.Text);
     Y_Coord = Convert.ToDouble(textBox26.Text);
     Z_Coord = Convert.ToDouble(textBox27.Text);
     // ..........................................................................
     Peleng objPeleng9 = new Peleng(str_tmp);
     // ..........................................................................

     // XYZ **********************************************************************

     objPeleng9.f_XYZ_BLH_42
            (

                // Входные параметры: прямоугольные пространственные координаты
                // для выбранного эллипсоида (km)
                X_Coord,
                Y_Coord,
                Z_Coord,

                // Выходные параметры,grad
                ref Lat_Coord,
                ref Long_Coord,
                ref H_Coord     // km

            );

     // ********************************************************************** XYZ

     // Отображение **************************************************************

     // Lat,Long -> grad
     //Lat_Coord = (Lat_Coord * 180) / Math.PI;
     //Long_Coord = (Long_Coord * 180) / Math.PI;

     // Lat,Long -> grad
     //textBox28.Text = Convert.ToString(Lat_Coord);
     //textBox29.Text = Convert.ToString(Long_Coord);
     //textBox30.Text = Convert.ToString(H_Coord);    // km

     ishislo = (long)(Lat_Coord * 10000000);
     dshislo = ((double)ishislo) / 10000000;
     textBox28.Text = Convert.ToString(dshislo);
     ishislo = (long)(Long_Coord * 10000000);
     dshislo = ((double)ishislo) / 10000000;
     textBox29.Text = Convert.ToString(dshislo);
     ishislo = (long)(H_Coord * 10000000);
     dshislo = ((double)ishislo) / 10000000;
     textBox30.Text = Convert.ToString(dshislo);

     // ************************************************************** Отображение

 } // P/P Button9
 // ************************************************ P/P Button9: XYZ->BLH_42

// P/P Button5: SK42->Krug **************************************************
// Преобразование геодезических координат (широта, долгота, высота) 
// эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
// проекции Гаусса-Крюгера

 private void button5_Click(object sender, EventArgs e)
 {

// ..........................................................................
// Входные параметры -> !!!grad

     Lat_Coord_Kr = Convert.ToDouble(textBox31.Text);
     Long_Coord_Kr = Convert.ToDouble(textBox32.Text);
// ..........................................................................
     Peleng objPeleng10 = new Peleng(str_tmp);
// ..........................................................................

// XY ***********************************************************************

     objPeleng10.f_SK42_Krug
            (

                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                Lat_Coord_Kr,   // широта
                Long_Coord_Kr,  // долгота

                // Выходные параметры (km)
                ref X_Coord_Kr,
                ref Y_Coord_Kr

            );

// *********************************************************************** XY

// Отображение **************************************************************

     // X,Y -> m
     //textBox33.Text = Convert.ToString(X_Coord_Kr);
     //textBox34.Text = Convert.ToString(Y_Coord_Kr);

     ishislo = (long)(X_Coord_Kr * 10000000);
     dshislo = ((double)ishislo) / 10000000;
     textBox33.Text = Convert.ToString(dshislo);
     ishislo = (long)(Y_Coord_Kr * 10000000);
     dshislo = ((double)ishislo) / 10000000;
     textBox34.Text = Convert.ToString(dshislo);

// ************************************************************** Отображение

 } // P/P Button5
// ************************************************** P/P Button5: SK42->Krug

// P/P Button6: Krug->SK42 **************************************************
// Преобразование плоских прямоугольных координат в проекци 
// Гаусса-Крюгера в геодезические координаты (широта, долгота, высота) 
// эллипсоида Красовского (СК42) 

 private void button6_Click(object sender, EventArgs e)
 {

// ..........................................................................
// Входные параметры -> km

     X_Coord_Kr = Convert.ToDouble(textBox33.Text);
     Y_Coord_Kr = Convert.ToDouble(textBox34.Text);
     // ..........................................................................
     Peleng objPeleng11 = new Peleng(str_tmp);
// ..........................................................................

// XY ***********************************************************************

     objPeleng11.f_Krug_SK42
            (

                // Входные параметры (km)
                X_Coord_Kr,
                Y_Coord_Kr,

                // Выходные параметры (grad)
                ref Lat_Coord_Kr,   // широта
                ref Long_Coord_Kr   // долгота

            );

// *********************************************************************** XY

// Отображение **************************************************************

     //Lat_Coord_Kr = (Lat_Coord_Kr * 180) / Math.PI;
     //Long_Coord_Kr = (Long_Coord_Kr * 180) / Math.PI;

     // Lat,Long -> grad
     //textBox31.Text = Convert.ToString(Lat_Coord_Kr);
     //textBox32.Text = Convert.ToString(Long_Coord_Kr);


     ishislo = (long)(Lat_Coord_Kr * 10000000);
     dshislo = ((double)ishislo) / 10000000;
     textBox31.Text = Convert.ToString(dshislo);
     ishislo = (long)(Long_Coord_Kr * 10000000);
     dshislo = ((double)ishislo) / 10000000;
     textBox32.Text = Convert.ToString(dshislo);


// ************************************************************** Отображение


 } // P/P Button6
 // ************************************************** P/P Button6: Krug->SK42


 private void textBox40_TextChanged(object sender, EventArgs e)
 {
     ;
 }

// P/P Button11: dd.ddddd(grad)->DD MM SS ***********************************

 private void button11_Click(object sender, EventArgs e)
 {
// ..........................................................................
// Входные параметры -> grad

     Grad_Dbl_Coord = Convert.ToDouble(textBox38.Text);
// ..........................................................................
     Peleng objPeleng12 = new Peleng(str_tmp);
// ..........................................................................

// DD MM SS *****************************************************************

     objPeleng12.f_Grad_GMS
            (
                // Входные параметры (grad)
                Grad_Dbl_Coord,

                // Выходные параметры 
                ref Grad_I_Coord,  // Целое 
                ref Min_Coord,     // Целое
                ref Sec_Coord      // Дробное

            );

// ***************************************************************** DD MM SS

// Отображение **************************************************************

     textBox39.Text = Convert.ToString(Grad_I_Coord);
     textBox40.Text = Convert.ToString(Min_Coord);
     textBox41.Text = Convert.ToString(Sec_Coord);

// ************************************************************** Отображение

 } // P/P Button11
 // *********************************** P/P Button11: dd.ddddd(grad)->DD MM SS

// P/P Button12: DD MM SS->dd.ddddd(grad) ***********************************

 private void button12_Click(object sender, EventArgs e)
 {
// ..........................................................................
// Входные параметры -> grad

     Grad_I_Coord = Convert.ToInt32(textBox39.Text);
     Min_Coord = Convert.ToInt32(textBox40.Text);
     Sec_Coord = Convert.ToDouble(textBox41.Text);
// ..........................................................................
     Peleng objPeleng13 = new Peleng(str_tmp);
// ..........................................................................

// dd.ddddd *****************************************************************

     objPeleng13.f_GMS_Grad
            (
               // Входные параметры 
               Grad_I_Coord, // Целое
               Min_Coord,    // Целое
               Sec_Coord,    // Дробное

               // Выходные параметры (grad)
               ref Grad_Dbl_Coord

            );

// ***************************************************************** dd.ddddd

// Отображение **************************************************************

     textBox38.Text = Convert.ToString(Grad_Dbl_Coord);

// ************************************************************** Отображение

 } // P/P Button12
// *********************************** P/P Button12: DD MM SS->dd.ddddd(grad)

// P/P Button13(Clear) ******************************************************
 private void button13_Click(object sender, EventArgs e)
 {
     textBox14.Clear();
     textBox15.Clear();
     textBox16.Clear();
     textBox17.Clear();
     textBox18.Clear();
     textBox19.Clear();
     textBox20.Clear();
     textBox21.Clear();
     textBox22.Clear();
     textBox23.Clear();
     textBox24.Clear();
     textBox25.Clear();
     textBox26.Clear();
     textBox27.Clear();
     textBox28.Clear();
     textBox29.Clear();
     textBox30.Clear();
     textBox31.Clear();
     textBox32.Clear();
     textBox33.Clear();
     textBox34.Clear();
     textBox38.Clear();
     textBox39.Clear();
     textBox40.Clear();
     textBox41.Clear();

 } // P/P Button13
// ****************************************************** P/P Button13(Clear)

// P/P Button14(RadioRoute) *************************************************
// Радионаправление

 private void button14_Click(object sender, EventArgs e)
{

// --------------------------------------------------------------------------
   dXY_Tst = Convert.ToDouble(textBox42.Text);

   dF1_Tst = Convert.ToDouble(textBox43.Text);
   dF2_Tst = Convert.ToDouble(textBox44.Text);
   dF3_Tst = Convert.ToDouble(textBox45.Text);

   dPel_Tst = Convert.ToDouble(textBox46.Text);

// -------------------------------------------------------------------------
   Peleng objPeleng = new Peleng(str_tmp);
// --------------------------------------------------------------------------
// Чтение входного файла

  MessageBox.Show("Enter filename for reading");

  if(
     (openFileDialog1.ShowDialog()==System.Windows.Forms.DialogResult.OK)&&
     (openFileDialog1.FileName.Length > 0)
    )

      flnameIn_Tst = openFileDialog1.FileName;

  BinaryReader reader = new BinaryReader(File.Open(flnameIn_Tst, FileMode.Open));


  i_Tst = 0;

  while (reader.PeekChar()!=-1)
  {
    mas_Tst[i_Tst]=reader.ReadDouble();
    i_Tst += 1;
  };

  reader.Close();

  // Общее количество источников
  NS_Tst = i_Tst / 4;
// --------------------------------------------------------------------------
// Выделение радионаправлений

      NS_Tst=objPeleng.RadioRoute(

                   // Входные параметры

                  // Массив входных данных в формате: N,X,Y,F
                  mas_Tst,        
                  1, // РН
                  //2, // РС
                  NS_Tst,
                  dXY_Tst,
                  dF1_Tst,
                  dF2_Tst,
                  dF3_Tst,

                  // Выходные параметры

                  // Массив выходных данных в формате: N,X,Y,F
                  ref mas_Out_Tst,   
                  // Массив в формате: 
                  //// число УС/РС,
                  // число ист-ов в 1-ом УС/РС,
                  // число ист-ов во 2-ом УС/РС, и т.д.
                  ref mas_RN_Tst        


                );

// --------------------------------------------------------------------------
// Окно отображения таблицы

 Form2 form2 = new Form2();
 form2.Show();
// --------------------------------------------------------------------------
// Вывод в таблицу

 form2.dataGridView1.ClearSelection();

 // Добавить пустые строки
 i_Tst = NS_Tst;
 while (i_Tst != 0)
 {
     form2.dataGridView1.Rows.Add("", "", "", "", "", "", "", "");
     i_Tst -= 1;
 };


 // Вывести данные
 i_Tst = 0;
 i1_Tst = 0;
 while (i_Tst < NS_Tst)
 {
     form2.dataGridView1.Rows[(int)i_Tst].Cells[0].Value = mas_Out_Tst[i1_Tst]; // N1
     i1_Tst += 1;
     form2.dataGridView1.Rows[(int)i_Tst].Cells[1].Value = mas_Out_Tst[i1_Tst]; // X1
     i1_Tst += 1;
     form2.dataGridView1.Rows[(int)i_Tst].Cells[2].Value = mas_Out_Tst[i1_Tst]; // Y1
     i1_Tst += 1;
     form2.dataGridView1.Rows[(int)i_Tst].Cells[3].Value = mas_Out_Tst[i1_Tst]; // F1
     i1_Tst += 1;

     form2.dataGridView1.Rows[(int)i_Tst].Cells[4].Value = mas_Out_Tst[i1_Tst]; // N2
     i1_Tst += 1;
     form2.dataGridView1.Rows[(int)i_Tst].Cells[5].Value = mas_Out_Tst[i1_Tst]; // X2
     i1_Tst += 1;
     form2.dataGridView1.Rows[(int)i_Tst].Cells[6].Value = mas_Out_Tst[i1_Tst]; // Y2
     i1_Tst += 1;
     form2.dataGridView1.Rows[(int)i_Tst].Cells[7].Value = mas_Out_Tst[i1_Tst]; // F2
     i1_Tst += 1;

     i_Tst += 1;
 };

// --------------------------------------------------------------------------

    
} // P/P Button14
// ************************************************* P/P Button14(RadioRoute)

// P/P Button15(RadioNet) ***************************************************
// Радиосеть

private void button15_Click(object sender, EventArgs e)
{

    // --------------------------------------------------------------------------
    dXY_Tst = Convert.ToDouble(textBox42.Text);

    dF1_Tst = Convert.ToDouble(textBox43.Text);
    dF2_Tst = Convert.ToDouble(textBox44.Text);
    dF3_Tst = Convert.ToDouble(textBox45.Text);

    dPel_Tst = Convert.ToDouble(textBox46.Text);

    // -------------------------------------------------------------------------
    Peleng objPeleng = new Peleng(str_tmp);
    // --------------------------------------------------------------------------
    // Чтение входного файла

    MessageBox.Show("Enter filename for reading");

    if (
       (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) &&
       (openFileDialog1.FileName.Length > 0)
      )

        flnameIn_Tst = openFileDialog1.FileName;

    BinaryReader reader = new BinaryReader(File.Open(flnameIn_Tst, FileMode.Open));


    i_Tst = 0;

    while (reader.PeekChar() != -1)
    {
        mas_Tst[i_Tst] = reader.ReadDouble();
        i_Tst += 1;
    };

    reader.Close();

    // Общее количество источников
    NS_Tst = i_Tst / 4;
    // --------------------------------------------------------------------------
    // Выделение радионаправлений

    NS_Tst = objPeleng.RadioRoute(

                 // Входные параметры

                // Массив входных данных в формате: N,X,Y,F
                mas_Tst,
                //1, // РН
                2, // РС
                NS_Tst,
                dXY_Tst,
                dF1_Tst,
                dF2_Tst,
                dF3_Tst,

                // Выходные параметры

                // Массив выходных данных в формате: N,X,Y,F
                ref mas_Out_Tst,
                // Массив в формате: 
                //// число УС/РС,
                // число ист-ов в 1-ом УС/РС,
                // число ист-ов во 2-ом УС/РС, и т.д.
                ref mas_RN_Tst


              );

    // --------------------------------------------------------------------------
    // Окно отображения таблицы

    Form3 form3 = new Form3();
    form3.Show();
    // --------------------------------------------------------------------------
    // Вывод в таблицу

    form3.dataGridView1.ClearSelection();

    // Добавить пустые строки
    i_Tst = NS_Tst;
    i2_Tst=0;
 
    while (i_Tst != 0)
     {
        form3.dataGridView1.Rows.Add("", "", "", "", "", "", "", "");
     
        for(i1_Tst=0; i1_Tst<mas_RN_Tst[i2_Tst]; i1_Tst++)
          form3.dataGridView1.Rows.Add("", "", "", "", "", "", "", "");
 
        i_Tst -= 1;
        i2_Tst+=1;
     };

    
    // Вывести данные
    i_Tst = 0;  // Общий счетчик количества РС
    i1_Tst = 0; // счетчик выходного файла
    i2_Tst=0;   // счетчик строк
    while (i_Tst < NS_Tst)
      {
        form3.dataGridView1.Rows[(int)i2_Tst].Cells[0].Value = i_Tst+1; // NРС (1...)
        i2_Tst+=1;

        for(i3_Tst=0; i3_Tst<mas_RN_Tst[i_Tst]; i3_Tst++)
          {
            form3.dataGridView1.Rows[(int)i2_Tst].Cells[0].Value = mas_Out_Tst[i1_Tst]; // N
            i1_Tst += 1;
            form3.dataGridView1.Rows[(int)i2_Tst].Cells[1].Value = mas_Out_Tst[i1_Tst]; // X
            i1_Tst += 1;
            form3.dataGridView1.Rows[(int)i2_Tst].Cells[2].Value = mas_Out_Tst[i1_Tst]; // Y
            i1_Tst += 1;
            form3.dataGridView1.Rows[(int)i2_Tst].Cells[3].Value = mas_Out_Tst[i1_Tst]; // F
            i1_Tst += 1;

            i2_Tst += 1;     
           }

       i_Tst += 1;

      };

// --------------------------------------------------------------------------


} // P/P Button15
// ************************************************* P/P Button15(RadioNet)


// P/P Button19(SelectR) **************************************************
private void button19_Click(object sender, EventArgs e)
{


// Учебный кусок ********************************************************
// !!! Кусок для пробного чтения и записи из текстового файла
// !!! Здесь выбирать либо только чтение либо запись

    /*
    // ----------------------------------------------------------------------

      String strLine = "{123-300}";

    // ----------------------------------------------------------------------
    // Read file


        MessageBox.Show("Select filename for reading");


        if (
           (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) &&
           (openFileDialog1.FileName.Length > 0)
          )

        flname_SelR_Read = openFileDialog1.FileName;

    // ----------------------------------------------------------------------
    // Write file


    //    MessageBox.Show("Enter filename for writing");
    //
    //    if (
    //        (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) &&
    //        (saveFileDialog1.FileName.Length > 0)
    //        )
    //
    //    flname_SelR_Write = saveFileDialog1.FileName;

    // ----------------------------------------------------------------------
       Peleng objPeleng = new Peleng(str_tmp);
    // ----------------------------------------------------------------------

       objPeleng.f_SelectR(

                           // For Writing
                           //flname_SelR_Write,
                           //strLine

                           // For reading
                          flname_SelR_Read


                          );

    // ----------------------------------------------------------------------
    */
    // ******************************************************** Учебный кусок



// Рабочий **************************************************************

// ----------------------------------------------------------------------
// Read file


    if (flName == 0)
    {
        MessageBox.Show("Select filename for reading");


        if (
           (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) &&
           (openFileDialog1.FileName.Length > 0)
          )

            flname_SelR_Read = openFileDialog1.FileName;

        flName = 1;
    }
// ----------------------------------------------------------------------

// !!! WORK

// ----------------------------------------------------------------------
    int kodret;

    InpF = Convert.ToDouble(textBox47.Text);
    InpKodMod = Convert.ToInt32(textBox48.Text);
    InpParMod = Convert.ToDouble(textBox49.Text);
    InpParMod1 = Convert.ToDouble(textBox51.Text);
// ----------------------------------------------------------------------
    //Peleng objPeleng = new Peleng();
    Peleng objPeleng = new Peleng(flname_SelR_Read);

// ----------------------------------------------------------------------
/*
    kodret=objPeleng.f_SelectR(

                     // Файл для чтения БД
                     flname_SelR_Read,

                     // Частота
                     InpF,
                     // Код модуляции
                     InpKodMod,
                     // Параметр модуляции
                     InpParMod,
                     InpParMod1,

                     // Имя РЭС
                     ref OutName

                       );

*/

    OutName = objPeleng.f_SelectR(

                     // Файл для чтения БД
                     //flname_SelR_Read,

                     // Частота
                     InpF,
                     // Код модуляции
                     InpKodMod,
                     // Параметр модуляции
                     InpParMod,
                     InpParMod1

                     // Имя РЭС
                     //ref OutName

                       );

// ---------------------------------------------------------------------
    //if(kodret!=-1)
    //   textBox50.Text = OutName;   
    //else
    //    textBox50.Text = "НЕТ идентификации";

    if(OutName=="")
        textBox50.Text = "НЕТ идентификации";
    else
       textBox50.Text = OutName;   

// ----------------------------------------------------------------------

// ************************************************************** Рабочий



} // P/P Button19
// ************************************************** P/P Button19(SelectR)


// P/P Button16(YS) ******************************************************
// YS

private void button16_Click(object sender, EventArgs e)
{
    // --------------------------------------------------------------------------
    dXY_Tst = Convert.ToDouble(textBox42.Text);

    dF1_Tst = Convert.ToDouble(textBox43.Text);
    dF2_Tst = Convert.ToDouble(textBox44.Text);
    dF3_Tst = Convert.ToDouble(textBox45.Text);

    dPel_Tst = Convert.ToDouble(textBox46.Text);

    // -------------------------------------------------------------------------
    Peleng objPeleng = new Peleng(str_tmp);
    // --------------------------------------------------------------------------
    // Чтение входного файла

    MessageBox.Show("Enter filename for reading");

    if (
       (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) &&
       (openFileDialog1.FileName.Length > 0)
      )

    flnameIn_Tst = openFileDialog1.FileName;

    BinaryReader reader = new BinaryReader(File.Open(flnameIn_Tst, FileMode.Open));


    i_Tst = 0;

    while (reader.PeekChar() != -1)
    {
        mas_Tst[i_Tst] = reader.ReadDouble();
        i_Tst += 1;
    };

    reader.Close();

    // Общее количество источников
    NS_Tst = i_Tst / 4;
    // --------------------------------------------------------------------------
    // Выделение УС

    NS_Tst = objPeleng.CommunicationCenter(

                 // Входные параметры

                // Массив входных данных в формате: N,X,Y,F
                mas_Tst,
                NS_Tst,
                dXY_Tst,
                dF1_Tst,
                dF2_Tst,
                dF3_Tst,

                // Выходные параметры

                // Массив выходных данных в формате: N,X,Y,F
                ref mas_Out_Tst,
                // Массив в формате: 
                //// число УС/РС,
                // число ист-ов в 1-ом УС/РС,
                // число ист-ов во 2-ом УС/РС, и т.д.
                ref mas_RN_Tst

              );

    // --------------------------------------------------------------------------
    // Окно отображения таблицы

    Form4 form4 = new Form4();
    form4.Show();
    // --------------------------------------------------------------------------
    // Вывод в таблицу

    form4.dataGridView1.ClearSelection();

    // Добавить пустые строки
    i_Tst = NS_Tst;
    i2_Tst = 0;

    while (i_Tst != 0)
    {
        form4.dataGridView1.Rows.Add("", "", "", "", "", "", "", "");

        for (i1_Tst = 0; i1_Tst < mas_RN_Tst[i2_Tst]; i1_Tst++)
            form4.dataGridView1.Rows.Add("", "", "", "", "", "", "", "");

        i_Tst -= 1;
        i2_Tst += 1;
    };


    // Вывести данные
    i_Tst = 0;  // Общий счетчик количества УС
    i1_Tst = 0; // счетчик выходного файла
    i2_Tst = 0;   // счетчик строк
    while (i_Tst < NS_Tst)
    {
        form4.dataGridView1.Rows[(int)i2_Tst].Cells[0].Value = i_Tst + 1; // NРС (1...)
        i2_Tst += 1;

        for (i3_Tst = 0; i3_Tst < mas_RN_Tst[i_Tst]; i3_Tst++)
        {
            form4.dataGridView1.Rows[(int)i2_Tst].Cells[0].Value = mas_Out_Tst[i1_Tst]; // N
            i1_Tst += 1;
            form4.dataGridView1.Rows[(int)i2_Tst].Cells[1].Value = mas_Out_Tst[i1_Tst]; // X
            i1_Tst += 1;
            form4.dataGridView1.Rows[(int)i2_Tst].Cells[2].Value = mas_Out_Tst[i1_Tst]; // Y
            i1_Tst += 1;
            form4.dataGridView1.Rows[(int)i2_Tst].Cells[3].Value = mas_Out_Tst[i1_Tst]; // F
            i1_Tst += 1;

            i2_Tst += 1;
        }

        i_Tst += 1;

    };

    // --------------------------------------------------------------------------

} // P/P Button16
// ****************************************************** P/P Button16(YS)

// P/P Button17(RR_Pel) **************************************************
// Radio Route (Peleng)

private void button17_Click(object sender, EventArgs e)
{
    // --------------------------------------------------------------------------
    dXY_Tst = Convert.ToDouble(textBox42.Text);

    dF1_Tst = Convert.ToDouble(textBox43.Text);
    dF2_Tst = Convert.ToDouble(textBox44.Text);
    dF3_Tst = Convert.ToDouble(textBox45.Text);

    dPel_Tst = Convert.ToDouble(textBox46.Text);

    // -------------------------------------------------------------------------
    Peleng objPeleng = new Peleng(str_tmp);
    // --------------------------------------------------------------------------
    // Чтение входного файла

    MessageBox.Show("Enter filename for reading");

    if (
       (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) &&
       (openFileDialog1.FileName.Length > 0)
      )

        flnameIn_Tst = openFileDialog1.FileName;

    BinaryReader reader = new BinaryReader(File.Open(flnameIn_Tst, FileMode.Open));


    i_Tst = 0;

    while (reader.PeekChar() != -1)
    {
        mas_Tst[i_Tst] = reader.ReadDouble();
        i_Tst += 1;
    };

    reader.Close();

    // Общее количество источников
    NS_Tst = i_Tst / 4;
    // --------------------------------------------------------------------------
    // Выделение радионаправлений

    NS_Tst = objPeleng.PelRadioRoute(

                 // Входные параметры

                // Массив входных данных в формате: N,X,Y,F
                mas_Tst,
                1, // РН
                //2, // РС
                NS_Tst,
                dXY_Tst,
                dF1_Tst,
                dF2_Tst,
                dF3_Tst,
                dPel_Tst,

                // Выходные параметры

                // Массив выходных данных в формате: N,X,Y,F
                ref mas_Out_Tst,
                // Массив в формате: 
                //// число УС/РС,
                // число ист-ов в 1-ом УС/РС,
                // число ист-ов во 2-ом УС/РС, и т.д.
                ref mas_RN_Tst


              );

    // --------------------------------------------------------------------------
    // Окно отображения таблицы

    Form5 form5 = new Form5();
    form5.Show();
    // --------------------------------------------------------------------------
    // Вывод в таблицу

    form5.dataGridView1.ClearSelection();

    // Добавить пустые строки
    i_Tst = NS_Tst;
    while (i_Tst != 0)
    {
        form5.dataGridView1.Rows.Add("", "", "", "", "", "");
        i_Tst -= 1;
    };


    // Вывести данные
    i_Tst = 0;
    i1_Tst = 0;
    while (i_Tst < NS_Tst)
    {
        form5.dataGridView1.Rows[(int)i_Tst].Cells[0].Value = mas_Out_Tst[i1_Tst]; // N1
        i1_Tst += 1;
        form5.dataGridView1.Rows[(int)i_Tst].Cells[1].Value = mas_Out_Tst[i1_Tst]; // Pel1
        i1_Tst += 1;
        i1_Tst += 1; // 0 пропускаем
        form5.dataGridView1.Rows[(int)i_Tst].Cells[2].Value = mas_Out_Tst[i1_Tst]; // F1
        i1_Tst += 1;

        form5.dataGridView1.Rows[(int)i_Tst].Cells[3].Value = mas_Out_Tst[i1_Tst]; // N2
        i1_Tst += 1;
        form5.dataGridView1.Rows[(int)i_Tst].Cells[4].Value = mas_Out_Tst[i1_Tst]; // Pel2
        i1_Tst += 1;
        i1_Tst += 1; // 0 пропускаем
        form5.dataGridView1.Rows[(int)i_Tst].Cells[5].Value = mas_Out_Tst[i1_Tst]; // F2
        i1_Tst += 1;

        i_Tst += 1;
    };

// --------------------------------------------------------------------------

} // P/P Button17
// ************************************************** P/P Button17(RR_Pel)

// P/P Button18 (RadioNet_Peleng) ******************************************
// RadioNet (Peleng)

private void button18_Click(object sender, EventArgs e)
{
// --------------------------------------------------------------------------
    dXY_Tst = Convert.ToDouble(textBox42.Text);

    dF1_Tst = Convert.ToDouble(textBox43.Text);
    dF2_Tst = Convert.ToDouble(textBox44.Text);
    dF3_Tst = Convert.ToDouble(textBox45.Text);

    dPel_Tst = Convert.ToDouble(textBox46.Text);

// -------------------------------------------------------------------------
    Peleng objPeleng = new Peleng(str_tmp);
// -------------------------------------------------------------------------
// Чтение входного файла

    MessageBox.Show("Enter filename for reading");

    if (
       (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) &&
       (openFileDialog1.FileName.Length > 0)
      )

        flnameIn_Tst = openFileDialog1.FileName;

    BinaryReader reader = new BinaryReader(File.Open(flnameIn_Tst, FileMode.Open));


    i_Tst = 0;

    while (reader.PeekChar() != -1)
    {
        mas_Tst[i_Tst] = reader.ReadDouble();
        i_Tst += 1;
    };

    reader.Close();

    // Общее количество источников
    NS_Tst = i_Tst / 4;
// --------------------------------------------------------------------------
// Выделение радиосетей

    NS_Tst = objPeleng.PelRadioRoute(

                 // Входные параметры

                // Массив входных данных в формате: N,X,Y,F
                mas_Tst,
                //1, // РН
                2, // РС
                NS_Tst,
                dXY_Tst,
                dF1_Tst,
                dF2_Tst,
                dF3_Tst,
                dPel_Tst,

                // Выходные параметры

                // Массив выходных данных в формате: N,X,Y,F
                ref mas_Out_Tst,
                // Массив в формате: 
                //// число УС/РС,
                // число ист-ов в 1-ом УС/РС,
                // число ист-ов во 2-ом УС/РС, и т.д.
                ref mas_RN_Tst

              );

// --------------------------------------------------------------------------
// Окно отображения таблицы

    Form6 form6 = new Form6();
    form6.Show();
// --------------------------------------------------------------------------
// Вывод в таблицу

    form6.dataGridView1.ClearSelection();

    // Добавить пустые строки
    i_Tst = NS_Tst;
    i2_Tst = 0;

    while (i_Tst != 0)
    {
        form6.dataGridView1.Rows.Add("", "", "");

        for (i1_Tst = 0; i1_Tst < mas_RN_Tst[i2_Tst]; i1_Tst++)
            form6.dataGridView1.Rows.Add("", "", "");

        i_Tst -= 1;
        i2_Tst += 1;
    };

    // Вывести данные
    i_Tst = 0;  // Общий счетчик количества РС
    i1_Tst = 0; // счетчик выходного файла
    i2_Tst = 0;   // счетчик строк
    while (i_Tst < NS_Tst)
    {
        form6.dataGridView1.Rows[(int)i2_Tst].Cells[0].Value = i_Tst + 1; // NРС (1...)
        i2_Tst += 1;

        for (i3_Tst = 0; i3_Tst < mas_RN_Tst[i_Tst]; i3_Tst++)
        {
            form6.dataGridView1.Rows[(int)i2_Tst].Cells[0].Value = mas_Out_Tst[i1_Tst]; // N
            i1_Tst += 1;
            form6.dataGridView1.Rows[(int)i2_Tst].Cells[1].Value = mas_Out_Tst[i1_Tst]; // Pel
            i1_Tst += 1;
            i1_Tst += 1; // Пропускаем 0
            form6.dataGridView1.Rows[(int)i2_Tst].Cells[2].Value = mas_Out_Tst[i1_Tst]; // F
            i1_Tst += 1;

            i2_Tst += 1;
        }

        i_Tst += 1;

    };

// --------------------------------------------------------------------------


}  // P/P Button18
// ****************************************** P/P Button18 (RadioNet_Peleng)



} // Class Form1
// ***********************************************************************

} // namespace PelengApplication
