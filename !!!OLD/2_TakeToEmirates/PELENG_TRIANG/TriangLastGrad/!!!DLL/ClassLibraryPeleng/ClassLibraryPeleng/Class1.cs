﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
//using System.Threading.Tasks;



namespace ClassLibraryPeleng
{

// !!! Examples of structs **************************************************
// Примеры структур

/*
// ......................................................................
    // Просто структура

    public struct st
    {

        public double a;
        public double b;

        public st(double a, double b)
        {
            this.a = 0;
            this.b = 0;
        }
    }
// ......................................................................
    // Структура с массивом

    public struct sr
    {

        public double[] mm;

    }
// ......................................................................
    // Структура с вложенной структурой

    public struct ss
    {

        public st st1;

    }

// ......................................................................
*/

// ************************************************** !!! Examples of structs



// Структуры ************************************************************

// ---------------------------------------------------------------------
// Структура БД

public struct BD
 {
    // Диапазон частот
    public double Fmin;
    public double Fmax;

    // Коды модуляции
    //public int[] kod_mod;

    // Код модуляции
    public int kod_mod;

    // Параметры модуляции:длительность импульса/ширина спектра
    public double Par1_Min; // длительность импульса
    public double Par1_Max;
    public double Par2_Min; // ширина спектра
    public double Par2_Max;


 }
// ---------------------------------------------------------------------
/*
// Структура параметров модуляции

public struct ParMod
{
    public int kod_mod1;
    public double ParMin;
    public double ParMax;

}
*/
// ---------------------------------------------------------------------

// ************************************************************ Структуры

    public class Peleng
    {
// ***********************************************************************
// Расчет траектории по пеленгу
// Нахождение ИРИ методом триангуляции
// ***********************************************************************

// ПЕРЕМЕННЫЕ VARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVAR

// Пеленг PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP

	   // Флаг НУ
 	   private int fl_first_Pel;
      
	   // Вспомогательные флаги
	   private int fl1_Pel;

	   // Индекс фиктивной точки (1,2, ...)
       private uint IndFP_Pel;
	   // Индекс i-й фиктивной точки в массивах(0,...)
       private uint IndMas_Pel; 

       // Расстояние по поверхности земли от точки стояния пеленгатора до текущей 
       // фиктивной точки 
       private double Mi_Pel;
       // Расстояние по прямой от точки стояния пеленгатора до пересечения прямой
       // (проходящей через текущую фиктивную точку и начало координат)и плоскости,
       // касательной в точке стояния пеленгатора
       private double Di_Pel;

	   // Радиус Земли (шар)
	   private double REarth_Pel;
	   // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
	   private double REarthP_Pel;
	   // Радиус Земли  в (i-1)-й фиктивной точке с учетом текущей широты 
	   //double REarthF_Pel;

	   // Координаты фиктивной точки в СК пеленгатора (СКП)
	   private double Xi_Pel;
	   private double Yi_Pel;
	   private double Zi_Pel;

	   // Координаты фиктивной точки в СК1 (Поворот СКП на угол=широте пеленгатора)
	   private double Xi0_Pel;
	   private double Yi0_Pel;
	   private double Zi0_Pel;

	   // Координаты фиктивной точки в опорной геоцентрической СК
	   // (Поворот СК1 на угол=долготе пеленгатора)
	   private double XiG_Pel;
	   private double YiG_Pel;
	   private double ZiG_Pel;

	   // Угловые координаты фиктивной точки в опорной геоцентрической СК
	   private double Ri_Pel;
	   private double LATi_Pel;
	   private double LONGi_Pel;

       // Координаты точки пересечения прямой (проходящей через текущую фиктивную
	   // точку и начало координат) и плоскости касательной в точке стояния 
	   // пеленгатора в опорной геоцентрической СК
	   private double XiG1_Pel;
	   private double YiG1_Pel;
	   private double ZiG1_Pel;
	   private double Ri1_Pel;

       // Центральный угол дуги Mi
       private double Alpha;

	   // Широта и долгота стояния пеленгатора
       private double LatP_Pel_tmp;
       private double LongP_Pel_tmp;

   	  // Количество фиктивных точек в плоскости пеленгования пеленгатора
       private uint NumbFikt_Pel_tmp;

	  // Max дальность отображения пеленга
       private double Mmax_Pel_tmp;     

       // Пеленг
       private double Theta_Pel_tmp;

       // Координаты пеленгатора в опорной геоцентрической СК
       private double XP_Pel_tmp;
       private double YP_Pel_tmp;
       private double ZP_Pel_tmp;

       // Счетчик
       private int sch_Pel;

       // ----------------------------------------------------------------
       // !!! VAR2

       private double Lat_Pel_N;  //Lat(n)
       private double Lat_Pel_N1;  //Lat(n-1)
       private double Long_Pel_N;  //Long(n)
       private double Long_Pel_N1;  //Long(n-1)
       private double V_Pel;

       private int fl_var2;
       private int fl1_var2;
       private int fl2_var2;

       // ----------------------------------------------------------------

// PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP Пеленг

// Триангуляция TriangTriangTriangTriangTriangTriangTriangTriangTriangTrian
	   // Флаг НУ
 	   private int fl_first_Tr;

	   // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
	   private double REarthP1_Tr;
	   private double REarthP2_Tr;

      // Расчетные коэффициенты
      private double K1_Tr;
      private double K2_Tr;
      private double K3_Tr;

	  // Коэффициенты канонического уравнения для плоскости пеленгования ИРИ
	  // i-ым пеленгатором 
	  private double A1_Tr; // Для пеленгатора1
	  private double T1_Tr;
	  private double C1_Tr;
	  private double A2_Tr; // Для пеленгатора2
	  private double T2_Tr;
	  private double C2_Tr;

	  // Координаты искомого ИРИ в опорной геоцентрической СК
      private double XIRI1_Tr;
      private double XIRI2_Tr;
      private double YIRI1_Tr;
      private double YIRI2_Tr;
      private double ZIRI1_Tr;
      private double ZIRI2_Tr;

      // Координаты середины отрезка, соединяющего N-ые фиктивные точки
	  private double XM_Tr;
	  private double YM_Tr;
	  private double ZM_Tr;

      // Расстояние до ПП
	  private double L1_Tr;
	  private double L2_Tr;

       // Координаты пеленгатора в опорной геоцентрической СК
        private double XP1_Tr_tmp;
        private double YP1_Tr_tmp;
        private double ZP1_Tr_tmp;
        private double XP2_Tr_tmp;
        private double YP2_Tr_tmp;
        private double ZP2_Tr_tmp;

        // Координаты N-й фиктивной точки в опорной геоцентрической СК
        private double XFN1_Tr_tmp;
        private double YFN1_Tr_tmp;
        private double ZFN1_Tr_tmp;
        private double XFN2_Tr_tmp;
        private double YFN2_Tr_tmp;
        private double ZFN2_Tr_tmp;

// TriangTriangTriangTriangTriangTriangTriangTriangTriangTrian Триангуляция

// Координаты ************************************************************

      private double Pi;

      // Число угловых секунд в радиане
      private double ro;

      // Эллипсоид Красовского
      private double aP;  // Большая полуось
      private double a1P; // Сжатие
      private double e2P; // Квадрат эксцентриситета
     
      // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
      private double aW;  // Большая полуось
      private double a1W; // Сжатие
      private double e2W; // Квадрат эксцентриситета

      // Вспомогательные значения для преобразования эллипсоидов
      private double a;  
      private double da; 
      private double e2; 
      private double de2; 

      // Линейные элементы трансформирования, в метрах
      //private double dx;  
      //private double dy;  
      //private double dz;  

      // Угловые элементы трансформирования, в секундах
      private double wx;  
      private double wy;  
      private double wz;  

      // Дифференциальное различие масштабов
      private double ms;  

	   // Флаг НУ
 	   private int fl_first1_Coord;
 	   private int fl_first2_Coord;
 	   private int fl_first3_Coord;
       private int fl_first5_Coord;
       private int fl_first6_Coord;
       private int fl_first7_Coord;
       private int fl_first8_Coord;

// ************************************************************ Координаты

// ИдентификацияРЭС ******************************************************
// Флаг инициализации БД

      private int flInitBD;
      private string strFileName;
// ****************************************************** ИдентификацияРЭС


// VARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVARVAR ПЕРЕМЕННЫЕ

// Конструктор ***********************************************************
// Конструктор

 public Peleng(string InputFileName)
 {

// Пеленг PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP
// Флаг НУ
 	   fl_first_Pel=0;

   	   // Вспомогательные флаги
	   fl1_Pel=0;

	   // Индекс фиктивной точки (1,2, ...)
       IndFP_Pel=1;
	   // Индекс i-й фиктивной точки в массивах(0,...)
       IndMas_Pel=0; 

       // Расстояние по поверхности земли от точки стояния пеленгатора до текущей 
       // фиктивной точки 
       Mi_Pel=0;
       // Расстояние по прямой от точки стояния пеленгатора до пересечения прямой
       // (проходящей через текущую фиктивную точку и начало координат)и плоскости,
       // касательной в точке стояния пеленгатора
       Di_Pel=0;

	   // Радиус Земли (шар)
	   REarth_Pel=6378245;
	   // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
	   REarthP_Pel=0;
	   // Радиус Земли  в (i-1)-й фиктивной точке с учетом текущей широты 
	   //REarthF_Pel=0;

	   // Координаты фиктивной точки в СК пеленгатора
	   Xi_Pel=0;
	   Yi_Pel=0;
	   Zi_Pel=0;

	   // Координаты фиктивной точки в СК1 (Поворот СКП на угол=широте пеленгатора)
	   Xi0_Pel=0;
	   Yi0_Pel=0;
	   Zi0_Pel=0;

	   // Координаты фиктивной точки в опорной геоцентрической СК
	   // (Поворот СК1 на угол=долготе пеленгатора)
	   XiG_Pel=0;
	   YiG_Pel=0;
	   ZiG_Pel=0;

	   // Угловые координаты фиктивной точки в опорной геоцентрической СК
	   Ri_Pel=0;
	   LATi_Pel=0;
	   LONGi_Pel=0;

       // Координаты точки пересечения прямой (проходящей через текущую фиктивную
	   // точку и начало координат) и плоскости касательной в точке стояния 
	   // пеленгатора в опорной геоцентрической СК
	   XiG1_Pel=0;
	   YiG1_Pel=0;
	   ZiG1_Pel=0;
	   Ri1_Pel=0;

       // Центральный угол дуги Mi
       Alpha=0;

	   // Широта и долгота стояния пеленгатора
       LatP_Pel_tmp=0;
       LongP_Pel_tmp=0;

   	  // Количество фиктивных точек в плоскости пеленгования пеленгатора
       NumbFikt_Pel_tmp=0;

	  // Max дальность отображения пеленга
       Mmax_Pel_tmp=0;     

       // Пеленг
       Theta_Pel_tmp=0;

       // Счетчик
       sch_Pel=0;

       // Координаты пеленгатора в опорной геоцентрической СК
       XP_Pel_tmp=0;
       YP_Pel_tmp=0;
       ZP_Pel_tmp=0;

       // ----------------------------------------------------------------
       // !!! VAR2

       Lat_Pel_N=0;  //Lat(n)
       Lat_Pel_N1=0;  //Lat(n-1)
       Long_Pel_N=0;  //Long(n)
       Long_Pel_N1=0;  //Long(n-1)
       V_Pel=0;

       fl_var2=0;
       fl1_var2 = 0;
       fl2_var2 = 0;

       // ----------------------------------------------------------------

// PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP Пеленг

// Триангуляция TriangTriangTriangTriangTriangTriangTriangTriangTriangTrian
       // Флаг НУ
 	   fl_first_Tr=0;

	   // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
	   REarthP1_Tr=0;
	   REarthP2_Tr=0;

      // Расчетные коэффициенты
      K1_Tr=0;
      K2_Tr=0;
      K3_Tr=0;

	  // Коэффициенты канонического уравнения для плоскости пеленгования ИРИ
	  // i-ым пеленгатором 
	  A1_Tr=0; // Для пеленгатора1
	  T1_Tr=0;
	  C1_Tr=0;
	  A2_Tr=0; // Для пеленгатора2
	  T2_Tr=0;
	  C2_Tr=0;

	  // Координаты искомого ИРИ в опорной геоцентрической СК
      XIRI1_Tr=0;
      XIRI2_Tr=0;
      YIRI1_Tr=0;
      YIRI2_Tr=0;
      ZIRI1_Tr=0;
      ZIRI2_Tr=0;

      // Координаты середины отрезка, соединяющего N-ые фиктивные точки
	  XM_Tr=0;
	  YM_Tr=0;
	  ZM_Tr=0;

	  L1_Tr=0;
	  L2_Tr=0;

       // Координаты пеленгатора в опорной геоцентрической СК
      XP1_Tr_tmp = 0;
      YP1_Tr_tmp = 0;
      ZP1_Tr_tmp = 0;
      XP2_Tr_tmp = 0;
      YP2_Tr_tmp = 0;
      ZP2_Tr_tmp = 0;

        // Координаты N-й фиктивной точки в опорной геоцентрической СК
      XFN1_Tr_tmp = 0;
      YFN1_Tr_tmp = 0;
      ZFN1_Tr_tmp = 0;
      XFN2_Tr_tmp = 0;
      YFN2_Tr_tmp = 0;
      ZFN2_Tr_tmp = 0;

// TriangTriangTriangTriangTriangTriangTriangTriangTriangTrian Триангуляция

// Координаты ************************************************************

      Pi = 3.14159265358979; 

      // Число угловых секунд в радиане
      ro=206264.8062;

      // Эллипсоид Красовского
      aP=6378245;  // Большая полуось
      a1P=0; // Сжатие
      e2P=0; // Квадрат эксцентриситета
     
      // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
      aW=6378137;  // Большая полуось
      a1W=0; // Сжатие
      e2W=0; // Квадрат эксцентриситета

      // Вспомогательные значения для преобразования эллипсоидов
      a=0;  
      da=0; 
      e2=0; 
      de2=0; 

     // ....................................................................
/*
      // Линейные элементы трансформирования, в метрах
      dx=23.92;  
      dy=-141.27;  
      dz=-80.9;  

      // Угловые элементы трансформирования, в секундах
      wx=0;  
      wy=0;  
      wz=0;  

      // Дифференциальное различие масштабов
      ms=0;
 */

      // ГОСТ 51794_2001
      //dx = 23.92;
      //dy = -141.27;
      //dz = -80.9;
      //wx = 0;
      //wy = 0.35;
      //wz = 0.82;
      //ms = -0.000012;

      // ГОСТ 51794_2008
      //dx = 25;
      //dy = -141;
      //dz = -80;
      //wx=0; 
      //wy = -0.35;
      //wz = -0.66;
      //ms = 0;
      //dx = 28;
      //dy = -130;
      //dz = -95;
      wx = 0;
      wy = 0;
      wz = 0;
      ms = 0;

      //dx = 28;
      //dy = -130;
      //dz = -95;

      // ....................................................................

      // Флаг НУ
 	  fl_first1_Coord=0;
 	  fl_first2_Coord=0;
 	  fl_first3_Coord=0;
      fl_first5_Coord = 0;
      fl_first6_Coord = 0;
      fl_first7_Coord = 0;
      fl_first8_Coord = 0;

// ************************************************************ Координаты

// ИдентификацияРЭС ******************************************************
// Флаг инициализации БД

      flInitBD=0;

      strFileName = InputFileName;
// ****************************************************** ИдентификацияРЭС


 } // Конструктор Peleng
// *********************************************************** Конструктор

// ***********************************************************************
    // Module
// ***********************************************************************
    public double module(double a)                   
     { 
        if (a >= 0) return (a);
        else        return (-a);
     }
// ***********************************************************************

// *************************************************************************
// Убрать источник(4элемента) из массива
// indtek - текущий индекс, с которого убирать,
// numbDel- кол-во элементов от текущего указателя до конца
// *************************************************************************
public void Del_Source(
                       uint numbDel,
                       uint indtek,
                       ref double [] mas_Tst
       
                       )
     {
// ----------------------------------------------------------------------------
      uint ind_del,ind_del4;
      uint numbDel_dubl;

      uint ipmas_dubl;
// ----------------------------------------------------------------------------
      ipmas_dubl = indtek;

      numbDel_dubl=numbDel;
// ----------------------------------------------------------------------------
     // FOR4
     for(ind_del4=0;ind_del4<4;ind_del4++)
     {
// ----------------------------------------------------------------------------
       // FOR
       for(ind_del=0;ind_del<(numbDel_dubl-1);ind_del++)
       {
         mas_Tst[ipmas_dubl] = mas_Tst[ipmas_dubl + 1];

         ipmas_dubl+=1;

       } // FOR
// ----------------------------------------------------------------------------
      numbDel_dubl--;

      ipmas_dubl = indtek;

// ----------------------------------------------------------------------------
      } // FOR4

     } // P/P
// *************************************************************************

// ***********************************************************************
// Определение координаты середины отрезка
// ***********************************************************************

public void DefMiddle_Pel(
                    double X1,
                    double X2,
                    ref double XM

                          )
{

    // -------------------------------------------------------------------
    if (X1 == 0)
        XM = X2 / 2;
    // -------------------------------------------------------------------
    else if (X2 == 0)
        XM = X1 / 2;
    // -------------------------------------------------------------------
    else if ((X1 > 0) && (X2 > 0))
    {
        if (X2 > (X1))
            XM = X1 + ((X2 - (X1)) / 2);
        else if (X1 > (X2))
            XM = X2 + ((X1 - (X2)) / 2);
        else
            XM = X1 / 2;
    }
    // -------------------------------------------------------------------
    else if ((X1 < 0) && (X2 < 0))
    {
        if (X2 < (X1))
            XM = X1 - ((module(X2) - module(X1)) / 2);
        else if (X1 < (X2))
            XM = X2 - ((module(X1) - module(X2)) / 2);
        else
            XM = X1 / 2;
    }
    // -------------------------------------------------------------------
    else if ((X1 > 0) && (X2 < 0))
    {
        if (module(X1) > module(X2))
            XM = (module(X2) + module(X1)) / 2;
        else if (module(X1) < module(X2))
            XM = -(module(X2) + module(X1)) / 2;
        else
            XM = 0;
    }
    // -------------------------------------------------------------------
    else
    {
        if (module(X2) > module(X1))
            XM = (module(X2) + module(X1)) / 2;
        else if (module(X2) < module(X1))
            XM = -(module(X2) + module(X1)) / 2;
        else
            XM = 0;
    }
    // -------------------------------------------------------------------

} // Функция DefMiddle_Pel
// ***********************************************************************

// ***********************************************************************
// Расчет долготы
// 0...360 к востоку от Гринвича
// ***********************************************************************

public void Def_Longitude(
                    double X,
                    double Y,
                    double R,
                    double Lat,
                    ref double Long

                          )
{
    double Long1;

    // -------------------------------------------------------------------
    if (module(R * Math.Cos(Lat)) > 1E-10)
        Long1 = Math.Asin(module(Y) / (R * Math.Cos(Lat)));
    else
        Long1 = Math.Asin(module(Y) / 1E-10);

    // -------------------------------------------------------------------
    if ((Y == 0) && (X > 0))
        Long = 0;
    // -------------------------------------------------------------------
    else if ((Y == 0) && (X < 0))
        Long = Math.PI;
    // -------------------------------------------------------------------
    else if ((Y > 0) && (X > 0))
        Long = Long1;
    // -------------------------------------------------------------------
    else if ((Y < 0) && (X > 0))
        Long = 2 * Math.PI - Long1;
    // -------------------------------------------------------------------
    else if ((Y > 0) && (X < 0))
        Long = Math.PI - Long1;
    // ------------------------------------------------------------------
    else // X<0 Y<0
        Long = Math.PI + Long1;
    // -------------------------------------------------------------------

} // Функция Def_Longitude
// ***********************************************************************

// ***********************************************************************
// Расчет долготы
// [0...180] к востоку от Гринвича
// ]0...-180[ к западу от Гринвича
// Широта -90(юг)...+90(север)
// ***********************************************************************

public void Def_Longitude_180(
                    double X,
                    double Y,
                    double R,
                    double Lat, // rad
                    ref double Long

                          )
{
    double Long1;

    // -------------------------------------------------------------------
    if (module(R * Math.Cos(Lat)) > 1E-10)
        Long1 = Math.Asin(module(Y) / (R * Math.Cos(Lat)));
    else
        Long1 = Math.Asin(module(Y) / 1E-10);

    // -------------------------------------------------------------------
    if ((Y == 0) && (X > 0))
        Long = 0;
    // -------------------------------------------------------------------
    else if ((Y == 0) && (X < 0))
        Long = Math.PI;
    // -------------------------------------------------------------------
    else if ((Y > 0) && (X > 0))
        Long = Long1;
    // -------------------------------------------------------------------
    else if ((Y < 0) && (X > 0))
        Long =  - Long1;
    // -------------------------------------------------------------------
    else if ((Y > 0) && (X < 0))
        Long = Math.PI - Long1;
    // ------------------------------------------------------------------
    else // X<0 Y<0
        Long = -(Math.PI - Long1);
    // -------------------------------------------------------------------

} // Функция Def_Longitude_180
// ***********************************************************************

// ***********************************************************************
// Отрисовка траектории N фиктивных точек по пеленгу
//
// Входные параметры: 
// Theta_Pel(град) - пеленг,
// Mmax_Pel(км) - максимальная дальность отображения пеленга,
// NumberFikt_Pel - количество фиктивных точек
// LatP_Pel,LongP_Pel (град) - широта и долгота стояния пеленгатора 

//
// Выходные параметры: 
// массив mas_Pel(R(км),Latitude(град),Longitude(град)) ->
// дальность, широта, долгота
// массив mas_Pel_XYZ (км) -> координаты в опорной геоцентрической СК
// XP_Pel,YP_Pel,ZP_Pel (км) -координаты пеленгатора в опорной геоцентрической СК
// XFN_Pel,YFN_Pel,ZFN_Pel (км) -координаты N-й фиктивной точки в опорной 
// геоцентрической СК

// Широта -90(юг)...+90(север)
// Долгота -180(на запад от Гринвича)...+180(на восток от Гринвича)
// Пеленг 0...360(по часовой стрелке)
// ***********************************************************************
public void f_Peleng
    (

         // Пеленг
         double Theta_Pel,
         // Max дальность отображения пеленга
         double Mmax_Pel,
         // Количество фиктивных точек в плоскости пеленгования пеленгатора
         uint NumbFikt_Pel,

         // Широта и долгота стояния пеленгатора
         double LatP_Pel,
         double LongP_Pel,

         // Координаты пеленгатора в опорной геоцентрической СК
         ref double XP_Pel,
         ref double YP_Pel,
         ref double ZP_Pel,

         // Координаты N-й фиктивной точки в опорной геоцентрической СК
         ref double XFN_Pel,
         ref double YFN_Pel,
         ref double ZFN_Pel,

         // Координаты фиктивных точек
         ref double[] mas_Pel,
         ref double[] mas_Pel_XYZ

         // ForOtl
         // Time of model
         //double *pT,
         // Discrete of time
         //double *pdt,
         //double *pmas_Otl_Pel        


    )
{

   // .........................................................................
    double yyy;
    int jjj;
    int lngp_i,lngi_i,lngpi_i;
    double lngp_d,lngi_d,lngpi_d;


    jjj = 0;
    lngp_i = 0;
    lngp_d = 0;
    lngi_i = 0;
    lngi_d = 0;
    lngpi_i = 0;
    lngpi_d = 0;
    // .........................................................................


    // Initial conditions ****************************************************
    // Initial conditions

    if (fl_first_Pel == 0)
    {


        //0223
        if (Theta_Pel == 360)
            Theta_Pel = 359.999;
        if (Theta_Pel == 0)
            Theta_Pel = 0.001;

        // ...............................................................
        // Широта и долгота стояния пеленгатора
        //0223

        LatP_Pel_tmp = (LatP_Pel * Math.PI) / 180;   // grad->rad
        LongP_Pel_tmp = (LongP_Pel * Math.PI) / 180;
        // ...............................................................



        // ...............................................................
        // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 

        // m
        //REarthP_Pel = REarth_Pel*(1.-0.003352*sin(*pLatP_Pel)*sin(*pLatP_Pel));
        REarthP_Pel = REarth_Pel; // Шар

        //0223
        REarthP_Pel = REarth_Pel * (1 - 0.003352 * Math.Sin(LatP_Pel_tmp) * Math.Sin(LatP_Pel_tmp));

        // ...............................................................

        fl_first_Pel = 1;
        // ...............................................................
        // Широта и долгота стояния пеленгатора

        //0223
        //LatP_Pel_tmp = (LatP_Pel * Math.PI)/180;   // grad->rad
        //LongP_Pel_tmp = (LongP_Pel * Math.PI)/180;
        // ...............................................................
        // Количество фиктивных точек в плоскости пеленгования пеленгатора

        NumbFikt_Pel_tmp = NumbFikt_Pel;
        // ...............................................................
        // Max дальность отображения пеленга

        Mmax_Pel_tmp = Mmax_Pel*1000; // km-> m
        // ...............................................................
        // Координаты пеленгатора в опорной геоцентрической СК

        // m
        XP_Pel_tmp = REarthP_Pel * Math.Cos(LatP_Pel_tmp) * Math.Cos(LongP_Pel_tmp);
        YP_Pel_tmp = REarthP_Pel * Math.Cos(LatP_Pel_tmp) * Math.Sin(LongP_Pel_tmp);
        ZP_Pel_tmp = REarthP_Pel * Math.Sin(LatP_Pel_tmp);

        // km
        XP_Pel = XP_Pel_tmp / 1000;
        YP_Pel = YP_Pel_tmp / 1000;
        ZP_Pel = ZP_Pel_tmp / 1000;
        // ...............................................................
        // Пеленг

        // grad -> rad
        Theta_Pel_tmp = (Theta_Pel*Math.PI)/180;
        // ...............................................................

        // ...............................................................
        // VAR2

        //0226
        //V_Pel = Mmax_Pel_tmp / NumbFikt_Pel;
        //V_Pel = 200;
        V_Pel = 100;



        Lat_Pel_N1 = LatP_Pel_tmp;
        Long_Pel_N1 = LongP_Pel_tmp;
        fl_var2=0;
        fl1_var2 = 0;
        fl2_var2 = 0;
        // ...............................................................

    }
    // **************************************************** Initial conditions

    while (IndMas_Pel < (NumbFikt_Pel))
    {


/*
        // VAR1 *******************************************************************
        //yyy = Math.Acos((Math.Sin(LATi_Pel) -Math.Sin(LatP_Pel_tmp)*Math.Cos(Alpha))/
        //(Math.Cos(LatP_Pel_tmp)*Math.Sin(Alpha)));
        //yyy = Math.Acos(Math.Sin(LatP_Pel)/Math.Cos(LATi_Pel)); 

        // Mi ********************************************************************
        // Расстояние по поверхности земли от точки стояния пеленгатора до текущей 
        // фиктивной точки (Mi-дуга болшьшого круга с центральным углом Mi/Rз радиан)

        Mi_Pel = (((double)IndFP_Pel) * Mmax_Pel_tmp) / ((double)(NumbFikt_Pel_tmp));

        // Центральный угол дуги Mi
        Alpha = Mi_Pel / REarthP_Pel;
        // ******************************************************************** Mi

        // ***********************************************************************
        if (Alpha >= Math.PI / 2)
        {

            jjj += 1;

            // A2
            yyy = Math.Acos(Math.Sin(LatP_Pel_tmp) / Math.Cos(LATi_Pel)); // Pel=45grad
            //yyy = Math.Acos(Math.Sin(LATi_Pel) / Math.Cos(LatP_Pel)); // Pel=45grad
            //yyy = Math.Asin((1 / Math.Tan(Math.PI / 2 - LatP_Pel)) /
            //(1 / Math.Tan(Theta_Pel_tmp))); // Pel=45grad

            NumbFikt_Pel_tmp = NumbFikt_Pel_tmp - IndFP_Pel;
            IndFP_Pel = 1;
            Mmax_Pel_tmp = Mmax_Pel_tmp - Mi_Pel;
            LatP_Pel_tmp = LATi_Pel;
            LongP_Pel_tmp = LONGi_Pel;
            //Theta_Pel_tmp = Math.PI - Theta_Pel_tmp;
            Mi_Pel = (((double)IndFP_Pel) * Mmax_Pel_tmp) / ((double)(NumbFikt_Pel_tmp));
            Alpha = Mi_Pel / REarthP_Pel;

            //Theta_Pel_tmp = yyy;

            // Pel<90 and !=0
            if ((Theta_Pel < Math.PI / 2) && (Theta_Pel != 0))
                Theta_Pel_tmp = Math.PI - yyy;  // Pel=45grad

            // Pel=0
            else if (Theta_Pel == 0)
            {
                //if (jjj<2)
                //    Theta_Pel_tmp = 0;
                //else
                //    Theta_Pel_tmp = Math.PI;

                lngp_i = (int)(LongP_Pel * 1000);
                lngp_d = ((double)lngp_i)/1000;
                lngi_i = (int)(LONGi_Pel * 1000);
                lngi_d = ((double)lngi_i) / 1000;
                lngpi_i = (int)(Math.PI * 1000);
                lngpi_d = ((double)lngpi_i) / 1000;


               if(lngi_d == lngp_d )
                  Theta_Pel_tmp = 0;
               else if (lngi_d == (lngp_d+lngpi_d))
                   Theta_Pel_tmp = Math.PI;
               else
                   Theta_Pel_tmp = 0;
            }

            // Pel=90
            else if (Theta_Pel == Math.PI/2)
            {
                Theta_Pel_tmp = Math.PI / 2;
            }

            //Theta_Pel_tmp = Math.PI + yyy;   // Pel=315grad
            //else
            //Theta_Pel_tmp = yyy;    // Pel=135grad
            //Theta_Pel_tmp = Math.PI / 2 + Theta_Pel_tmp; 


        } // Alpha >= Math.PI / 2
        // ***********************************************************************

        // Di ********************************************************************
        // Расстояние по прямой от точки стояния пеленгатора до пересечения прямой
        // (проходящей через текущую фиктивную точку и начало координат)и плоскости,
        // касательной в точке стояния пеленгатора

        //Di_Pel = REarthP_Pel * Math.Tan(Alpha);
        //Di_Pel = REarthP_Pel * module(Math.Tan(Mi_Pel / REarthP_Pel));
        Di_Pel = REarthP_Pel * module(Math.Tan(Alpha));

        // ******************************************************************** Di

        // XiYiZi ****************************************************************
        // Координаты точки пересечения прямой (проходящей через текущую фиктивную
        // точку и начало координат) и плоскости касательной в точке стояния 
        // пеленгатора в СК пеленгатора

        Xi_Pel = REarthP_Pel;
        Yi_Pel = Di_Pel * Math.Sin(Theta_Pel_tmp);
        Zi_Pel = Di_Pel * Math.Cos(Theta_Pel_tmp);

        // **************************************************************** XiYiZi

        // Xi0Yi0Zi0 *************************************************************
        // Координаты точки пересечения прямой (проходящей через текущую фиктивную
        // точку и начало координат) и плоскости касательной в точке стояния 
        // пеленгатора в СК1 (Поворот СКП на угол=широте пеленгатора)

        Xi0_Pel = Xi_Pel * Math.Cos(LatP_Pel_tmp) - Zi_Pel * Math.Sin(LatP_Pel_tmp);
        Yi0_Pel = Yi_Pel;
        Zi0_Pel = Xi_Pel * Math.Sin(LatP_Pel_tmp) + Zi_Pel * Math.Cos(LatP_Pel_tmp);

        // ************************************************************* Xi0Yi0Zi0

        // XiG1YiG1ZiG1 **********************************************************
        // Координаты точки пересечения прямой (проходящей через текущую фиктивную
        // точку и начало координат) и плоскости касательной в точке стояния 
        // пеленгатора в опорной геоцентрической СК (Поворот СК1 на угол=долготе 
        // пеленгатора)

        // !!!рабочий вариант
        //XiG1_Pel = Xi0_Pel * Math.Cos(LongP_Pel_tmp) - Yi0_Pel * Math.Sin(LongP_Pel_tmp);
        //YiG1_Pel = Xi0_Pel * Math.Sin(LongP_Pel_tmp) + Yi0_Pel * Math.Cos(LongP_Pel_tmp);

        XiG1_Pel = Xi0_Pel * Math.Cos(LongP_Pel_tmp) + Yi0_Pel * Math.Sin(LongP_Pel_tmp);
        YiG1_Pel = -Xi0_Pel * Math.Sin(LongP_Pel_tmp) + Yi0_Pel * Math.Cos(LongP_Pel_tmp);
        ZiG1_Pel = Zi0_Pel;

        // ********************************************************** XiG1YiG1ZiG1

        // RiLATiLONGi ***********************************************************
        // Угловые координаты фиктивной точки в опорной геоцентрической СК

        Ri1_Pel = Math.Sqrt(XiG1_Pel * XiG1_Pel + YiG1_Pel * YiG1_Pel + ZiG1_Pel * ZiG1_Pel);

        LATi_Pel = Math.Asin(ZiG1_Pel / Ri1_Pel);

        Def_Longitude(
                      XiG1_Pel,
                      YiG1_Pel,
                      Ri1_Pel,
                      LATi_Pel,
                      ref LONGi_Pel
                     );

        XiG_Pel = REarthP_Pel * Math.Cos(LATi_Pel) * Math.Cos(LONGi_Pel);
        YiG_Pel = REarthP_Pel * Math.Cos(LATi_Pel) * Math.Sin(LONGi_Pel);
        ZiG_Pel = REarthP_Pel * Math.Sin(LATi_Pel);
        Ri_Pel = Math.Sqrt(XiG_Pel * XiG_Pel + YiG_Pel * YiG_Pel + ZiG_Pel * ZiG_Pel);

        // *********************************************************** RiLATiLONGi

        // ******************************************************************* VAR1
*/


        // VAR2 ******************************************************************
        // !!!VAR2

        // ......................................................................
        // LAT

        if ((fl_var2 == 0) && (fl2_var2 == 0))
           Lat_Pel_N= Lat_Pel_N1+(V_Pel*Math.Cos(Theta_Pel_tmp))/REarthP_Pel;

        // Перешли через северный полюс
        else if ((fl_var2 == 1) && (fl2_var2 == 0) && (Math.Cos(Theta_Pel_tmp) >= 0))
            Lat_Pel_N = Lat_Pel_N1 - (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;
        else if ((fl_var2 == 1) && (fl2_var2 == 0) && (Math.Cos(Theta_Pel_tmp) < 0))
            Lat_Pel_N = Lat_Pel_N1 + (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;

        // Перешли через южный полюс 
        else if ((fl2_var2 == 1) && (fl_var2 == 0) && (Math.Cos(Theta_Pel_tmp) >= 0))
            Lat_Pel_N = Lat_Pel_N1 + (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;
        else if ((fl2_var2 == 1) && (fl_var2 == 0) && (Math.Cos(Theta_Pel_tmp) < 0))
            Lat_Pel_N = Lat_Pel_N1 - (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;


        // Перешли через северный полюс
        if (
            (fl_var2 == 0) &&
            (Lat_Pel_N >0)&&
            (Lat_Pel_N >= Math.PI / 2)
           )
        {
            Lat_Pel_N = Math.PI / 2;

            fl_var2 = 1;
            fl2_var2 = 0;
            fl1_var2 = 0;

        }

        // Перешли через южный полюс
        if (
            (fl2_var2 == 0) &&
            (Lat_Pel_N < 0) &&
            (Lat_Pel_N <= -Math.PI / 2)
           )
        {
            Lat_Pel_N = -Math.PI / 2;
            //Theta_Pel = 0;

            fl2_var2 = 1;
            fl_var2 = 0;
            fl1_var2 = 0;

        }


        //0223
        REarthP_Pel = REarth_Pel * (1 - 0.003352 * Math.Sin(Lat_Pel_N) * Math.Sin(Lat_Pel_N));

        // .......................................................................
        // LONG

        // Перешли через полюс
        if (
            (fl1_var2 == 0) &&
            ((fl_var2 == 1) || (fl2_var2 == 1))
           )
        {
            if (Long_Pel_N1 >= 0)
                Long_Pel_N1 = Long_Pel_N1 + Math.PI;
            else if (Long_Pel_N1 < 0)
                Long_Pel_N1 = Long_Pel_N1 - Math.PI;

            if (Long_Pel_N1 > Math.PI)
                Long_Pel_N1 = -(2 * Math.PI - Long_Pel_N1);
            else if (Long_Pel_N1 < -Math.PI)
                Long_Pel_N1 = 2 * Math.PI + Long_Pel_N1;


            fl1_var2 = 1;
        }


        // Обычный расчет
        Long_Pel_N = Long_Pel_N1 +
                    (V_Pel * Math.Sin(Theta_Pel_tmp)) / (REarthP_Pel * Math.Cos(Lat_Pel_N1));

        if (Long_Pel_N > Math.PI)
            Long_Pel_N = -(2 * Math.PI - Long_Pel_N);
        else if (Long_Pel_N < -Math.PI)
            Long_Pel_N = 2 * Math.PI + Long_Pel_N;
        // .......................................................................

        // ........................................................................
        XiG_Pel = REarthP_Pel * Math.Cos(Lat_Pel_N) * Math.Cos(Long_Pel_N);
        YiG_Pel = REarthP_Pel * Math.Cos(Lat_Pel_N) * Math.Sin(Long_Pel_N);
        ZiG_Pel = REarthP_Pel * Math.Sin(Lat_Pel_N);
        Ri_Pel = Math.Sqrt(XiG_Pel * XiG_Pel + YiG_Pel * YiG_Pel + ZiG_Pel * ZiG_Pel);
        // ........................................................................

        Lat_Pel_N1 = Lat_Pel_N;
        Long_Pel_N1 = Long_Pel_N;
        // ****************************************************************** VAR2

        // Массивы ***************************************************************
        // Занесение в массивы

        // Ri,LATi,LONGi (км, град)
        mas_Pel[IndMas_Pel * 3] = Ri_Pel/1000;
        //mas_Pel[IndMas_Pel * 3 + 1] = LATi_Pel;
        //mas_Pel[IndMas_Pel * 3 + 2] = LONGi_Pel;

        // VAR2
        mas_Pel[IndMas_Pel * 3 + 1] = (Lat_Pel_N*180)/Math.PI;
        mas_Pel[IndMas_Pel * 3 + 2] = (Long_Pel_N*180)/Math.PI;


        // XYZ в опорной геоцентрической СК (км)
        mas_Pel_XYZ[IndMas_Pel * 3] = XiG_Pel/1000;
        mas_Pel_XYZ[IndMas_Pel * 3 + 1] = YiG_Pel/1000;
        mas_Pel_XYZ[IndMas_Pel * 3 + 2] = ZiG_Pel/1000;

        // *************************************************************** Массивы

        // Вывод для отладки ****************************************************

        //*(pmas_Otl_Pel+IndMas_Pel) = Di_Pel;

        // **************************************************** Вывод для отладки`

        // ***********************************************************************
        // Переход к следующей фиктивной точке

        IndFP_Pel += 1;
        IndMas_Pel += 1;
        // ***********************************************************************

    }; // WHILE

    // km
    XFN_Pel = XiG_Pel/1000;
    YFN_Pel = YiG_Pel/1000;
    ZFN_Pel = ZiG_Pel/1000;

} // Функция f_Peleng
//************************************************************************

// ***********************************************************************
// Расчет координат точки пересечения двух пеленгов

// Входные параметры: 
// XP1_Tr,YP1_Tr,ZP1_Tr - Координаты пеленгатора1 в опорной геоцентрической СК(км)
// XP2_Tr,YP2_Tr,ZP2_Tr - Координаты пеленгатора2 в опорной геоцентрической СК(км)
// XFN1_Tr,YFN1_Tr,ZFN1_Tr -  Координаты N-й фиктивной точки в опорной 
//                            геоцентрической СК для пеленгатора1(км)
// XFN2_Tr,YFN2_Tr,ZFN2_Tr -  Координаты N-й фиктивной точки в опорной 
//                            геоцентрической СК для пеленгатора2(км)

//
// Выходные параметры: 
// XIRI_Tr,YIRI_Tr,ZIRI_Tr - Координаты искомого ИРИ в опорной геоцентрической СК(км)
// LatIRI_Tr,LongIRI_Tr - Угловые координаты искомого ИРИ в опорной геоцентрической СК(град)
// RIRI_Tr - Дальность до искомого ИРИ в км
 
// Широта -90(юг)...+90(север)
// Долгота -180(на запад от Гринвича)...+180(на восток от Гринвича)
// Пеленг 0...360(по часовой стрелке)
// ***********************************************************************

public void f_Triang

    (
       // Координаты пеленгатора в опорной геоцентрической СК(км)
        double XP1_Tr,
        double YP1_Tr,
        double ZP1_Tr,
        double XP2_Tr,
        double YP2_Tr,
        double ZP2_Tr,

        // Координаты N-й фиктивной точки в опорной геоцентрической СК(км)
        double XFN1_Tr,
        double YFN1_Tr,
        double ZFN1_Tr,
        double XFN2_Tr,
        double YFN2_Tr,
        double ZFN2_Tr,

        // Координаты искомого ИРИ в опорной геоцентрической СК(км)
        ref double XIRI_Tr,
        ref double YIRI_Tr,
        ref double ZIRI_Tr,

        // Угловые координаты искомого ИРИ в опорной геоцентрической СК(град)
        ref double RIRI_Tr,  // км
        ref double LatIRI_Tr,
        ref double LongIRI_Tr

    )
{

    // Initial conditions ****************************************************
    // Initial conditions

    if (fl_first_Tr == 0)
    {

        // ...............................................................
        // Перевод в м 
 
        XP1_Tr_tmp=XP1_Tr*1000;
        YP1_Tr_tmp=YP1_Tr*1000;
        ZP1_Tr_tmp=ZP1_Tr*1000;
        XP2_Tr_tmp = XP2_Tr * 1000;
        YP2_Tr_tmp = YP2_Tr * 1000;
        ZP2_Tr_tmp = ZP2_Tr * 1000;

        XFN1_Tr_tmp = XFN1_Tr * 1000;
        YFN1_Tr_tmp = YFN1_Tr * 1000;
        ZFN1_Tr_tmp = ZFN1_Tr * 1000;
        XFN2_Tr_tmp = XFN2_Tr * 1000;
        YFN2_Tr_tmp = YFN2_Tr * 1000;
        ZFN2_Tr_tmp = ZFN2_Tr * 1000;

        // ...............................................................

        fl_first_Pel = 1;
        // ...............................................................

    }
    // **************************************************** Initial conditions

    // A,T,C *****************************************************************
    // Расчет коэффициентов пллоскости пеленгования

    A1_Tr = YP1_Tr_tmp * (ZFN1_Tr_tmp) - ZP1_Tr_tmp * (YFN1_Tr_tmp);
    T1_Tr = ZP1_Tr_tmp * (XFN1_Tr_tmp) - XP1_Tr_tmp * (ZFN1_Tr_tmp);
    C1_Tr = XP1_Tr_tmp * (YFN1_Tr_tmp) - YP1_Tr_tmp * (XFN1_Tr_tmp);

    A2_Tr = YP2_Tr_tmp * (ZFN2_Tr_tmp) - ZP2_Tr_tmp * (YFN2_Tr_tmp);
    T2_Tr = ZP2_Tr_tmp * (XFN2_Tr_tmp) - XP2_Tr_tmp * (ZFN2_Tr_tmp);
    C2_Tr = XP2_Tr_tmp * (YFN2_Tr_tmp) - YP2_Tr_tmp * (XFN2_Tr_tmp);

    // ***************************************************************** A,T,C

    // C1!=0 11111111111111111111111111111111111111111111111111111111111111111
    if (C1_Tr != 0)
    {
        // .......................................................................

        if (module(C1_Tr) < 1E-10)
        {
            K1_Tr = -(A1_Tr / 1E-10);
            K2_Tr = -(T1_Tr / 1E-10);
        }

        else
        {
            K1_Tr = -(A1_Tr / C1_Tr);
            K2_Tr = -(T1_Tr / C1_Tr);
        }

        if (module(T1_Tr * C2_Tr - C1_Tr * T2_Tr) < 1E-10)
            K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / 1E-10;
        else
            K3_Tr = (C1_Tr * A2_Tr - A1_Tr * C2_Tr) / (T1_Tr * C2_Tr - C1_Tr * T2_Tr);
        // .......................................................................
        // Координаты искомого ИРИ в опорной геоцентрической СК

        if (module(Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr))) < 1E-10)
        {
            XIRI1_Tr = REarth_Pel / 1E-10;
            XIRI2_Tr = -REarth_Pel / 1E-10;
        }
        else
        {
            XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
            XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K3_Tr * K3_Tr + (K1_Tr + K2_Tr * K3_Tr) * (K1_Tr + K2_Tr * K3_Tr));
        }

        YIRI1_Tr = K3_Tr * XIRI1_Tr;
        YIRI2_Tr = K3_Tr * XIRI2_Tr;

        ZIRI1_Tr = K1_Tr * XIRI1_Tr + K2_Tr * YIRI1_Tr;
        ZIRI2_Tr = K1_Tr * XIRI2_Tr + K2_Tr * YIRI2_Tr;
        // .......................................................................

    } // C1!=0
    // 11111111111111111111111111111111111111111111111111111111111111111 C1!=0

    // C1=0 222222222222222222222222222222222222222222222222222222222222222222
    else
    {
        // .......................................................................
        if (module(T1_Tr) < 1E-10)
            K3_Tr = -A1_Tr / 1E-10;
        else
            K3_Tr = -A1_Tr / T1_Tr;

        /*
            if (module(C2_Tr) < 1E-10)
                K2_Tr = (A1_Tr + K3_Tr * T1_Tr) / 1E-10; 
            else
                K2_Tr = (A1_Tr+K3_Tr*T1_Tr)/C2_Tr; 
         */

        if (module(C2_Tr) < 1E-10)
            K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / 1E-10;
        else
            K2_Tr = -(A2_Tr + K3_Tr * T2_Tr) / C2_Tr;

        // .......................................................................
        // Координаты искомого ИРИ в опорной геоцентрической СК

        if (module(Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr)) < 1E-10)
        {
            XIRI1_Tr = REarth_Pel / 1E-10;
            XIRI2_Tr = -REarth_Pel / 1E-10;
        }

        else
        {
            XIRI1_Tr = REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
            XIRI2_Tr = -REarth_Pel / Math.Sqrt(1 + K2_Tr * K2_Tr + K3_Tr * K3_Tr);
        }

        YIRI1_Tr = K3_Tr * XIRI1_Tr;
        YIRI2_Tr = K3_Tr * XIRI2_Tr;

        ZIRI1_Tr = K2_Tr * XIRI1_Tr;
        ZIRI2_Tr = K2_Tr * XIRI2_Tr;
        // .......................................................................

    } // C1=0
    // 222222222222222222222222222222222222222222222222222222222222222222 C1=0

    // XIRI_Tr,YIRI_Tr,ZIRI_Tr ***********************************************
    // Критерий выбора координат искомого ИРИ в опорной геоцентрической СК
    // (минимальное расстояние до ПП)

    /*
     L1_Tr =((*pXFN1_Tr-*pXFN2_Tr)/2.-XIRI1_Tr)*((*pXFN1_Tr-*pXFN2_Tr)/2.-XIRI1_Tr)+
            ((*pYFN1_Tr-*pYFN2_Tr)/2.-YIRI1_Tr)*((*pYFN1_Tr-*pYFN2_Tr)/2.-YIRI1_Tr)+
            ((*pZFN1_Tr-*pZFN2_Tr)/2.-ZIRI1_Tr)*((*pZFN1_Tr-*pZFN2_Tr)/2.-ZIRI1_Tr);

     L2_Tr =((*pXFN1_Tr-*pXFN2_Tr)/2.-XIRI2_Tr)*((*pXFN1_Tr-*pXFN2_Tr)/2.-XIRI2_Tr)+
            ((*pYFN1_Tr-*pYFN2_Tr)/2.-YIRI2_Tr)*((*pYFN1_Tr-*pYFN2_Tr)/2.-YIRI2_Tr)+
            ((*pZFN1_Tr-*pZFN2_Tr)/2.-ZIRI2_Tr)*((*pZFN1_Tr-*pZFN2_Tr)/2.-ZIRI2_Tr);

    */

    // Координаты середины отрезка, соединяющего N-ые фиктивные точки
    DefMiddle_Pel(XFN1_Tr_tmp, XFN2_Tr_tmp, ref XM_Tr);
    DefMiddle_Pel(YFN1_Tr_tmp, YFN2_Tr_tmp, ref YM_Tr);
    DefMiddle_Pel(ZFN1_Tr_tmp, ZFN2_Tr_tmp, ref ZM_Tr);

    L1_Tr = Math.Sqrt((XM_Tr - XIRI1_Tr) * (XM_Tr - XIRI1_Tr) +
                      (YM_Tr - YIRI1_Tr) * (YM_Tr - YIRI1_Tr) +
                      (ZM_Tr - ZIRI1_Tr) * (ZM_Tr - ZIRI1_Tr));

    L2_Tr = Math.Sqrt((XM_Tr - XIRI2_Tr) * (XM_Tr - XIRI2_Tr) +
                      (YM_Tr - YIRI2_Tr) * (YM_Tr - YIRI2_Tr) +
                      (ZM_Tr - ZIRI2_Tr) * (ZM_Tr - ZIRI2_Tr));

    if (L1_Tr < L2_Tr)
    {
        XIRI_Tr = XIRI1_Tr;
        YIRI_Tr = YIRI1_Tr;
        ZIRI_Tr = ZIRI1_Tr;
    }
    else
    {
        XIRI_Tr = XIRI2_Tr;
        YIRI_Tr = YIRI2_Tr;
        ZIRI_Tr = ZIRI2_Tr;
    }
    // *********************************************** XIRI_Tr,YIRI_Tr,ZIRI_Tr

    // RIRI_Tr,LatIRI_Tr,LongIRI_Tr ******************************************
    // Угловые координаты искомого ИРИ в опорной геоцентрической СК

    // !!! Д.б. *pRIRI_Tr==REarth_Pel, т.е. r=Rз
    RIRI_Tr = Math.Sqrt(XIRI_Tr * XIRI_Tr + YIRI_Tr * YIRI_Tr + ZIRI_Tr * ZIRI_Tr);

    LatIRI_Tr = Math.Asin(ZIRI_Tr / RIRI_Tr);


    /*
     if (module(RIRI_Tr * Math.Cos(LatIRI_Tr)) < 1E-10)
     {

         if ((XIRI_Tr < 0) && (YIRI_Tr > 0))
             LongIRI_Tr = Math.PI - Math.Asin(YIRI_Tr / 1E-10);
         else if ((XIRI_Tr < 0) && (YIRI_Tr < 0))
             LongIRI_Tr = -Math.PI - Math.Asin(YIRI_Tr / 1E-10);
         else
             LongIRI_Tr = Math.Asin(YIRI_Tr / 1E-10);

     }

     else
     {

         if ((XIRI_Tr < 0) && (YIRI_Tr > 0))
             LongIRI_Tr = Math.PI - Math.Asin(YIRI_Tr / (RIRI_Tr * Math.Cos(LatIRI_Tr)));
         else if ((XIRI_Tr < 0) && (YIRI_Tr < 0))
             LongIRI_Tr = -Math.PI - Math.Asin(YIRI_Tr / (RIRI_Tr * Math.Cos(LatIRI_Tr)));
         else
             LongIRI_Tr = Math.Asin(YIRI_Tr / (RIRI_Tr * Math.Cos(LatIRI_Tr)));
     }
    */

    //Def_Longitude(
    //              XIRI_Tr,
    //              YIRI_Tr,
    //              RIRI_Tr,
    //              LatIRI_Tr,
    //              ref LongIRI_Tr
    //             );


    Def_Longitude_180(
                  XIRI_Tr,
                  YIRI_Tr,
                  RIRI_Tr,
                  LatIRI_Tr,
                  ref LongIRI_Tr
                 );

    // ****************************************** RIRI_Tr,LatIRI_Tr,LongIRI_Tr

    // *********************************************************************
    // Перевод в км и град

    // Координаты искомого ИРИ в опорной геоцентрической СК
    XIRI_Tr=XIRI_Tr/1000;
    YIRI_Tr=YIRI_Tr/1000;
    ZIRI_Tr=ZIRI_Tr/1000;

    RIRI_Tr=RIRI_Tr/1000;

    // Угловые координаты искомого ИРИ в опорной геоцентрической СК
    LatIRI_Tr=(LatIRI_Tr*180)/Math.PI;
    LongIRI_Tr=(LongIRI_Tr*180)/Math.PI;
    // *********************************************************************


} // Функция f_Triang
//**************************************************************************

// ***********************************************************************
// Расчет dH
// (преобразования Молоденского)
// ***********************************************************************
public void f_WGS84_Alt
    (

        // Входные параметры (при нажатии кнопки переводятся в радианы)
        double Lat_Coord_8442,   // широта
        double Long_Coord_8442,  // долгота
        double H_Coord_8442,     // высота

        // DATUM
        double dX_Coord,
        double dY_Coord,
        double dZ_Coord,

        // Выходные параметры (m)
        ref double dH_Coord      // dH

    )
{

    double N;

    // Initial conditions ****************************************************
    // Initial conditions

    if (fl_first1_Coord == 0)
    {

        // Эллипсоид Красовского
        a1P = 1 / 298.3;         // Сжатие
        e2P = 2 * a1P - a1P * a1P; // Квадрат эксцентриситета

        // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
        a1W = 1 / 298.257223563; // Сжатие
        e2W = 2 * a1W - a1W * a1W;  // Квадрат эксцентриситета

        // Вспомогательные значения для преобразования эллипсоидов
        a = (aP + aW) / 2;
        e2 = (e2P + e2W) / 2;
        da = aW - aP;
        de2 = e2W - e2P;
        // ...............................................................

        fl_first1_Coord = 1;
        // ...............................................................

    }
    // **************************************************** Initial conditions

    // Радиус кривизны первого вертикала
    N = a / Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442) * Math.Sin(Lat_Coord_8442));

    dH_Coord = -a / N * da + N * Math.Sin(Lat_Coord_8442) * Math.Sin(Lat_Coord_8442) * de2 / 2 +
               dX_Coord * Math.Cos(Lat_Coord_8442) * Math.Cos(Long_Coord_8442) +
               dY_Coord * Math.Cos(Lat_Coord_8442) * Math.Sin(Long_Coord_8442) +
               dZ_Coord * Math.Sin(Lat_Coord_8442) -
               N * e2 * Math.Sin(Lat_Coord_8442) * Math.Cos(Lat_Coord_8442) *
              (wx / ro * Math.Sin(Long_Coord_8442) -
              wy / ro * Math.Cos(Long_Coord_8442)) +
              (a * a / N + H_Coord_8442) * ms;


    //H_Coord_Vyx = H_Coord_8442 + dH;

} // Функция f_WGS84_Alt
//************************************************************************

// ***********************************************************************
public void f_WGS84_SK42_Alt
    (

        // Входные параметры,m
        double H_Coord_8442,     // высота
        double dH_Coord,

        // Выходные параметры (m)
        ref double H_Coord_Vyx_8442      // высота

    )
{

    H_Coord_Vyx_8442 = H_Coord_8442 - dH_Coord;

} // Функция f_WGS84_SK42_Alt
//************************************************************************

// ***********************************************************************
public void f_SK42_WGS84_Alt
    (

        // Входные параметры,m
        double H_Coord_8442,     // высота
        double dH_Coord,

        // Выходные параметры (m)
        ref double H_Coord_Vyx_4284      // высота

    )
{

    H_Coord_Vyx_4284 = H_Coord_8442 + dH_Coord;

} // Функция f_SK42_WGS84_Alt
//************************************************************************

// ***********************************************************************
// Расчет приращения по долготе при преобразованиях координат WGS84 (эллипсоид)
// <-> эллипсоид Красовского(Пулково-42)   (преобразования Молоденского) в
//  угл.сек

// Входные параметры:
// Lat_Coord_8442 - широта (град)
// Long_Coord_8442 - долгота (град)
// H_Coord_8442 - высота (км)
// dX_Coord,dY_Coord,dZ_Coord - !!! это д.б.глобальные переменные, в которых
// хранится DATUM (м) 

// Выходные параметры:
// dLong_Coord - приращение по долготе, !!! угловые секунды
// ***********************************************************************
public void f_dLong
    (

        // Входные параметры (внутри функции переводятся в рад и м)
        double Lat_Coord_8442,   // широта   (grad)
        double Long_Coord_8442,  // долгота  (grad)
        double H_Coord_8442,     // высота   (km)

        // DATUM
        double dX_Coord,
        double dY_Coord,
        double dZ_Coord,

        // Выходные параметры 
        ref double dLong_Coord   // приращение по долготе, угл.сек

    )
{

    double N;
    double Lat_Coord_8442_tmp;
    double Long_Coord_8442_tmp;
    double H_Coord_8442_tmp;


    // Initial conditions ****************************************************
    // Initial conditions

    if (fl_first2_Coord == 0)
    {

        // Эллипсоид Красовского
        a1P = 1 / 298.3;         // Сжатие
        e2P = 2 * a1P - a1P * a1P; // Квадрат эксцентриситета

        // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
        a1W = 1 / 298.257223563; // Сжатие
        e2W = 2 * a1W - a1W * a1W;  // Квадрат эксцентриситета

        // Вспомогательные значения для преобразования эллипсоидов
        a = (aP + aW) / 2;
        e2 = (e2P + e2W) / 2;
        da = aW - aP;
        de2 = e2W - e2P;
        // ...............................................................

        fl_first2_Coord = 1;
        // ...............................................................

    }
    // **************************************************** Initial conditions

    // ...................................................................
    // Перевод в радианы и м

    H_Coord_8442_tmp = H_Coord_8442 * 1000; // km->m

    // grad->rad
    Lat_Coord_8442_tmp = (Lat_Coord_8442 * Math.PI) / 180;
    Long_Coord_8442_tmp = (Long_Coord_8442 * Math.PI) / 180;
    // ...................................................................

    // Радиус кривизны первого вертикала
    N = a / Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp));

    dLong_Coord = ro / ((N + H_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp)) *
                 (-dX_Coord * Math.Sin(Long_Coord_8442_tmp) + dY_Coord * Math.Cos(Long_Coord_8442_tmp)) +
                Math.Tan(Lat_Coord_8442_tmp) * (1 - e2) * (wx * Math.Cos(Long_Coord_8442_tmp) + wy * Math.Sin(Long_Coord_8442_tmp)) - wz;


} // Функция f_dLong
//************************************************************************

// ***********************************************************************
// Расчет приращения по широте при преобразованиях координат WGS84 (эллипсоид)
// <-> эллипсоид Красовского(Пулково-42)   (преобразования Молоденского) в
//  угл.сек

// Входные параметры:
// Lat_Coord_8442 - широта (град)
// Long_Coord_8442 - долгота (град)
// H_Coord_8442 - высота (км)
// dX_Coord,dY_Coord,dZ_Coord - !!! это д.б.глобальные переменные, в которых
// хранится DATUM (м) 

// Выходные параметры:
// dLat_Coord - приращение по широте, !!! угловые секунды
// ***********************************************************************
public void f_dLat
    (

        // Входные параметры (внутри функции переводятся в рад и м)
        double Lat_Coord_8442,   // широта (grad)
        double Long_Coord_8442,  // долгота (grad)
        double H_Coord_8442,     // высота (km)

        // DATUM
        double dX_Coord,
        double dY_Coord,
        double dZ_Coord,

        // Выходные параметры 
        ref double dLat_Coord    // приращение по широте, угл.сек

    )
{

    double N, M;
    double Lat_Coord_8442_tmp;
    double Long_Coord_8442_tmp;
    double H_Coord_8442_tmp;

    // Initial conditions ****************************************************
    // Initial conditions

    if (fl_first2_Coord == 0)
    {
        // ...................................................................

        // Эллипсоид Красовского
        a1P = 1 / 298.3;                 // Сжатие
        //e2P = 2 * a1P - a1P*a1P;       // Квадрат эксцентриситета
        e2P = 2 * a1P - Math.Pow(a1P, 2); // Квадрат эксцентриситета


        // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
        a1W = 1 / 298.257223563; // Сжатие
        e2W = 2 * a1W - a1W * a1W;  // Квадрат эксцентриситета

        // Вспомогательные значения для преобразования эллипсоидов
        a = (aP + aW) / 2;
        e2 = (e2P + e2W) / 2;
        da = aW - aP;
        de2 = e2W - e2P;
        // ...............................................................

        fl_first2_Coord = 1;
        // ...............................................................

    }
    // **************************************************** Initial conditions

    // ...................................................................
    // Перевод в радианы и м

    H_Coord_8442_tmp = H_Coord_8442 * 1000; // km->m

    // grad->rad
    Lat_Coord_8442_tmp = (Lat_Coord_8442 * Math.PI) / 180;
    Long_Coord_8442_tmp = (Long_Coord_8442 * Math.PI) / 180;
    // ...................................................................

    // Радиус кривизны первого вертикала
    N = a / Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp));

    // Радиус кривизны меридиана
    M = a * (1 - e2) *
        (1 / (Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp)) *
            Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp)) *
            Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp))));

    dLat_Coord = (ro / (M + H_Coord_8442_tmp)) *
                  ((N / a) * e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp) * da +
                  ((N * N) / (a * a) + 1) * N * Math.Sin(Lat_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp) * (de2 / 2) -
                  dX_Coord * Math.Cos(Long_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp) -
                  dY_Coord * Math.Sin(Long_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp) +
                  dZ_Coord * Math.Cos(Lat_Coord_8442_tmp)) -
                  wx * Math.Sin(Long_Coord_8442_tmp) * (1 + e2 * Math.Cos(2 * Lat_Coord_8442_tmp)) +
                  wy * Math.Cos(Long_Coord_8442_tmp) * (1 + e2 * Math.Cos(2 * Lat_Coord_8442_tmp)) -
                  ro * ms * e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp);


} // Функция f_dLat
//************************************************************************

// ***********************************************************************
// Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)

// Входные параметры:
// Lat_Coord_8442 - широта (град)
// Long_Coord_8442 - долгота (град)
// H_Coord_8442 - высота (км)
// Lat_Coord - приращение по широте, !!! угловые секунды
// Long_Coord - приращение по долготе, !!! угловые секунды

// Выходные параметры:
// Lat_Coord_Vyx_8442 - широта (град)
// Long_Coord_Vyx_8442 - долгота (град)

// ***********************************************************************
public void f_WGS84_SK42_Lat_Long
       (

           // Входные параметры (grad,km)
           double Lat_Coord_8442,   // широта
           double Long_Coord_8442,  // долгота
           double H_Coord_8442,     // высота
           double dLat_Coord,       // приращение по долготе,угл.сек
           double dLong_Coord,      // приращение по долготе,угл.сек

           // Выходные параметры (grad)
           ref double Lat_Coord_Vyx_8442,   // широта
           ref double Long_Coord_Vyx_8442   // долгота

       )
{

    // grad
    //Lat_Coord_Vyx_8442 = (Lat_Coord_8442 * 180) / Math.PI - dLat_Coord / 3600; 
    //Long_Coord_Vyx_8442 = (Long_Coord_8442 * 180) / Math.PI - dLong_Coord / 3600;
    Lat_Coord_Vyx_8442 = Lat_Coord_8442  - dLat_Coord / 3600;
    Long_Coord_Vyx_8442 = Long_Coord_8442  - dLong_Coord / 3600;

} // Функция f_WGS84_SK42_Lat_Long
//************************************************************************

// ***********************************************************************
// Перевод координат эллипсоид Красовского(Пулково-42)->WGS84 (эллипсоид)

// Входные параметры:
// Lat_Coord_8442 - широта (град)
// Long_Coord_8442 - долгота (град)
// H_Coord_8442 - высота (км)
// Lat_Coord - приращение по широте, !!! угловые секунды
// Long_Coord - приращение по долготе, !!! угловые секунды

// Выходные параметры:
// Lat_Coord_Vyx_4284 - широта (град)
// Long_Coord_Vyx_4284 - долгота (град)

// ***********************************************************************
public void f_SK42_WGS84_Lat_Long
       (

           // Входные параметры (grad,km)
           double Lat_Coord_8442,   // широта
           double Long_Coord_8442,  // долгота
           double H_Coord_8442,     // высота
           double dLat_Coord,    // приращение по долготе
           double dLong_Coord,   // приращение по долготе

           // Выходные параметры (grad)
           ref double Lat_Coord_Vyx_4284,   // широта
           ref double Long_Coord_Vyx_4284  // долгота

       )
{

    // grad
    //Lat_Coord_Vyx_4284 = (Lat_Coord_8442 * 180) / Math.PI + dLat_Coord / 3600;
    //Long_Coord_Vyx_4284 = (Long_Coord_8442 * 180) / Math.PI + dLong_Coord / 3600;
    Lat_Coord_Vyx_4284 = Lat_Coord_8442  + dLat_Coord / 3600;
    Long_Coord_Vyx_4284 = Long_Coord_8442  + dLong_Coord / 3600;


} // Функция f_SK42_WGS84_Lat_Long
//************************************************************************

// ***********************************************************************
// Преобразование геодезических координат в пространственные прямоугольные 
// координаты для эллипсоида WGS84

// Входные параметры:
// Lat_Coord - широта (град)
// Long_Coord - долгота (град)
// H_Coord - высота (км)

// Выходные параметры:
// X_Coord,Y_Coord,Z_Coord - Прямоугольные пространственные координаты для
// выбранного эллипсоида (км)
// ***********************************************************************
public void f_BLH_XYZ_84
    (

        // Входные параметры (град, км)
        double Lat_Coord,   // широта
        double Long_Coord,  // долгота
        double H_Coord,     // высота

        // Прямоугольные пространственные координаты для выбранного эллипсоида
        // км
        ref double X_Coord,
        ref double Y_Coord,
        ref double Z_Coord

    )
{

    double N;      // Радиус кривизны первого вертикала
    double e2_tmp; // Квадрат эксцентриситета

    double Lat_Coord_tmp;
    double Long_Coord_tmp;
    double H_Coord_tmp;

// Initial conditions ****************************************************
// Initial conditions

    if (fl_first5_Coord == 0)
    {

        // ...............................................................
        // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
        a1W = 1 / 298.257223563; // Сжатие
        // ...............................................................
        fl_first5_Coord = 1;
        // ...............................................................

    }
// **************************************************** Initial conditions

    // ...................................................................
    // Перевод в радианы и м

    H_Coord_tmp = H_Coord * 1000; // km->m

    // grad->rad
    Lat_Coord_tmp = (Lat_Coord * Math.PI) / 180;
    Long_Coord_tmp = (Long_Coord * Math.PI) / 180;
    // ...................................................................

// N *********************************************************************
    // Квадрат эксцентриситета
    e2_tmp = 2 * a1W - Math.Pow(a1W, 2);

    // Радиус кривизны первого вертикала
    // (aW-большая полуось эллипсоида WGS84)
    N = aW / Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(Lat_Coord_tmp), 2));

// ********************************************************************* N

// XYZ *******************************************************************
// km

    X_Coord = ((N + H_Coord_tmp) * Math.Cos(Lat_Coord_tmp) * Math.Cos(Long_Coord_tmp))/1000;

    Y_Coord = ((N + H_Coord_tmp) * Math.Cos(Lat_Coord_tmp) * Math.Sin(Long_Coord_tmp))/1000;

    Z_Coord = (((1 - e2_tmp) * N + H_Coord_tmp) * Math.Sin(Lat_Coord_tmp)) / 1000;

    //Z_Coord = N*(1-)*;

// ******************************************************************* XYZ


} // Функция f_BLH_XYZ_84
//************************************************************************

// ***********************************************************************
// Преобразование геодезических координат в пространственные прямоугольные 
// координаты для эллипсоида Красовского (SK42)

// Входные параметры:
// Lat_Coord - широта (град)
// Long_Coord - долгота (град)
// H_Coord - высота (км)

// Выходные параметры:
// X_Coord,Y_Coord,Z_Coord - Прямоугольные пространственные координаты для
// выбранного эллипсоида (км)

// ***********************************************************************
public void f_BLH_XYZ_42
    (

        // Входные параметры (град, км)
        double Lat_Coord,   // широта
        double Long_Coord,  // долгота
        double H_Coord,     // высота

        // Прямоугольные пространственные координаты для выбранного эллипсоида
        ref double X_Coord,
        ref double Y_Coord,
        ref double Z_Coord

    )
{

    double N;      // Радиус кривизны первого вертикала
    double e2_tmp; // Квадрат эксцентриситета

    double Lat_Coord_tmp;
    double Long_Coord_tmp;
    double H_Coord_tmp;

// Initial conditions ****************************************************
// Initial conditions

    if (fl_first6_Coord == 0)
    {

        // ...............................................................
        // Эллипсоид Красовского
        a1P = 1 / 298.3;                 // Сжатие
        // ...............................................................
        fl_first6_Coord = 1;
        // ...............................................................

    }
// **************************************************** Initial conditions

    // ...................................................................
    // Перевод в радианы и м

    H_Coord_tmp = H_Coord * 1000; // km->m

    // grad->rad
    Lat_Coord_tmp = (Lat_Coord * Math.PI) / 180;
    Long_Coord_tmp = (Long_Coord * Math.PI) / 180;
    // ...................................................................

// N *********************************************************************
    // Квадрат эксцентриситета
    e2_tmp = 2 * a1P - Math.Pow(a1P, 2);

    // Радиус кривизны первого вертикала
    // (aP-большая полуось эллипсоида Красовского)
    N = aP / Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(Lat_Coord_tmp), 2));

// ********************************************************************* N

// XYZ *******************************************************************
// km

    X_Coord = ((N + H_Coord_tmp) * Math.Cos(Lat_Coord_tmp) * Math.Cos(Long_Coord_tmp))/1000;

    Y_Coord = ((N + H_Coord_tmp) * Math.Cos(Lat_Coord_tmp) * Math.Sin(Long_Coord_tmp))/1000;

    Z_Coord = (((1 - e2_tmp) * N + H_Coord_tmp) * Math.Sin(Lat_Coord_tmp))/1000;

// ******************************************************************* XYZ


} // Функция f_BLH_XYZ_42
//************************************************************************

// ***********************************************************************
// Преобразование пространственных прямоугольных координат в геодезические
// координаты для эллипсоида WGS84

// Входные параметры:
// X_Coord,Y_Coord,Z_Coord - Прямоугольные пространственные координаты для
// выбранного эллипсоида (км)

// Выходные параметры:
// Lat_Coord - широта (град)
// Long_Coord - долгота (град)
// H_Coord - высота (км)

// ***********************************************************************
public void f_XYZ_BLH_84
    (

        // Входные параметры: прямоугольные пространственные координаты
        // для выбранного эллипсоида (км)
        double X_Coord,
        double Y_Coord,
        double Z_Coord,

        // Выходные параметры (град)
        ref double Lat_Coord,   // широта
        ref double Long_Coord,  // долгота
        ref double H_Coord      // высота (км)

    )
{

    double e2_tmp; // Квадрат эксцентриситета

    // Вспомогательные величины
    double D_tmp, La_tmp;
    double r_tmp, c_tmp, p_tmp, b_tmp;
    double S1_tmp, S2_tmp;
    double d_tmp;

    double delt_d_tmp;

    double X_Coord_tmp;
    double Y_Coord_tmp;
    double Z_Coord_tmp;


    // Initial conditions ****************************************************
    // Initial conditions

    if (fl_first7_Coord == 0)
    {

        // ...............................................................
        // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
        a1W = 1 / 298.257223563; // Сжатие
        // ...............................................................
        fl_first7_Coord = 1;
        // ...............................................................

    }
    // **************************************************** Initial conditions

    // .......................................................................
    // Перевод в m

    X_Coord_tmp = X_Coord * 1000;
    Y_Coord_tmp = Y_Coord * 1000;
    Z_Coord_tmp = Z_Coord * 1000;
    // .......................................................................

    // N *********************************************************************
    // Квадрат эксцентриситета

    e2_tmp = 2 * a1W - Math.Pow(a1W, 2);
    // ********************************************************************* N

    // Long ******************************************************************
    // Вычсление долготы -> анализ D

    D_tmp = Math.Sqrt(Math.Pow(X_Coord_tmp, 2) + Math.Pow(Y_Coord_tmp, 2));

    // .......................................................................
    // D=0

    if (((int)(D_tmp + 0.5)) == 0)
    {
        Long_Coord = 0;

        Lat_Coord = (Math.PI / 2) * (Z_Coord_tmp / module(Z_Coord_tmp));

        H_Coord = Z_Coord_tmp * Math.Sin(Lat_Coord) -
                  aW * Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(Lat_Coord), 2));

        return;

    } // D=0
    // .......................................................................
    // D>0

    else
    {

        La_tmp = Math.Asin(Y_Coord_tmp / D_tmp);
        Long_Coord = La_tmp;

        if ((Y_Coord_tmp < 0) && (X_Coord_tmp > 0))
            Long_Coord = 2 * Math.PI - La_tmp;
        else if ((Y_Coord_tmp < 0) && (X_Coord_tmp < 0))
            Long_Coord = 2 * Math.PI + La_tmp;
        else if ((Y_Coord_tmp > 0) && (X_Coord_tmp < 0))
            Long_Coord = Math.PI - La_tmp;
        //else if ((Y_Coord > 0) && (X_Coord > 0))
        //    Long_Coord = La_tmp;

    } // D>0
    // .......................................................................

    // ***************************************************************** Long

    // Lat,H ****************************************************************
    // Вычисление широты, высоты -> анализ Z

    // .......................................................................
    // Z=0

    if (((int)(Z_Coord_tmp + 0.5)) == 0)
    {
        Lat_Coord = 0;
        H_Coord = D_tmp - aW;

        return;

    } // Z=0
    // .......................................................................
    // Z!=0

    else
    {

        r_tmp = Math.Sqrt(Math.Pow(X_Coord_tmp, 2) + Math.Pow(Y_Coord_tmp, 2) +
                          Math.Pow(Z_Coord_tmp, 2));

        c_tmp = Math.Asin(Z_Coord_tmp / r_tmp);

        p_tmp = (e2_tmp * aW) / (2 * r_tmp);

        S1_tmp = 0;

        // ????????????????????????????????
        // Количество рад для погрешности delt_d=0.0001угл.с
        delt_d_tmp = 0.0001 / ro;

        b_tmp = c_tmp + S1_tmp;

        if (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2)) > 1E-15)
            S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2))));
        else
            S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / 1E-15);

        d_tmp = module(S2_tmp - S1_tmp);

        while (module(d_tmp) >= delt_d_tmp)
        {

            S1_tmp = S2_tmp;

            b_tmp = c_tmp + S1_tmp;

            if (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2)) > 1E-15)
                S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2))));
            else
                S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / 1E-15);

            d_tmp = module(S2_tmp - S1_tmp);

        }; // WHILE

        Lat_Coord = b_tmp;

        H_Coord = D_tmp * Math.Cos(Lat_Coord) + Z_Coord_tmp * Math.Sin(Lat_Coord) -
                  aW * Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(Lat_Coord), 2));


    } // Z!=0
    // .......................................................................

    // **************************************************************** Lat,H

    // ......................................................................
    // Перевод в градусы и км

    H_Coord = H_Coord / 1000;

    Lat_Coord = (Lat_Coord * 180) / Math.PI;
    Long_Coord = (Long_Coord * 180) / Math.PI;
    // ......................................................................


} // Функция f_XYZ_BLH_84
//************************************************************************

// ***********************************************************************
// Преобразование пространственных прямоугольных координат в геодезические
// координаты для эллипсоида Красовского (SK42)

// Входные параметры:
// X_Coord,Y_Coord,Z_Coord - Прямоугольные пространственные координаты для
// выбранного эллипсоида (км)

// Выходные параметры:
// Lat_Coord - широта (град)
// Long_Coord - долгота (град)
// H_Coord - высота (км)
// ***********************************************************************
public void f_XYZ_BLH_42
    (

        // Входные параметры: прямоугольные пространственные координаты
        // для выбранного эллипсоида (км)
        double X_Coord,
        double Y_Coord,
        double Z_Coord,

        // Выходные параметры (град)
        ref double Lat_Coord,   // широта
        ref double Long_Coord,  // долгота
        ref double H_Coord      // высота (км)

    )
{

    double e2_tmp; // Квадрат эксцентриситета

    // Вспомогательные величины
    double D_tmp, La_tmp;
    double r_tmp, c_tmp, p_tmp, b_tmp;
    double S1_tmp, S2_tmp;
    double d_tmp;

    double delt_d_tmp;

    double X_Coord_tmp;
    double Y_Coord_tmp;
    double Z_Coord_tmp;

// Initial conditions ****************************************************
    // Initial conditions

    if (fl_first8_Coord == 0)
    {

        // ...............................................................
        // Эллипсоид Красовского
        a1P = 1 / 298.3;                 // Сжатие
        // ...............................................................
        fl_first8_Coord = 1;
        // ...............................................................

    }
// **************************************************** Initial conditions

    // .......................................................................
    // Перевод в m

    X_Coord_tmp = X_Coord * 1000;
    Y_Coord_tmp = Y_Coord * 1000;
    Z_Coord_tmp = Z_Coord * 1000;
    // .......................................................................

// N *********************************************************************
    // Квадрат эксцентриситета

    e2_tmp = 2 * a1P - Math.Pow(a1P, 2);
// ********************************************************************* N

// Long ******************************************************************
// Вычсление долготы -> анализ D

    D_tmp = Math.Sqrt(Math.Pow(X_Coord_tmp, 2) + Math.Pow(Y_Coord_tmp, 2));

    // .......................................................................
    // D=0

    if (((int)(D_tmp + 0.5)) == 0)
    {
        Long_Coord = 0;

        Lat_Coord = (Math.PI / 2) * (Z_Coord_tmp / module(Z_Coord_tmp));

        H_Coord = Z_Coord_tmp * Math.Sin(Lat_Coord) -
                  aP * Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(Lat_Coord), 2));

        return;

    } // D=0
// .......................................................................
    // D>0

    else
    {

        La_tmp = Math.Asin(Y_Coord_tmp / D_tmp);
        Long_Coord = La_tmp;

        if ((Y_Coord_tmp < 0) && (X_Coord_tmp > 0))
            Long_Coord = 2 * Math.PI - La_tmp;
        else if ((Y_Coord_tmp < 0) && (X_Coord_tmp < 0))
            Long_Coord = 2 * Math.PI + La_tmp;
        else if ((Y_Coord_tmp > 0) && (X_Coord_tmp < 0))
            Long_Coord = Math.PI - La_tmp;
        //else if ((Y_Coord > 0) && (X_Coord > 0))
        //    Long_Coord = La_tmp;

    } // D>0
// .......................................................................

// ***************************************************************** Long

// Lat,H ****************************************************************
// Вычисление широты, высоты -> анализ Z

// .......................................................................
// Z=0

    if (((int)(Z_Coord_tmp + 0.5)) == 0)
    {
        Lat_Coord = 0;
        H_Coord = D_tmp - aP;

        return;

    } // Z=0
// .......................................................................
// Z!=0

    else
    {

        r_tmp = Math.Sqrt(Math.Pow(X_Coord_tmp, 2) + Math.Pow(Y_Coord_tmp, 2) +
                          Math.Pow(Z_Coord_tmp, 2));

        c_tmp = Math.Asin(Z_Coord_tmp / r_tmp);

        p_tmp = (e2_tmp * aP) / (2 * r_tmp);

        S1_tmp = 0;

        // ????????????????????????????????
        // Количество рад для погрешности delt_d=0.0001угл.с
        delt_d_tmp = 0.0001 / ro;

        b_tmp = c_tmp + S1_tmp;

        if (module(Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2))) > 1E-15)
            S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2))));
        else
            S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / 1E-15);

        d_tmp = module(S2_tmp - S1_tmp);

        while (module(d_tmp) >= delt_d_tmp)
        {

            S1_tmp = S2_tmp;

            b_tmp = c_tmp + S1_tmp;

            if (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2)) > 1E-15)
                S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2))));
            else
                S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / 1E-15);

            d_tmp = module(S2_tmp - S1_tmp);

        }; // WHILE

        Lat_Coord = b_tmp;

        H_Coord = D_tmp * Math.Cos(Lat_Coord) + Z_Coord_tmp * Math.Sin(Lat_Coord) -
                  aP * Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(Lat_Coord), 2));


    } // Z!=0
// .......................................................................

// **************************************************************** Lat,H

    // ......................................................................
    // Перевод в градусы и км

    H_Coord = H_Coord / 1000;

    Lat_Coord = (Lat_Coord * 180) / Math.PI;
    Long_Coord = (Long_Coord * 180) / Math.PI;
    // ......................................................................

} // Функция f_XYZ_BLH_42
//************************************************************************

// ***********************************************************************
// Преобразование геодезических координат (широта, долгота, высота) 
// эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
// проекции Гаусса-Крюгера

// Входные параметры:
// Lat_Coord_Kr - широта (град)
// Long_Coord_Kr - долгота (град)

// Выходные параметры:
// X_Coord_Kr, Y_Coord_Kr - плоские прямоугольные координаты (км)
// ***********************************************************************
public void f_SK42_Krug
    (

        // Входные параметры (!!! град)
        double Lat_Coord_Kr,   // широта
        double Long_Coord_Kr,  // долгота
        //double H_Coord_Kr,   // высота (m)

        // Выходные параметры (км)
        ref double X_Coord_Kr,
        ref double Y_Coord_Kr

    )
{

    int No_tmp;
    double Lo_tmp, Bo_tmp;

    // Вспомогательные величины
    double Xa_tmp, Xb_tmp, Xc_tmp, Xd_tmp;
    double Ya_tmp, Yb_tmp, Yc_tmp;
    double sin2_B, sin4_B, sin6_B;

// No ********************************************************************
// Номер шестиградусной зоны в проекци Гаусса-Крюгера (1,2,...)
// !!! Long - в град

    No_tmp = (int)(6 + Long_Coord_Kr) / 6;

// ******************************************************************** No

// Lo ********************************************************************
// Расстояние от определяемой точки до осевого меридиана зоны, рад

    Lo_tmp = (Long_Coord_Kr - (3 + 6 * (No_tmp - 1))) / 57.29577951;

// ******************************************************************** Lo

// Bo ********************************************************************
// Переводим широту в рад

    Bo_tmp = (Lat_Coord_Kr * Math.PI) / 180;


    sin2_B = Math.Pow(Math.Sin(Bo_tmp), 2);
    sin4_B = Math.Pow(Math.Sin(Bo_tmp), 4);
    sin6_B = Math.Pow(Math.Sin(Bo_tmp), 6);

// ******************************************************************** Bo

// X *********************************************************************

    Xa_tmp = Math.Pow(Lo_tmp, 2) *
             (109500 - 574700 * sin2_B + 863700 * sin4_B -
              398600 * sin6_B);

    Xb_tmp = Math.Pow(Lo_tmp, 2) *
             (278194 - 830174 * sin2_B + 572434 * sin4_B -
              16010 * sin6_B + Xa_tmp);

    Xc_tmp = Math.Pow(Lo_tmp, 2) *
            (672483.4 - 811219.9 * sin2_B + 5420 * sin4_B -
             10.6 * sin6_B + Xb_tmp);

    Xd_tmp = Math.Pow(Lo_tmp, 2) *
            (1594561.25 + 5336.535 * sin2_B + 26.79 * sin4_B +
            0.149 * sin6_B + Xc_tmp);

    X_Coord_Kr = 6367558.4968 * Bo_tmp -
                 Math.Sin(Bo_tmp * 2) *
                 (16002.89 + 66.9607 * sin2_B + 0.3515 * sin4_B - Xd_tmp);

// ********************************************************************* X

// Y *********************************************************************

    Ya_tmp = Math.Pow(Lo_tmp, 2) *
            (79690 - 866190 * sin2_B + 1730360 * sin4_B -
             945460 * sin6_B);

    Yb_tmp = Math.Pow(Lo_tmp, 2) *
            (270806 - 1523417 * sin2_B + 1327645 * sin4_B -
             21701 * sin6_B + Ya_tmp);

    Yc_tmp = Math.Pow(Lo_tmp, 2) *
             (1070204.16 - 2136826.66 * sin2_B + 17.98 * sin4_B -
              11.99 * sin6_B + Yb_tmp);

    Y_Coord_Kr = (5 + 10 * No_tmp) * 100000 +
                 Lo_tmp * Math.Cos(Bo_tmp) *
                 (6378245 + 21346.1415 * sin2_B + 107.159 * sin4_B +
                  0.5977 * sin6_B + Yc_tmp);

// ********************************************************************* Y

// .......................................................................
// перевод в км

    X_Coord_Kr = X_Coord_Kr / 1000;
    Y_Coord_Kr = Y_Coord_Kr / 1000;
// .......................................................................


} // Функция f_SK42_Krug
//************************************************************************

// ***********************************************************************
// Преобразование плоских прямоугольных координат в проекции 
// Гаусса-Крюгера в геодезические координаты (широта, долгота, высота) 
// эллипсоида Красовского (СК42) 

// Входные параметры:
// X_Coord_Kr, Y_Coord_Kr - плоские прямоугольные координаты (км)

// Выходные параметры:
// Lat_Coord_Kr - широта (град)
// Long_Coord_Kr - долгота (град)

// ***********************************************************************
public void f_Krug_SK42
    (

        // Входные параметры (км)
        double X_Coord_Kr,
        double Y_Coord_Kr,

        // Выходные параметры (град)
        ref double Lat_Coord_Kr,   // широта
        ref double Long_Coord_Kr   // долгота

    )
{

    int No_tmp;
    double Bo_tmp;

    // Вспомогательные величины
    double Bi_tmp, Zo_tmp;
    double Ba_tmp, Bb_tmp, Bc_tmp;
    double dB_tmp;
    double La_tmp, Lb_tmp, Lc_tmp, Ld_tmp;
    double dL_tmp;
    double sin2_B, sin4_B, sin6_B;

    double X_Coord_Kr_tmp;
    double Y_Coord_Kr_tmp;

// .......................................................................
// Перевод в м

    X_Coord_Kr_tmp = X_Coord_Kr * 1000;
    Y_Coord_Kr_tmp = Y_Coord_Kr * 1000;
// .......................................................................
   

// No ********************************************************************
// Номер шестиградусной зоны в проекци Гаусса-Крюгера (1,2,...)

    No_tmp = (int)(Y_Coord_Kr_tmp * Math.Pow(10, -6));

// ******************************************************************** No

// Bi,Bo *****************************************************************
// Bo->Геодезическая широта точки, абсцисса которой равнв абсциссе заданной
// точки, а ордината равна нулю (рад)

    Bi_tmp = X_Coord_Kr_tmp / 6367558.4968;

    Bo_tmp = Bi_tmp + Math.Sin(Bi_tmp * 2) *
             (0.00252588685 - 0.0000149186 * Math.Pow(Math.Sin(Bi_tmp), 2) +
              0.00000011904 * Math.Pow(Math.Sin(Bi_tmp), 4));

    sin2_B = Math.Pow(Math.Sin(Bo_tmp), 2);
    sin4_B = Math.Pow(Math.Sin(Bo_tmp), 4);
    sin6_B = Math.Pow(Math.Sin(Bo_tmp), 6);

// ***************************************************************** Bi,Bo

// Bi,Zo *****************************************************************
// Вспомогательная величина

    if (module(6378245 * Math.Cos(Bo_tmp)) > 1E-15)
        Zo_tmp = (Y_Coord_Kr_tmp - (10 * No_tmp + 5) * 100000) / (6378245 * Math.Cos(Bo_tmp));
    else
        Zo_tmp = (Y_Coord_Kr_tmp - (10 * No_tmp + 5) * 100000) / 1E-15;

// ***************************************************************** Bi,Zo

// Широта ****************************************************************

    Ba_tmp = Zo_tmp * Zo_tmp *
             (0.01672 - 0.0063 * sin2_B + 0.01188 * sin4_B - 0.00328 * sin6_B);

    Bb_tmp = Zo_tmp * Zo_tmp *
             (0.042858 - 0.025318 * sin2_B + 0.014346 * sin4_B - 0.001264 * sin6_B - Ba_tmp);

    Bc_tmp = Zo_tmp * Zo_tmp *
            (0.10500614 - 0.04559916 * sin2_B + 0.00228901 * sin4_B -
             0.00002987 * sin6_B - Bb_tmp);

    dB_tmp = -Zo_tmp * Zo_tmp * Math.Sin(Bo_tmp * 2) *
             (0.251684631 - 0.003369263 * sin2_B + 0.000011276 * sin4_B - Bc_tmp);


    Lat_Coord_Kr = Bo_tmp + dB_tmp;  // rad
// **************************************************************** Широта

// Долгота ***************************************************************

    La_tmp = Zo_tmp * Zo_tmp *
             (0.0038 + 0.0524 * sin2_B + 0.0482 * sin4_B + 0.0032 * sin6_B);

    Lb_tmp = Zo_tmp * Zo_tmp *
            (0.01225 + 0.09477 * sin2_B + 0.03282 * sin4_B - 0.00034 * sin6_B - La_tmp);

    Lc_tmp = Zo_tmp * Zo_tmp *
            (0.0420025 + 0.1487407 * sin2_B + 0.005942 * sin4_B - 0.000015 * sin6_B - Lb_tmp);

    Ld_tmp = Zo_tmp * Zo_tmp *
             (0.16778975 + 0.16273586 * sin2_B - 0.0005249 * sin4_B -
              0.00000846 * sin6_B - Lc_tmp);

    dL_tmp = Zo_tmp *
             (1 - 0.0033467108 * sin2_B - 0.0000056002 * sin4_B -
              0.0000000187 * sin6_B - Ld_tmp);

    Long_Coord_Kr = 6 * (No_tmp - 0.5) / 57.29577951 + dL_tmp; // rad
// *************************************************************** Долгота

// .......................................................................
// Перевод в градусы

    Lat_Coord_Kr = (Lat_Coord_Kr * 180) / Math.PI;
    Long_Coord_Kr = (Long_Coord_Kr * 180) / Math.PI;
// .......................................................................


} // Функция f_Krug_SK42
//************************************************************************

// ***********************************************************************
// Преобразование dd.ddddd (grad) -> DD MM SS 
// (дробное значение градусов -> в градусы(целое значение), минуты(целое значение),
// секунды(дробное значение))

// Входные параметры:
// Grad_Dbl_Coord - градусы в дробном виде

// Выходные параметры:
// Grad_I_Coord - градусы (целое)
// Min_Coord - минуты (целое)
// Sec_Coord - секунды (дробное)
// ***********************************************************************
public void f_Grad_GMS
    (

        // Входные параметры (grad)
        double Grad_Dbl_Coord,

        // Выходные параметры 
        ref int Grad_I_Coord,
        ref int Min_Coord,
        ref double Sec_Coord

    )
{

    Grad_I_Coord = (int)(Grad_Dbl_Coord);

    Min_Coord = (int)((module(Grad_Dbl_Coord) - module(Grad_I_Coord)) * 60);

    Sec_Coord = ((module(Grad_Dbl_Coord) - module(Grad_I_Coord)) * 60 - Min_Coord) * 60;

} // Функция f_Grad_GMS
//************************************************************************

// ***********************************************************************
// Преобразование DD MM SS (градусы(целое значение), минуты(целое значение),
// секунды(дробное значение))-> dd.ddddd (дробное значение градусов) 

// Входные параметры:
// Grad_I_Coord - градусы (целое)
// Min_Coord - минуты (целое)
// Sec_Coord - секунды (дробное)

// Выходные параметры:
// Grad_Dbl_Coord - градусы в дробном виде

// ***********************************************************************
public void f_GMS_Grad
    (

        // Входные параметры 
        int Grad_I_Coord,
        int Min_Coord,
        double Sec_Coord,

        // Выходные параметры (grad)
        ref double Grad_Dbl_Coord

    )
{

    double mm_tmp, ss_tmp;

    mm_tmp = ((double)(Min_Coord)) / 60;
    ss_tmp = Sec_Coord / 3600;


    if (Grad_I_Coord < 0)
    {
        Grad_Dbl_Coord = -ss_tmp - mm_tmp + Grad_I_Coord;
    }

    else
    {
        Grad_Dbl_Coord = Grad_I_Coord + mm_tmp + ss_tmp;
    }

} // Функция f_GMS_Grad
//************************************************************************

// ***********************************************************************
// !!! Используется DATUM ГОСТ 51794-2008 :
// dX_Coord=25m  dY_Coord=-141m  dZ_Coord=-80m
// !!! Задать в Public
// Для изменения DATUM необходимо вызвать функциюf_Change_DATUM

// Изменение датума dX,dY,dZ

// Входные параметры:
// New_dX_Coord,New_dY_Coord,New_dZ_Coord - новый датум (м)

// Выходные параметры:
// dX_Coord,dY_Coord,dZ_Coord - !!! это д.б.глобальные переменные, в которых
// хранится DATUM (м) -> в них занесется новые значения датума
// ***********************************************************************
public void f_Change_DATUM
    (

        // Входные параметры (м) 
        double New_dX_Coord,
        double New_dY_Coord,
        double New_dZ_Coord,

        // Выходные параметры (м)
        ref double dX_Coord,
        ref double dY_Coord,
        ref double dZ_Coord

    )
{

    dX_Coord = New_dX_Coord;
    dY_Coord = New_dY_Coord;
    dZ_Coord = New_dZ_Coord;

} // Функция f_Change_DATUM
// ***********************************************************************

// *************************************************************************
// Выделение узлов связи (УС) из входного массива данных
// (тестовая функция)

// Входные данные:
// mas_Tst - массив, куда переписан основной файл данных в формате:
// N(номер ИРИ),X,Y(координаты ИРИ),F(частота ИРИ)
// number_s - количество источников в основном массиве
// dXY_Tst - погрешность определения по координатам
// dF1_Tst - погрешность определения по частоте в диапазоне частот1
// dF2_Tst - погрешность определения по частоте в диапазоне частот2
// dF3_Tst - погрешность определения по частоте в диапазоне частот3

// Выходные параметры
// mas_Out_Tst - Массив выходных данных в том же формате, что и входной
// mas_RN_Tst - дополнительный массив для УС в формате:
// количество источников в 1-ом УС,
// количество источников во 2-ом УС,
// и т.д.

// Функция возвращает количество УС

// *************************************************************************

    public uint CommunicationCenter(


                   // Входные параметры

                  // Массив входных данных в формате: N,X,Y,F
                  double[] mas_Tst,
                  uint number_s,
                  double dXY_Tst,
                  double dF1_Tst,
                  double dF2_Tst,
                  double dF3_Tst,

                  // Выходные параметры

                  // Массив выходных данных в формате: N,X,Y,F
                  ref double[] mas_Out_Tst,
                 // Массив в формате: 
                 //// число УС/РС,
                 // число ист-ов в 1-ом УС,
                 // число ист-ов во 2-ом УС, и т.д.
                  ref double[] mas_RN_Tst

                                      )
   {

 double temp_p1;
// -----------------------------------------------------------------------------
// Для сравнения частот и расстояний

double dF_tmp,dXY_tmp;
int idF_tmp;
double DX12,DY12,D12;
// -----------------------------------------------------------------------------
uint ind_mas_F1,ind_mas_F2,ind_mas_F3;
uint ipmas_F, ipmas_F_dubl, ipmas_F_dubl1;
uint ind_RN;

uint ipmas_F_RN, ipmas_F_RN1;

// Количество источников в основном массиве
uint number_s_dubl;

// Общее число рассматриваемых источников из основного файла
// (которые будем сравнивать с основным)
uint number_s_dubl1;

// Количество пропущенных источников с другими частотами
uint number_s_dubl2;

// Количество источников, записанных в masF
uint number_s_dubl3;
uint number_s_dubl3_1;
uint number_s_dubl3_2;

// Количество источников в mas_F
uint number_sF;

//double *pmas_file_dubl;
uint ipmas_file_dubl;
// -----------------------------------------------------------------------------
// Массив источников с определенной частотой
double[] mas_F = new double[4000];
// -----------------------------------------------------------------------------
uint imas_Out_Tst;
uint imas_RN_Tst;
// -----------------------------------------------------------------------------


// Обнуление массива
 Array.Clear(mas_F, 0, 4000);
 ipmas_F=0;
 ipmas_F_dubl=0;

 imas_Out_Tst = 0;
 imas_RN_Tst = 0;

 ipmas_F_RN = 0;
// -----------------------------------------------------------------------------
// Количество источников в основном файле

number_s_dubl=number_s;
// -----------------------------------------------------------------------------
// Указатель основного массива

ipmas_file_dubl=0;
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Количество источников в mas_F

number_sF=0;
number_s_dubl3=0;
// -----------------------------------------------------------------------------
// Количество УС

 ind_mas_F3=0;
// -----------------------------------------------------------------------------

// Начало(WHILE1) WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
// Берем 1-й источник, ищем все источники с его X,Y и переписываем в массив
// pmas_F (основной массив сдвигаем)

while(number_s_dubl!=0)
{

  // ...........................................................................
   ipmas_file_dubl=0;
   ipmas_F_dubl=0;

   number_s_dubl3=0;
  // ...........................................................................
  // Выбор погрешности по частоте и расстоянию


  if ((mas_Tst[3] > 0) && (mas_Tst[3] <= 1000))
      dF_tmp = dF1_Tst;
  else if ((mas_Tst[3] > 1000) && (mas_Tst[3] <= 2000))
      dF_tmp = dF2_Tst;
  else
      dF_tmp = dF3_Tst;

  dXY_tmp = dXY_Tst;
// ...........................................................................
  // 1-й источник из основного массива записываем в mas_F

   for (ind_mas_F1 = 0; ind_mas_F1 < 4; ind_mas_F1++)
   {
       mas_F[ipmas_F_dubl] = mas_Tst[ipmas_file_dubl];
       ipmas_F_dubl += 1;
       ipmas_file_dubl += 1;
   }

   // Восстанавливаем указатель основного массива
   ipmas_file_dubl = 0;

   // Убираем его из основного файла
   Del_Source(4 * number_s_dubl, 0, ref mas_Tst);

   // Количество источников в основном файле
   number_s_dubl--;

   // Количество источников в masF
   number_s_dubl3++;

// ...........................................................................


// Начало(WHILE2) WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
// Ищем все элементы из основного с такой же X,Y,если это радионаправление
//  -> записываем в файл

// Общее число рассматриваемых источников из основного файла
// (которые будем сравнивать с основным)
number_s_dubl1=number_s_dubl;
// Число оставленных источников с другими X,Y
number_s_dubl2=0;

while(number_s_dubl1!=0)
{

// Сравниваем записанный в masF[0] источник с последующим источником из
// основного массива

 // Расстояние между источниками
 DX12 = mas_F[ipmas_F + 1] - mas_Tst[ipmas_file_dubl + 1];
 DY12 = mas_F[ipmas_F + 2] - mas_Tst[ipmas_file_dubl + 2];
 D12 = Math.Sqrt(DX12 * DX12 + DY12 * DY12);

 // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 // IF1
 // Следующий источник из основного файла с той же X,Y
 if(D12<=dXY_tmp)

 {
  // Если Fi==Fj -> просто убираем его
  if (module(mas_F[ipmas_F + 3] - mas_Tst[ipmas_file_dubl + 3]) <= dF_tmp)

   {
    Del_Source(4 * (number_s_dubl - number_s_dubl2), ipmas_file_dubl, ref mas_Tst);

    // Количество источников в основном файле
    number_s_dubl--;
   }

   // Если нет -> переписываем в pmasF и убираем из основного файла
   else
   {

    for (ind_mas_F1 = 0; ind_mas_F1 < 4; ind_mas_F1++)
    {
        mas_F[ipmas_F_dubl] = mas_Tst[ipmas_file_dubl];
        ipmas_F_dubl += 1;
        ipmas_file_dubl += 1;
    }

    // Восстанавливаем указатель
    for (ind_mas_F1 = 0; ind_mas_F1 < 4; ind_mas_F1++)
        ipmas_file_dubl -= 1;

    // Убираем его из основного файла
    Del_Source(4 * (number_s_dubl - number_s_dubl2), ipmas_file_dubl, ref mas_Tst);


    number_s_dubl--;
    number_s_dubl3++;

   }

 } // IF1
 // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 // Этот источник с другой X,Y -> пропускаем и переходим к следующему

 else
 {
   number_s_dubl2++;

   ipmas_file_dubl = ipmas_file_dubl + 4;


 }
 // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

 number_s_dubl1--;
}; //

// WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHI Конец(WHILE2)

  // ...........................................................................
  // Проверяем, нет ли источников c Fi=Fj

  if(number_s_dubl3>2)
   {

     number_s_dubl3_1=number_s_dubl3-1; // 1-й уже проверили
     number_s_dubl3_2=0;

     ipmas_F_dubl = ipmas_F + 4;

     // WHILE3
     while(number_s_dubl3_1!=1)

     {

      ind_mas_F2=0;

      ipmas_F_dubl1 = ipmas_F_dubl + 4;

      number_s_dubl3_2=0;

      // WHILE4
      while(ind_mas_F2<(number_s_dubl3_1-1))
      {

       // Если он расположен рядом -> просто убираем его
       if (module(mas_F[ipmas_F_dubl + 3] - mas_F[ipmas_F_dubl1 + 3]) <= dF_tmp)

       {
        Del_Source( 4 * ((number_s_dubl3_1 - 1) - number_s_dubl3_2),ipmas_F_dubl1,ref mas_F);

        number_s_dubl3_1--;
        number_s_dubl3--;
        }
       else
       {
        number_s_dubl3_2++;

        ipmas_F_dubl1 += 4;

        ind_mas_F2++;
       }

      }; // WHILE4


      if(number_s_dubl3_1!=1)
      {
       number_s_dubl3_1--;

       ipmas_F_dubl += 4;

      }

     }; // while3


   } // number_s_dubl3>2
  // ...........................................................................
  // CommunicationCenters

  if(number_s_dubl3==0)
   break;
  else
  {
    ipmas_F_RN1 = ipmas_F;

    for(ind_RN=0;ind_RN<number_s_dubl3;ind_RN++)
    {
     temp_p1 = mas_F[ipmas_F_RN1];// N
     ipmas_F_RN1 += 1;
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

     temp_p1 = mas_F[ipmas_F_RN1];// X
     ipmas_F_RN1 += 1;
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

     temp_p1 = mas_F[ipmas_F_RN1];// Y
     ipmas_F_RN1 += 1;
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

     temp_p1 = mas_F[ipmas_F_RN1];// F
     ipmas_F_RN1 += 1;
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;


     }

    mas_RN_Tst[ipmas_F_RN] = number_s_dubl3;
     ipmas_F_RN += 1;

     ind_mas_F3++;
  }
// ...........................................................................


}; // while(number_s_dubl!=0)
// WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHI Конец(WHILE1)

// .......................................................................

      return(ind_mas_F3);
     } // P/P
// *************************************************************************


// *************************************************************************
// Выделение радионаправлений (РН)/радиосетей (РС) из входного массива данных
// (тестовая функция)

// Входные данные:
// mas_Tst - массив, куда переписан основной файл данных в формате:
// N(номер ИРИ),X,Y(координаты ИРИ),F(частота ИРИ)
// flag_Rad (=1 -> выделяем РН, =2 -> выделяем РС)
// number_s - количество источников в основном массиве
// dXY_Tst - погрешность определения по координатам
// dF1_Tst - погрешность определения по частоте в диапазоне частот1
// dF2_Tst - погрешность определения по частоте в диапазоне частот2
// dF3_Tst - погрешность определения по частоте в диапазоне частот3

// Выходные параметры
// mas_Out_Tst - Массив выходных данных в том же формате, что и входной
// mas_RN_Tst - дополнительный массив для РС в формате:
// количество источников в 1-й РС,
// количество источников во 2-й РС,
// и т.д.

// Функция возвращает количество РН/РС
// *************************************************************************

public uint RadioRoute(


                  // Входные параметры

                  // Массив входных данных в формате: N,X,Y,F
                  double[] mas_Tst,
                  int flag_Rad,
                  uint number_s,
                  double dXY_Tst,
                  double dF1_Tst,
                  double dF2_Tst,
                  double dF3_Tst,

                  // Выходные параметры

                  // Массив выходных данных в формате: N,X,Y,F
                  ref double[] mas_Out_Tst,
                  // Массив в формате: 
                  //// число УС/РС,
                  // число ист-ов в 1-ом УС/РС,
                  // число ист-ов во 2-ом УС/РС, и т.д.
                  ref double[] mas_RN_Tst


                                      )
    {



 double temp_p1;
// -----------------------------------------------------------------------------
// Для сравнения частот и расстояний

double dF_tmp,dXY_tmp;
int idF_tmp;
double DX12,DY12,D12;
// -----------------------------------------------------------------------------
uint ind_mas_F1,ind_mas_F2,ind_mas_F3;
//double *pmas_F,*pmas_F_dubl,*pmas_F_dubl1;
uint ipmas_F,ipmas_F_dubl,ipmas_F_dubl1;
uint ind_RN;
//double *pmas_F_RN,*pmas_F_RN1;
uint ipmas_F_RN, ipmas_F_RN1;

// Количество источников в основном массиве
uint number_s_dubl;

// Общее число рассматриваемых источников из основного файла
// (которые будем сравнивать с основным)
uint number_s_dubl1;

// Количество пропущенных источников с другими частотами
uint number_s_dubl2;

// Количество источников, записанных в masF
uint number_s_dubl3;
uint number_s_dubl3_1;
uint number_s_dubl3_2;

// Количество источников в mas_F
uint number_sF;

//double *pmas_file_dubl;
uint ipmas_file_dubl;

// -----------------------------------------------------------------------------
// Массив источников с определенной частотой
//double mas_F[4000];
double[] mas_F = new double[4000];
// -----------------------------------------------------------------------------
uint imas_Out_Tst;
uint imas_RN_Tst;
// -----------------------------------------------------------------------------

// Обнуление массива

// pmas_F=&mas_F[0];
// pmas_F_dubl=pmas_F;
// for(ind_mas_F1=0;ind_mas_F1<4000;ind_mas_F1++)
//     mas_F[ind_mas_F1]=0;
// -----------------------------------------------------------------------------
// Количество источников в основном файле
//number_s_dubl=number_s;
// -----------------------------------------------------------------------------
// Указатель основного массива
//pmas_file_dubl=pmas_file;
// -----------------------------------------------------------------------------
// Обнуление массива
 Array.Clear(mas_F, 0, 4000);
 ipmas_F=0;
 ipmas_F_dubl=0;

 imas_Out_Tst = 0;
 imas_RN_Tst = 0;

 ipmas_F_RN = 0;
// -----------------------------------------------------------------------------
// Количество источников в основном файле

number_s_dubl=number_s;
// -----------------------------------------------------------------------------
// Указатель основного массива

ipmas_file_dubl=0;
// -----------------------------------------------------------------------------
// Количество источников в mas_F

number_sF=0;
number_s_dubl3=0;
// -----------------------------------------------------------------------------
// Количество радионаправлений

 ind_mas_F3=0;
// -----------------------------------------------------------------------------


// Начало(WHILE1) WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
// Берем 1-й источник, ищем все источники с его частотой и переписываем в массив
// pmas_F (основной массив сдвигаем)

while(number_s_dubl!=0)
{

  // ...........................................................................
   //pmas_file_dubl=pmas_file;
   //pmas_F_dubl=pmas_F;
   //number_s_dubl3=0;

   ipmas_file_dubl=0;
   ipmas_F_dubl=0;
   number_s_dubl3=0;

  // ...........................................................................
  // Выбор погрешности по частоте и расстоянию

  //if((*(pmas_file+3)>0)&&(*(pmas_file+3)<=1000))
  //      dF_tmp=dF1;
  //else if((*(pmas_file+3)>1000)&&(*(pmas_file+3)<=2000))
  //      dF_tmp=dF2;
  //else
  //      dF_tmp=dF3;
  //dXY_tmp=dXY/1000.;

  if ((mas_Tst[3] > 0) && (mas_Tst[3] <= 1000))
      dF_tmp = dF1_Tst;
  else if ((mas_Tst[3] > 1000) && (mas_Tst[3] <= 2000))
      dF_tmp = dF2_Tst;
  else
      dF_tmp = dF3_Tst;
  dXY_tmp = dXY_Tst;
  // ...........................................................................
  // 1-й источник из основного массива записываем в mas_F

   //for(ind_mas_F1=0;ind_mas_F1<4;ind_mas_F1++)
   // *pmas_F_dubl++=*pmas_file_dubl++;

   for (ind_mas_F1 = 0; ind_mas_F1 < 4; ind_mas_F1++)
   {
       mas_F[ipmas_F_dubl] = mas_Tst[ipmas_file_dubl];
       ipmas_F_dubl += 1;
       ipmas_file_dubl += 1;
   }

   // Восстанавливаем указатель основного массива
   //pmas_file_dubl=pmas_file;
   ipmas_file_dubl = 0;

   // Убираем его из основного файла
   //Del_Source(pmas_file,4*number_s_dubl);
   Del_Source(4 * number_s_dubl, 0, ref mas_Tst);

   // Количество источников в основном файле
   number_s_dubl--;

   // Количество источников в masF
   number_s_dubl3++;

  // ...........................................................................


// Начало(WHILE2) WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
// Ищем все элементы из основного с такой же частотой,если это радионаправление
//  -> записываем в файл

// Общее число рассматриваемых источников из основного файла
// (которые будем сравнивать с основным)
number_s_dubl1=number_s_dubl;
// Число оставленных источников с другими частотами
number_s_dubl2=0;

while(number_s_dubl1!=0)
{


// Сравниваем записанный в masF[0] источник с последующим источником из
// основного массива

 // Расстояние между источниками
 //DX12 = *(pmas_F+1)-(*(pmas_file_dubl+1));
 //DY12 = *(pmas_F+2)-(*(pmas_file_dubl+2));
 //D12 = sqrt(DX12*DX12+DY12*DY12);
 DX12 = mas_F[ipmas_F + 1] - mas_Tst[ipmas_file_dubl + 1];
 DY12 = mas_F[ipmas_F + 2] - mas_Tst[ipmas_file_dubl + 2];
 D12 = Math.Sqrt(DX12 * DX12 + DY12 * DY12);

 // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 // IF1
 // Следующий источник из основного файла с той же частотой
 //if(Funcs->module(*(pmas_F+3)-(*(pmas_file_dubl+3)))<=dF_tmp)
   if (module(mas_F[ipmas_F + 3] - mas_Tst[ipmas_file_dubl + 3]) <= dF_tmp)

 {
  // Если он расположен около 1-го -> просто убираем его
  if(D12<=dXY_tmp)
   {
    //Del_Source(pmas_file_dubl,4*(number_s_dubl-number_s_dubl2));
    Del_Source(4 * (number_s_dubl - number_s_dubl2), ipmas_file_dubl, ref mas_Tst);

    // Количество источников в основном файле
    number_s_dubl--;
   }

   // Если нет -> переписываем в pmasF и убираем из основного файла
   else
   {
    //for(ind_mas_F1=0;ind_mas_F1<4;ind_mas_F1++)
    //  *pmas_F_dubl++=*pmas_file_dubl++;
    for (ind_mas_F1 = 0; ind_mas_F1 < 4; ind_mas_F1++)
     {
         mas_F[ipmas_F_dubl] = mas_Tst[ipmas_file_dubl];
         ipmas_F_dubl += 1;
         ipmas_file_dubl += 1;
     }

    // Восстанавливаем указатель
    //for(ind_mas_F1=0;ind_mas_F1<4;ind_mas_F1++)
    //  pmas_file_dubl--;
    for (ind_mas_F1 = 0; ind_mas_F1 < 4; ind_mas_F1++)
      ipmas_file_dubl -= 1;

    // Убираем его из основного файла
    //Del_Source(pmas_file_dubl,4*(number_s_dubl-number_s_dubl2));
    Del_Source(4 * (number_s_dubl - number_s_dubl2), ipmas_file_dubl, ref mas_Tst);

    number_s_dubl--;
    number_s_dubl3++;

   }

 } // IF1

 // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 // Этот источник с другой частотой -> пропускаем и переходим к следующему

 else
 {
   number_s_dubl2++;

   //pmas_file_dubl=pmas_file_dubl+4;
   ipmas_file_dubl = ipmas_file_dubl + 4;

 }
 // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

 number_s_dubl1--;
} //
// WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHI Конец(WHILE2)


  // ...........................................................................
  // Проверяем, нет ли рядом расположенных источников

  if(number_s_dubl3>2)
   {

     number_s_dubl3_1=number_s_dubl3-1; // 1-й уже проверили
     number_s_dubl3_2=0;

     //pmas_F_dubl=pmas_F+4;
     ipmas_F_dubl = ipmas_F + 4;

     // WHILE3
     while(number_s_dubl3_1!=1)

     {

      ind_mas_F2=0;

      //pmas_F_dubl1=pmas_F_dubl+4;
      ipmas_F_dubl1 = ipmas_F_dubl + 4;

      number_s_dubl3_2=0;

      // WHILE4
      while(ind_mas_F2<(number_s_dubl3_1-1))
      {
       // Расстояние между источниками
       //DX12 = *(pmas_F_dubl+1)-(*(pmas_F_dubl1+1));
       //DY12 = *(pmas_F_dubl+2)-(*(pmas_F_dubl1+2));
       //D12 = sqrt(DX12*DX12+DY12*DY12);
       DX12 = mas_F[ipmas_F_dubl + 1] - mas_F[ipmas_F_dubl1 + 1];
       DY12 = mas_F[ipmas_F_dubl + 2] - mas_F[ipmas_F_dubl1 + 2];
       D12 = Math.Sqrt(DX12 * DX12 + DY12 * DY12);

       // Если он расположен рядом -> просто убираем его
       if(D12<=dXY_tmp)
       {
        //Del_Source(pmas_F_dubl1,4*((number_s_dubl3_1-1)-number_s_dubl3_2));
        Del_Source(4 * ((number_s_dubl3_1 - 1) - number_s_dubl3_2), ipmas_F_dubl1, ref mas_F);

        number_s_dubl3_1--;
        number_s_dubl3--;
        }
       else
       {
        number_s_dubl3_2++;

        //pmas_F_dubl1+=4;
        ipmas_F_dubl1 += 4;

        ind_mas_F2++;
       }

      } // WHILE4


      if(number_s_dubl3_1!=1)
      {
       number_s_dubl3_1--;

       //pmas_F_dubl+=4;
       ipmas_F_dubl += 4;

      }

     } // while3


   } // number_s_dubl3>2

// ...........................................................................
  // Определение радионаправления

 if(flag_Rad==1)
 {
  if(number_s_dubl3<=1)
   break;
  else if(number_s_dubl3>2)
   break;
  else
  {
    //temp_p1 = *(pmas_F);// N1
    //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R10[0]);
     temp_p1 = mas_F[ipmas_F];// N1
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

    //temp_p1 = *(pmas_F+1);// X1
    //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R10[0]);
     temp_p1 = mas_F[ipmas_F+1];// X1
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

    //temp_p1 = *(pmas_F+2);// Y1
    //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R10[0]);
     temp_p1 = mas_F[ipmas_F+2];// Y1
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

    //temp_p1 = *(pmas_F+3);// F1
    //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R10[0]);
     temp_p1 = mas_F[ipmas_F+3];// F1
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

    //temp_p1 = *(pmas_F+4);// N2
    //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R10[0]);
     temp_p1 = mas_F[ipmas_F+4];// N2
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

    //temp_p1 = *(pmas_F+5);// X2
    //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R10[0]);
     temp_p1 = mas_F[ipmas_F+5];// X2
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

    //temp_p1 = *(pmas_F+6);// Y2
    //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R10[0]);
     temp_p1 = mas_F[ipmas_F+6];// Y2
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

    //temp_p1 = *(pmas_F+7);// F2
    //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R10[0]);
     temp_p1 = mas_F[ipmas_F+7];// F2
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

     ind_mas_F3++;


  }

 } // RadioRoute
  // ...........................................................................
  // RadioNet

 else if(flag_Rad==2)
 {
  if(number_s_dubl3<=1)
   break;
  else if(number_s_dubl3==2)
   break;
  else
  {
    //pmas_F_RN1=pmas_F;
    ipmas_F_RN1 = ipmas_F;


    for(ind_RN=0;ind_RN<number_s_dubl3;ind_RN++)
    {
     //temp_p1 = *pmas_F_RN1++;// N
     //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R10[0]);
     temp_p1 = mas_F[ipmas_F_RN1];// N
     ipmas_F_RN1 += 1;
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

     //temp_p1 = *pmas_F_RN1++;// X
     //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R10[0]);
     temp_p1 = mas_F[ipmas_F_RN1];// X
     ipmas_F_RN1 += 1;
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

     //temp_p1 = *pmas_F_RN1++;// Y
     //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R10[0]);
     temp_p1 = mas_F[ipmas_F_RN1];// Y
     ipmas_F_RN1 += 1;
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

     //temp_p1 = *pmas_F_RN1++;// F
     //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R10[0]);
     temp_p1 = mas_F[ipmas_F_RN1];// F
     ipmas_F_RN1 += 1;
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

     }

    //*pmas_fRN++=number_s_dubl3;
     mas_RN_Tst[ipmas_F_RN] = number_s_dubl3;
     ipmas_F_RN += 1;

     ind_mas_F3++;


  }

 } // RadioNet
  // ...........................................................................


} // while(number_s_dubl!=0)
// WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHI Конец(WHILE1)

      // .......................................................................
      // Закрыть файл данных

      //fclose(mfl_R10[0]);
      // .......................................................................

      return(ind_mas_F3);


    } // P/P
// *************************************************************************

// *************************************************************************
// Выделение радионаправлений (РН)/радиосетей (РС) из входного массива данных
// при отсутствии координат по пеленгу
// (тестовая функция)

// Входные данные:
// mas_Tst - массив, куда переписан основной файл данных в формате:
// N(номер ИРИ),Pel(пеленг, град),0,F(частота ИРИ)
// flag_Rad (=1 -> выделяем РН, =2 -> выделяем РС)
// number_s - количество источников в основном массиве
// dXY_Tst - погрешность определения по координатам(не нужен)
// dF1_Tst - погрешность определения по частоте в диапазоне частот1
// dF2_Tst - погрешность определения по частоте в диапазоне частот2
// dF3_Tst - погрешность определения по частоте в диапазоне частот3
// dPel_Tst - погрешность по пеленгу, град

// Выходные параметры
// mas_Out_Tst - Массив выходных данных в том же формате, что и входной
// mas_RN_Tst - дополнительный массив для РС в формате:
// количество источников в 1-й РС,
// количество источников во 2-й РС,
// и т.д.

// Функция возвращает количество РН/РС
// *************************************************************************

public uint PelRadioRoute(


                  // Входные параметры

                  // Массив входных данных в формате: N,Pel,0,F
                  double[] mas_Tst,
                  int flag_Rad,
                  uint number_s,
                  double dXY_Tst,
                  double dF1_Tst,
                  double dF2_Tst,
                  double dF3_Tst,
                  double dPel_Tst,

                  // Выходные параметры

                  // Массив выходных данных в формате: N,Pel,0,F
                  ref double[] mas_Out_Tst,
                  // Массив в формате: 
                  //// число УС/РС,
                  // число ист-ов в 1-ом УС/РС,
                  // число ист-ов во 2-ом УС/РС, и т.д.
                  ref double[] mas_RN_Tst


                                      )
{


double temp_p1;
// -----------------------------------------------------------------------------
// Для сравнения частот и расстояний

double dF_tmp,dPel_tmp;
int idF_tmp;
double DX12,DY12,D12;
// -----------------------------------------------------------------------------
uint ind_mas_F1,ind_mas_F2,ind_mas_F3;
//double *pmas_F,*pmas_F_dubl,*pmas_F_dubl1;
uint ipmas_F,ipmas_F_dubl,ipmas_F_dubl1;
uint ind_RN;
//double *pmas_F_RN,*pmas_F_RN1;
uint ipmas_F_RN, ipmas_F_RN1;

// Количество источников в основном массиве
uint number_s_dubl;
// Общее число рассматриваемых источников из основного файла
// (которые будем сравнивать с основным)
uint number_s_dubl1;
// Количество пропущенных источников с другими частотами
uint number_s_dubl2;
// Количество источников, записанных в masF
uint number_s_dubl3;
uint number_s_dubl3_1;
uint number_s_dubl3_2;

// Флаг нахождения первого пеленга
uint flag_Pel1;

// Количество источников в mas_F
uint number_sF;

//double *pmas_file_dubl;
uint ipmas_file_dubl;

// -----------------------------------------------------------------------------
// Массив источников с определенной частотой
//double mas_F[4000];
double[] mas_F = new double[4000];
// -----------------------------------------------------------------------------
uint imas_Out_Tst;
uint imas_RN_Tst;
// -----------------------------------------------------------------------------
// Обнуление массива

 //pmas_F=&mas_F[0];
 //pmas_F_dubl=pmas_F;
 //for(ind_mas_F1=0;ind_mas_F1<4000;ind_mas_F1++)
 //    mas_F[ind_mas_F1]=0;
 // -----------------------------------------------------------------------------
 // Обнуление массива
 Array.Clear(mas_F, 0, 4000);
 ipmas_F = 0;
 ipmas_F_dubl = 0;

 imas_Out_Tst = 0;
 imas_RN_Tst = 0;

 ipmas_F_RN = 0;
 // -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Количество источников в основном файле

number_s_dubl=number_s;
// -----------------------------------------------------------------------------
// Указатель основного массива

//pmas_file_dubl=pmas_file;
ipmas_file_dubl = 0;
// -----------------------------------------------------------------------------
// Количество источников в mas_F

number_sF=0;
number_s_dubl3=0;
// -----------------------------------------------------------------------------
// Количество радионаправлений

 ind_mas_F3=0;
// -----------------------------------------------------------------------------

// Начало(WHILE1) WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
// Берем 1-й источник, ищем все источники с его частотой и переписываем в массив
// pmas_F (основной массив сдвигаем)

while(number_s_dubl!=0)
{

  // ...........................................................................
   //pmas_file_dubl=pmas_file;
   //pmas_F_dubl=pmas_F;
   //number_s_dubl3=0;
   ipmas_file_dubl = 0;
   ipmas_F_dubl = 0;
   number_s_dubl3 = 0;
  // ...........................................................................
  // Выбор погрешности по частоте и расстоянию

  //if((*(pmas_file+3)>0)&&(*(pmas_file+3)<=1000))
  //      dF_tmp=dF1;
  //else if((*(pmas_file+3)>1000)&&(*(pmas_file+3)<=2000))
  //      dF_tmp=dF2;
  //else
  //      dF_tmp=dF3;
  if ((mas_Tst[3] > 0) && (mas_Tst[3] <= 1000))
      dF_tmp = dF1_Tst;
  else if ((mas_Tst[3] > 1000) && (mas_Tst[3] <= 2000))
      dF_tmp = dF2_Tst;
  else
      dF_tmp = dF3_Tst;

  dPel_tmp=dPel_Tst;


  // ...........................................................................
  // 1-й источник==пеленг из основного массива записываем в mas_F

  // Флаг нахождения первого пеленга
  flag_Pel1=0;

 while(
       (flag_Pel1==0)&&
       (number_s_dubl!=0)
       )
 {
  // Это пеленг
  //if(*(pmas_file_dubl+2)==0)
  if (mas_Tst[ipmas_file_dubl + 2] == 0)

  {
   //for(ind_mas_F1=0;ind_mas_F1<4;ind_mas_F1++)
   // *pmas_F_dubl++=*pmas_file_dubl++;
   for (ind_mas_F1 = 0; ind_mas_F1 < 4; ind_mas_F1++)
   {
       mas_F[ipmas_F_dubl] = mas_Tst[ipmas_file_dubl];
       ipmas_F_dubl += 1;
       ipmas_file_dubl += 1;
   }

   // Восстанавливаем указатель основного массива
   //pmas_file_dubl=pmas_file;
   ipmas_file_dubl = 0;

   // Убираем его из основного файла
   //Del_Source(pmas_file,4*number_s_dubl);
   Del_Source(4 * number_s_dubl, 0, ref mas_Tst);

   // Количество источников в основном файле
   number_s_dubl--;

   // Количество источников в masF
   number_s_dubl3++;

   flag_Pel1=1;

  } // IF(Это пеленг)

  // Это не пеленг
  else
  {
   // Убираем его из основного файла
   //Del_Source(pmas_file,4*number_s_dubl);
   Del_Source(4 * number_s_dubl, 0, ref mas_Tst);

   // Количество источников в основном файле
   number_s_dubl--;
  }

 } // while(flag_Pel1==0)
  // ...........................................................................

  // !!! -> Остались только не пеленги
  if(number_s_dubl==0)
    break;

// STOP
// Начало(WHILE2) WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
// Ищем все элементы из основного с такой же частотой,если это радионаправление
//  -> записываем в файл

// Общее число рассматриваемых источников из основного файла
// (которые будем сравнивать с основным)
number_s_dubl1=number_s_dubl;
// Число оставленных источников с другими частотами
number_s_dubl2=0;

while(number_s_dubl1!=0)
{


// Сравниваем записанный в masF[0] источник с последующим источником из
// основного массива

 // Разница по пеленгу между источниками
 //DX12 = *(pmas_F+1)-(*(pmas_file_dubl+1));
 //DY12 = *(pmas_F+2)-(*(pmas_file_dubl+2));
 //D12 = sqrt(DX12*DX12+DY12*DY12);
  //D12=*(pmas_F+1)-(*(pmas_file_dubl+1));
  D12 = mas_F[ipmas_F + 1] - mas_Tst[ipmas_file_dubl + 1];

 // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 // IF1
 // Следующий источник из основного файла с той же частотой и это пеленг!!!

 //if(
 //    (Funcs->module(*(pmas_F+3)-(*(pmas_file_dubl+3)))<=dF_tmp)&&
 //    (*(pmas_file_dubl+2)==0) // Y=0 -> это только пеленг
 //   )

     if (
         (module(mas_F[ipmas_F + 3] - mas_Tst[ipmas_file_dubl + 3]) <= dF_tmp) &&
         (mas_Tst[ipmas_file_dubl + 2] == 0) // Y=0 -> это только пеленг
        )

 {
  // Если он расположен около 1-го по пеленгу -> просто убираем его
  if(module(D12)<=dPel_tmp)
   {
    //Del_Source(pmas_file_dubl,4*(number_s_dubl-number_s_dubl2));
    Del_Source(4 * (number_s_dubl - number_s_dubl2), ipmas_file_dubl, ref mas_Tst);

    // Количество источников в основном файле
    number_s_dubl--;
   }

   // Если нет -> переписываем в pmasF и убираем из основного файла
   else
   {
    //for(ind_mas_F1=0;ind_mas_F1<4;ind_mas_F1++)
    //  *pmas_F_dubl++=*pmas_file_dubl++;

    for (ind_mas_F1 = 0; ind_mas_F1 < 4; ind_mas_F1++)
     {
        mas_F[ipmas_F_dubl] = mas_Tst[ipmas_file_dubl];
        ipmas_F_dubl += 1;
        ipmas_file_dubl += 1;
     }

    // Восстанавливаем указатель
    //for(ind_mas_F1=0;ind_mas_F1<4;ind_mas_F1++)
    //  pmas_file_dubl--;
    for (ind_mas_F1 = 0; ind_mas_F1 < 4; ind_mas_F1++)
       ipmas_file_dubl -= 1;

    // Убираем его из основного файла
    //Del_Source(pmas_file_dubl,4*(number_s_dubl-number_s_dubl2));
    Del_Source(4 * (number_s_dubl - number_s_dubl2), ipmas_file_dubl, ref mas_Tst);

    number_s_dubl--;
    number_s_dubl3++;

   }

 } // IF1
 // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 // Этот источник с другой частотой/не пеленг -> пропускаем и переходим к следующему

 else
 {
   number_s_dubl2++;

   //pmas_file_dubl=pmas_file_dubl+4;
   ipmas_file_dubl = ipmas_file_dubl + 4;

 }
 // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

 number_s_dubl1--;
} //
// WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHI Конец(WHILE2)

  // ...........................................................................
  // Проверяем, нет ли рядом расположенных источников

  if(number_s_dubl3>2)
   {

     number_s_dubl3_1=number_s_dubl3-1; // 1-й уже проверили
     number_s_dubl3_2=0;

     //pmas_F_dubl=pmas_F+4;
     ipmas_F_dubl = ipmas_F + 4;


     // WHILE3
     while(number_s_dubl3_1!=1)

     {

      ind_mas_F2=0;

      //pmas_F_dubl1=pmas_F_dubl+4;
      ipmas_F_dubl1 = ipmas_F_dubl + 4;

      number_s_dubl3_2=0;

      // WHILE4
      while(ind_mas_F2<(number_s_dubl3_1-1))
      {
       // Расстояние между источниками по пеленгу
       //D12=*(pmas_F_dubl+1)-(*(pmas_F_dubl1+1));
       D12 = mas_F[ipmas_F_dubl + 1] - mas_F[ipmas_F_dubl1 + 1];

       // Если он расположен рядом -> просто убираем его
       if(module(D12)<=dPel_tmp)
       {
        //Del_Source(pmas_F_dubl1,4*((number_s_dubl3_1-1)-number_s_dubl3_2));
        Del_Source(4 * ((number_s_dubl3_1 - 1) - number_s_dubl3_2), ipmas_F_dubl1, ref mas_F);

        number_s_dubl3_1--;
        number_s_dubl3--;
        }
       else
       {
        number_s_dubl3_2++;

        //pmas_F_dubl1+=4;
        ipmas_F_dubl1 += 4;

        ind_mas_F2++;
       }

      } // WHILE4

      if(number_s_dubl3_1!=1)
      {
       number_s_dubl3_1--;

       //pmas_F_dubl+=4;
       ipmas_F_dubl += 4;

      }

     } // while3


   } // number_s_dubl3>2
// ...........................................................................
// Определение радионаправления

 if(flag_Rad==1)
 {
  if(number_s_dubl3<=1)
   break;
  else if(number_s_dubl3>2)
   break;
  else
  {
    //temp_p1 = *(pmas_F);// N1
    //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R13[0]);
    temp_p1 = mas_F[ipmas_F];// N1
    mas_Out_Tst[imas_Out_Tst] = temp_p1;
    imas_Out_Tst += 1;

    //temp_p1 = *(pmas_F+1);// Pel1
    //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R13[0]);
    temp_p1 = mas_F[ipmas_F+1];// Pel1
    mas_Out_Tst[imas_Out_Tst] = temp_p1;
    imas_Out_Tst += 1;

    //temp_p1 = *(pmas_F+2);// Y1=0
    //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R13[0]);
    temp_p1 = mas_F[ipmas_F + 2];// Y1=0
    mas_Out_Tst[imas_Out_Tst] = temp_p1;
    imas_Out_Tst += 1;

    //temp_p1 = *(pmas_F+3);// F1
    //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R13[0]);
    temp_p1 = mas_F[ipmas_F + 3];// F1
    mas_Out_Tst[imas_Out_Tst] = temp_p1;
    imas_Out_Tst += 1;

    //temp_p1 = *(pmas_F+4);// N2
    //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R13[0]);
    temp_p1 = mas_F[ipmas_F + 4];// N2
    mas_Out_Tst[imas_Out_Tst] = temp_p1;
    imas_Out_Tst += 1;

    //temp_p1 = *(pmas_F+5);// Pel2
    //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R13[0]);
    temp_p1 = mas_F[ipmas_F + 5];// Pel2
    mas_Out_Tst[imas_Out_Tst] = temp_p1;
    imas_Out_Tst += 1;

    //temp_p1 = *(pmas_F+6);// Y2=0
    //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R13[0]);
    temp_p1 = mas_F[ipmas_F + 6];// Y2=0
    mas_Out_Tst[imas_Out_Tst] = temp_p1;
    imas_Out_Tst += 1;

    //temp_p1 = *(pmas_F+7);// F2
    //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R13[0]);
    temp_p1 = mas_F[ipmas_F + 7];// F2
    mas_Out_Tst[imas_Out_Tst] = temp_p1;
    imas_Out_Tst += 1;

    ind_mas_F3++;

  }

 } // RadioRoute
  // ...........................................................................
  // RadioNet

 else if(flag_Rad==2)
 {
  if(number_s_dubl3<=1)
   break;
  else if(number_s_dubl3==2)
   break;
  else
  {
    //pmas_F_RN1=pmas_F;
    ipmas_F_RN1 = ipmas_F;


    for(ind_RN=0;ind_RN<number_s_dubl3;ind_RN++)
    {
     //temp_p1 = *pmas_F_RN1++;// N
     //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R13[0]);
     temp_p1 = mas_F[ipmas_F_RN1];// N
     ipmas_F_RN1 += 1;
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

     //temp_p1 = *pmas_F_RN1++;// Pel
     //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R13[0]);
     temp_p1 = mas_F[ipmas_F_RN1];// Pel
     ipmas_F_RN1 += 1;
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

     //temp_p1 = *pmas_F_RN1++;// Y=0
     //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R13[0]);
     temp_p1 = mas_F[ipmas_F_RN1];// Y=0
     ipmas_F_RN1 += 1;
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

     //temp_p1 = *pmas_F_RN1++;// F
     //fwrite((char*)&temp_p1,sizeof(double),1,mfl_R13[0]);
     temp_p1 = mas_F[ipmas_F_RN1];// F
     ipmas_F_RN1 += 1;
     mas_Out_Tst[imas_Out_Tst] = temp_p1;
     imas_Out_Tst += 1;

     }

    //*pmas_fRN++=number_s_dubl3;
     mas_RN_Tst[ipmas_F_RN] = number_s_dubl3;
     ipmas_F_RN += 1;

     ind_mas_F3++;

  }

 } // RadioNet
  // ...........................................................................


} // while(number_s_dubl!=0)
// WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHI Конец(WHILE1)

// .......................................................................
// Закрыть файл данных

 //fclose(mfl_R13[0]);
// .......................................................................

 return(ind_mas_F3);



} // P/P
// *************************************************************************

// *************************************************************************
// Проверка: какой параметр проверять в БД
// возврат 1-> 1-й
//         2-> 2-й
//         -1->код модуляции неопределен
// *************************************************************************

public int f_ParMod1(

             int kod_mod2

                     )
{

    switch(kod_mod2)
    {
    case 3:
    case 10:
    case 11:
    case 12:
    case 18:
    case 19:
          return 1;
    break;

    case 1:
    case 2:
    case 4:
    case 5:
           return 2;
    break;


    };

    return -1;

} // P/P f_ParMod1

// *************************************************************************

// *************************************************************************
// Чтение БД из файла
// Возвращает число записей
// *************************************************************************

public int f_ReadBD(

              // For Reading
              string strFileName,

             // Для возврата наименования РЭС
             ref string[] mas_name_RES,

              // Основная БД
              ref BD[] mass_BD

                     )
{
// -----------------------------------------------------------------------
    String strLine = "";
    String strLineWrite = "{5,666}";

    String strLine1 = "";
    String strLine2 = "";

    double number1;
    int number2;

    char symb1 = '{';
    char symb2 = '-';
    char symb3 = ';';
    char symb4 = '}';

    int indStart;
    int indStop;
    int iLength;

    int IndZap;
    int TekPoz;

// ----------------------------------------------------------------------

StreamReader srFile = new StreamReader(strFileName);
// -----------------------------------------------------------------------
IndZap = 0;


// Чтение

   try
   {

    TekPoz = 0;
    strLine = srFile.ReadLine(); // 1-я строка

    while (strLine != "")
    {
      // ...................................................................
      // F1

      indStart = strLine.IndexOf(symb1,TekPoz);
      indStop = strLine.IndexOf(symb2,TekPoz);
      iLength = indStop - indStart + 1;
      strLine1 = strLine.Substring(indStart + 1, iLength - 2);
      number1 = Convert.ToDouble(strLine1);

      mass_BD[IndZap].Fmin = number1;

      TekPoz = indStop-1;
    // ...................................................................
    // F2

      indStart = strLine.IndexOf(symb2, TekPoz);
      indStop = strLine.IndexOf(symb3, TekPoz);
      iLength = indStop - indStart + 1;
      strLine1 = strLine.Substring(indStart + 1, iLength - 2);
      number1 = Convert.ToDouble(strLine1);

      mass_BD[IndZap].Fmax = number1;

      TekPoz = indStop-1;
     // ...................................................................
     // Код модуляции

      indStart = strLine.IndexOf(symb3, TekPoz);
      indStop = strLine.IndexOf(symb3, TekPoz+2);
      iLength = indStop - indStart + 1;
      strLine1 = strLine.Substring(indStart + 1, iLength - 2);
      number2 = Convert.ToInt32(strLine1);

      mass_BD[IndZap].kod_mod = number2;

      TekPoz = indStop-1;
     // ...................................................................
     // Параметр модуляции (длительность импульса) P1min

      indStart = strLine.IndexOf(symb3, TekPoz);
      indStop = strLine.IndexOf(symb2, TekPoz);
      iLength = indStop - indStart + 1;
      strLine1 = strLine.Substring(indStart + 1, iLength - 2);
      number1 = Convert.ToDouble(strLine1);

      mass_BD[IndZap].Par1_Min = number1;

      TekPoz = indStop - 1;
     // ...................................................................
      // Параметр модуляции (длительность импульса) P1max

      indStart = strLine.IndexOf(symb2, TekPoz);
      indStop = strLine.IndexOf(symb3, TekPoz);
      iLength = indStop - indStart + 1;
      strLine1 = strLine.Substring(indStart + 1, iLength - 2);
      number1 = Convert.ToDouble(strLine1);

      mass_BD[IndZap].Par1_Max = number1;

      TekPoz = indStop - 1;
     // ...................................................................
     // Параметр модуляции (длительность импульса) P2min

      indStart = strLine.IndexOf(symb3, TekPoz);
      indStop = strLine.IndexOf(symb2, TekPoz);
      iLength = indStop - indStart + 1;
      strLine1 = strLine.Substring(indStart + 1, iLength - 2);
      number1 = Convert.ToDouble(strLine1);

      mass_BD[IndZap].Par2_Min = number1;

      TekPoz = indStop - 1;
     // ...................................................................
      // Параметр модуляции (длительность импульса) P2max

      indStart = strLine.IndexOf(symb2, TekPoz);
      indStop = strLine.IndexOf(symb3, TekPoz);
      iLength = indStop - indStart + 1;
      strLine1 = strLine.Substring(indStart + 1, iLength - 2);
      number1 = Convert.ToDouble(strLine1);

      mass_BD[IndZap].Par2_Max = number1;

      TekPoz = indStop - 1;
     // ...................................................................
     // Имя РЭС


      indStart = strLine.IndexOf(symb3, TekPoz);
      indStop = strLine.IndexOf(symb4, TekPoz);
      iLength = indStop - indStart + 1;
      strLine1 = strLine.Substring(indStart + 1, iLength - 2);

      mas_name_RES[IndZap] = strLine1;
     // ...................................................................
      IndZap += 1;
      TekPoz = 0;

      // Вся запись -> следующая строка
      strLine = srFile.ReadLine();

     // ...................................................................


    } // WHILE
   }
   catch
   {
   }
// ----------------------------------------------------------------------
  srFile.Close();
// -----------------------------------------------------------------------

  return IndZap;

} // P/P f_ReadBD

// *************************************************************************


// *************************************************************************
// Идентификация РЭС

// strFileName - имя файла с данными
// InpF - частота, кГц
// InpKodMod - код модуляции
// InpParMod - длительность импульса, мс
// InpParMod1 - ширина спектра, кГц

// Возврат: имя РЭС
// *************************************************************************
public string f_SelectR(


                     // For Reading
                     //string strFileName,
                     // Частота
                     double InpF,
                     // Код модуляции
                     int InpKodMod,
                     // Параметр модуляции (длительность импульса)
                     double InpParMod,
                     // Параметр модуляции (ширина спектра)
                     double InpParMod1

                     )


{

// ------------------------------------------------------------------------
// Размер БД
int Size_BD;

int sch1,sch2;
int kout;

Size_BD = 100;

sch1 = 0;
sch2 = 0;
kout = 0;
// ------------------------------------------------------------------------
// Для возврата наименования РЭС   
    
string[] mas_name_RES=new string[Size_BD];
// ------------------------------------------------------------------------
// Основная БД

BD[] mass_BD = new BD[Size_BD];
// ------------------------------------------------------------------------    
// Чтение БД из файла

if (flInitBD == 0)
{
    Size_BD = f_ReadBD(

                      // For Reading
                      strFileName,

                      // Для возврата наименования РЭС
                      ref mas_name_RES,

                     // Основная БД
                     ref mass_BD

                       );

    flInitBD = 1;
}
// ------------------------------------------------------------------------

// FOR1
for (sch1 = 0; sch1 < Size_BD; sch1++)
{

 // ........................................................................
 // Нашли частоту

 // IF1
    if ((InpF >= mass_BD[sch1].Fmin) && (InpF <= mass_BD[sch1].Fmax))
 {

     
   // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   // Проверка кода модуляции

         if (InpKodMod == mass_BD[sch1].kod_mod)
         {
          // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
          // Проверяем параметры модуляции

           // какой параметр проверять
           kout=f_ParMod1(InpKodMod);

           // !!! Нашли
           // IF3
           if ((kout == 1) && (InpParMod >= mass_BD[sch1].Par1_Min) && (InpParMod <= mass_BD[sch1].Par1_Max))
          {

              return mas_name_RES[sch1];

          } // IF3

           else if ((kout == 2) && (InpParMod1 >= mass_BD[sch1].Par2_Min) && (InpParMod1 <= mass_BD[sch1].Par2_Max))
           {

               return mas_name_RES[sch1];


           } // IF3

         // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

         } // IF2 нашли код модуляции

//     } // FOR2
   // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

 } // IF1 нашли частоту
 // ........................................................................

} // FOR1

// -------------------------------------------------------------------------

return "";

} // P/P f_SelectR
// *************************************************************************


 

    } // class Peleng

} // namespace
