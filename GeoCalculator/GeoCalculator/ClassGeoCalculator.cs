﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
//using System.Drawing.Imaging;
using System.Windows.Input;

/*
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Windows.Input;
*/

namespace GeoCalculator
{
    public static class ClassGeoCalculator
    {


        // VARS *******************************************************************************



        // ******************************************************************************* VARS


        // Конструктор *************************************************************************

        //ClassGeoCalculator()
        //{



        //} // Конструктор
        // ************************************************************************* Конструктор

        // *************************************************************************************
        // Module
        // *************************************************************************************
        public static double module(double a)
        {
            if (a >= 0) return (a);
            else return (-a);

        } // P/P module
        // *************************************************************************************

        // *************************************************************************************
        // Module
        // *************************************************************************************

        public static int f_validate(ref double latitude, ref double longitude)
        {
            /*
                        if (latitude < -90.0 || latitude > 90.0 || longitude < -180.0
                            || longitude > 180.0)
                        {
                            return -1;
                        }
            */

            if (latitude < -90.0)
                latitude = -90;
            if (latitude > 90.0)
                latitude = 90;

            if (longitude < -180.0)
                longitude = -180;
            if (longitude > 180.0)
                longitude = 180;

            return 0;
        }
        // *************************************************************************************
        // Пересчет пространственные геоцентрические координаты на эллипсоиде WGS84 (XYZ) 
        //  в топоцентрическую прямоугольную систему координат с центром в точке стояния РЛС
        // Y-  в зенит по местной нормали к поверхности эллипсоида 
        // X,Z - в горизонтальной плоскости и образуют правую систему координат
        // Направление Z задается в горизонтальной плоскости азимутом, отсчитываемым от направления на север по часовой стрелке

        // *************************************************************************************
        public static void Geoc_Topoc(
                                      // RLS
                                      double LatRLS,
                                      double LonRLS,
                                      double HRLS,
                                      double XGeo,
                                      double YGeo,
                                      double ZGeo,
                                      ref double XTop,
                                      ref double YTop,
                                      ref double ZTop

                                     )
        {

            double rnz = 0;
            double deltaz = 0;
            double rzz = 0;
            double azz = 0;
            double bzz = 0;
            double fzz = 0;
            double Blat = 0;
            azz = 6378137;
            bzz = 6356000;
            fzz = (azz - bzz) / azz;
            rzz = azz * (1 - fzz * Math.Sin(LatRLS) * Math.Sin(LatRLS));
            if (((1 - fzz) * (1 - fzz)) > 1E-15)
                Blat = Math.Atan(Math.Tan(LatRLS) / ((1 - fzz) * (1 - fzz)));
            else
                Blat = Math.Atan(Math.Tan(LatRLS) / (1E-15));

            if (Math.Cos(Blat) > 1E-15)
                rnz = (rzz * Math.Sin(LatRLS)) / Math.Sin(Blat);
            else
                rnz = (rzz * Math.Sin(LatRLS)) / 1E-15;

            deltaz = rzz * Math.Cos(LatRLS) - rnz * Math.Cos(Blat);


                // Координаты станций в МЗСК
            XTop = XGeo * Math.Sin(LonRLS) -
                   YGeo * Math.Cos(LonRLS);
            YTop = XGeo * Math.Cos(LonRLS) * Math.Cos(Blat) +
                   YGeo * Math.Sin(LonRLS) * Math.Cos(Blat) +
                   ZGeo * Math.Sin(Blat) -
                   rnz - HRLS - deltaz * Math.Cos(Blat);
            ZTop = -XGeo * Math.Cos(LonRLS) * Math.Sin(Blat) -
                    YGeo * Math.Sin(LonRLS) * Math.Sin(Blat) +
                    ZGeo * Math.Cos(Blat) +
                    deltaz * Math.Sin(Blat);


        }
        // *************************************************************************************

        // *************************************************************************************
        //  Пересчет координат из топоцентрической прямоугольной систему координат с центром в точке стояния РЛС
        //  (Y-  в зенит по местной нормали к поверхности эллипсоида 
        //  X,Z - в горизонтальной плоскости и образуют правую систему координат
        //  Направление Z задается в горизонтальной плоскости азимутом, отсчитываемым от направления на север по часовой стрелке)
        //  в пространственные геоцентрические координаты на эллипсоиде WGS84 (XYZ) 
        // *************************************************************************************
        public static void Topoc_Geoc(
                                       // RLS
                                      double LatRLS,
                                      double LonRLS,
                                      double HRLS,
                                      double XTop,
                                      double YTop,
                                      double ZTop,
                                      ref double XGeo,
                                      ref double YGeo,
                                      ref double ZGeo

                                     )
        {

            double rnz = 0;
            double deltaz = 0;
            double rzz = 0;
            double azz = 0;
            double bzz = 0;
            double fzz = 0;
            double Blat = 0;
            azz = 6378137;
            bzz = 6356000;
            fzz = (azz - bzz) / azz;
            rzz = azz * (1 - fzz * Math.Sin(LatRLS) * Math.Sin(LatRLS));
            if (((1 - fzz) * (1 - fzz)) > 1E-15)
                Blat = Math.Atan(Math.Tan(LatRLS) / ((1 - fzz) * (1 - fzz)));
            else
                Blat = Math.Atan(Math.Tan(LatRLS) / (1E-15));

            if (Math.Cos(Blat) > 1E-15)
                rnz = (rzz * Math.Sin(LatRLS)) / Math.Sin(Blat);
            else
                rnz = (rzz * Math.Sin(LatRLS)) / 1E-15;

            deltaz = rzz * Math.Cos(LatRLS) - rnz * Math.Cos(Blat);


            // Координаты станций в МЗСК
            XGeo = XTop * Math.Sin(LonRLS) +
                    YTop * Math.Cos(LonRLS) * Math.Cos(Blat) -
                    ZTop * Math.Cos(LonRLS) * Math.Sin(Blat) +
                    Math.Cos(LonRLS) * (rnz * Math.Cos(Blat) + HRLS * Math.Cos(Blat) + deltaz);

            YGeo = -XTop * Math.Cos(LonRLS) +
                    YTop * Math.Sin(LonRLS) * Math.Cos(Blat) -
                    ZTop * Math.Sin(LonRLS) * Math.Sin(Blat) +
                    Math.Sin(LonRLS) * (rnz * Math.Cos(Blat) + HRLS * Math.Cos(Blat) + deltaz);

            ZGeo = YTop * Math.Sin(Blat) +
                    ZTop * Math.Cos(Blat) +
                    Math.Sin(Blat) * (rnz + HRLS);

        }
        // *************************************************************************************

        // *************************************************************************************
        // Пересчет пространственных геоцентрических координат на эллипсоиде WGS84 (XYZ) 
        //  в местную земную систему координат с центром в точке стояния РЛС
        // Z-  в зенит по местной нормали к поверхности эллипсоида (высота)
        // X-север Y(запад) - в горизонтальной плоскости и образуют правую систему координат

        // *************************************************************************************
        public static void Geoc_MZSK(
            // RLS
                                      double LatRLS,
                                      double LonRLS,
                                      double XRLSGeo,
                                      double YRLSGeo,
                                      double ZRLSGeo,
                                      double XGeo,
                                      double YGeo,
                                      double ZGeo,
                                      ref double XMZSK,
                                      ref double YMZSK,
                                      ref double ZMZSK
                                     )
        {

            // Координаты в МЗСК
            XMZSK = -(XGeo - XRLSGeo) * Math.Sin(LatRLS) * Math.Cos(LonRLS) -
                     (YGeo - YRLSGeo) * Math.Sin(LatRLS) * Math.Sin(LonRLS) +
                     (ZGeo - ZRLSGeo) * Math.Cos(LatRLS);
            //YMZSK = -(XGeo - XRLSGeo) * Math.Sin(LonRLS) +
            //       (YGeo - YRLSGeo) * Math.Cos(LonRLS);
            YMZSK =  (XGeo - XRLSGeo) * Math.Sin(LonRLS) -
                     (YGeo - YRLSGeo) * Math.Cos(LonRLS);
            ZMZSK =  (XGeo - XRLSGeo) * Math.Cos(LatRLS) * Math.Cos(LonRLS) +
                     (YGeo - YRLSGeo) * Math.Cos(LatRLS) * Math.Sin(LonRLS) +
                     (ZGeo - ZRLSGeo) * Math.Sin(LatRLS);

        }
        // *************************************************************************************

        // *************************************************************************************
        // Пересчет пространственных геоцентрических координат на эллипсоиде WGS84 (XYZ) 
        //  в местную земную систему координат с центром в точке стояния РЛС
        // *************************************************************************************
        public static void Geoc_MZSK_V(
                                      // RLS
                                      double LatRLS,
                                      double LonRLS,
                                      double XRLSGeo,
                                      double YRLSGeo,
                                      double ZRLSGeo,
                                      double XGeo,
                                      double YGeo,
                                      double ZGeo,
                                      ref double XMZSK,
                                      ref double YMZSK,
                                      ref double ZMZSK
                                     )
        {

            // Координаты в МЗСК
            XMZSK = (XGeo - XRLSGeo) * (-Math.Sin(LatRLS) * Math.Cos(LonRLS)) +
                     (YGeo - YRLSGeo) *(- Math.Sin(LatRLS) * Math.Sin(LonRLS)) +
                     (ZGeo - ZRLSGeo) * Math.Cos(LatRLS);
            YMZSK = (XGeo - XRLSGeo) * (Math.Cos(LatRLS) * Math.Cos(LonRLS)) +
                     (YGeo - YRLSGeo) * (Math.Cos(LatRLS)*Math.Sin(LonRLS))+
                     (ZGeo - ZRLSGeo) * Math.Sin(LatRLS);
            ZMZSK = (XGeo - XRLSGeo) * (-Math.Sin(LonRLS)) +
                     (YGeo - YRLSGeo) * (Math.Cos(LonRLS));

        }
        // *************************************************************************************

        // *************************************************************************************
        // Пересчет пространственных геоцентрических координат на эллипсоиде WGS84 (XYZ) 
        //  в местную земную систему координат с центром в точке стояния РЛС
        // Z-  в зенит по местной нормали к поверхности эллипсоида (высота)
        // X-север Y(запад) - в горизонтальной плоскости и образуют правую систему координат

        // *************************************************************************************
        public static void MZSK_Geoc(
                                      // RLS
                                      double LatRLS,
                                      double LonRLS,
                                      double XRLSGeo,
                                      double YRLSGeo,
                                      double ZRLSGeo,
                                      double XMZSK,
                                      double YMZSK,
                                      double ZMZSK,
                                      ref double XGeo,
                                      ref double YGeo,
                                      ref double ZGeo
                                     )
        {
/*
            XGeo = -XMZSK * Math.Sin(LatRLS) * Math.Cos(LonRLS) -
                    YMZSK * Math.Sin(LonRLS) +
                    ZMZSK * Math.Cos(LatRLS) * Math.Cos(LonRLS) + 
                    XRLSGeo;
            YGeo = -XMZSK * Math.Sin(LatRLS) * Math.Sin(LonRLS) +
                    YMZSK * Math.Cos(LonRLS) +
                    ZMZSK * Math.Cos(LatRLS) * Math.Sin(LonRLS) + 
                    YRLSGeo;
            XGeo =  XMZSK * Math.Cos(LatRLS) +
                    ZMZSK * Math.Sin(LatRLS) + 
                    ZRLSGeo;
*/
            XGeo = -XMZSK * Math.Sin(LatRLS) * Math.Cos(LonRLS) +
                    YMZSK * Math.Sin(LonRLS) +
                    ZMZSK * Math.Cos(LatRLS) * Math.Cos(LonRLS) +
                    XRLSGeo;
            YGeo = -XMZSK * Math.Sin(LatRLS) * Math.Sin(LonRLS) -
                    YMZSK * Math.Cos(LonRLS) +
                    ZMZSK * Math.Cos(LatRLS) * Math.Sin(LonRLS) +
                    YRLSGeo;
            ZGeo =  XMZSK * Math.Cos(LatRLS) +
                    ZMZSK * Math.Sin(LatRLS) +
                    ZRLSGeo;

        }
        // *************************************************************************************

        // *************************************************************************************
        // Пересчет пространственных геоцентрических координат на эллипсоиде WGS84 (XYZ) 
        //  в местную земную систему координат с центром в точке стояния РЛС
        // *************************************************************************************
        public static void MZSK_Geoc_V(
                                      // RLS
                                      double LatRLS,
                                      double LonRLS,
                                      double XRLSGeo,
                                      double YRLSGeo,
                                      double ZRLSGeo,
                                      double XMZSK,
                                      double YMZSK,
                                      double ZMZSK,
                                      ref double XGeo,
                                      ref double YGeo,
                                      ref double ZGeo
                                     )
        {
            XGeo =  XMZSK * (-Math.Sin(LatRLS) * Math.Cos(LonRLS)) +
                    YMZSK * (Math.Cos(LatRLS)*Math.Cos(LonRLS)) +
                    ZMZSK * (-Math.Sin(LonRLS)) +
                    XRLSGeo;
            YGeo =  XMZSK * (-Math.Sin(LatRLS) * Math.Sin(LonRLS)) +
                    YMZSK * (Math.Cos(LatRLS)*Math.Sin(LonRLS)) +
                    ZMZSK * (Math.Cos(LonRLS)) +
                    YRLSGeo;
            ZGeo =  XMZSK * Math.Cos(LatRLS) +
                    YMZSK * Math.Sin(LatRLS) +
                    ZRLSGeo;

        }
        // *************************************************************************************




        //**************************************************************************************
        // Пересчет геодезических координат на эллипсоиде WGS84 (долгота, широта , высоту не 
        // учитываем) в пространственные геоцентрические координаты на эллипсоиде WGS84 (XYZ)

        // Входные параметры:
        // Lat - широта (град)
        // Long - долгота (град)

        // Выходные параметры:
        // X,Y,Z - Прямоугольные пространственные координаты для
        // выбранного эллипсоида (м)
        //**************************************************************************************
        public static void f_BLH_XYZ_84
            //public (double,double,double) f_BLH_XYZ_84

            (

                // Входные параметры (град)
                double Lat,   // широта
                double Long,  // долгота

                // Прямоугольные пространственные координаты для выбранного эллипсоида
            // м
                ref double X,
                ref double Y,
                ref double Z
            )
        {


            double N = 0;      // Радиус кривизны первого вертикала
            double e2_tmp = 0; // Квадрат эксцентриситета

            double Lat_Coord_tmp = 0;
            double Long_Coord_tmp = 0;
            double H_Coord_tmp = 0;
            double a1W = 0; // Сжатие
            double aW = 0; // Большая полуось


            // Проверка на некорректный ввод ******************************************
            int fval = 0;

            fval = f_validate(ref Lat, ref Long);
            if (fval == -1)
            {
                MessageBox.Show("Incorrect input of latitude and longitude (latitude: -90...90  longitude: -180...180) 1");
                return;
            }
            // ****************************************** Проверка на некорректный ввод

            // Initial conditions ****************************************************
            // Initial conditions

            //var result = (0,0,0);

            // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
            a1W = 1 / 298.257223563; // Сжатие
            aW = 6378137;  // Большая полуось
            // **************************************************** Initial conditions

            // ...................................................................
            H_Coord_tmp = 0;

            // grad->rad
            Lat_Coord_tmp = (Lat * Math.PI) / 180;
            Long_Coord_tmp = (Long * Math.PI) / 180;
            // ...................................................................

            // N *********************************************************************
            // Квадрат эксцентриситета
            e2_tmp = 2 * a1W - Math.Pow(a1W, 2);

            // Радиус кривизны первого вертикала
            // (aW-большая полуось эллипсоида WGS84)

            if (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(Lat_Coord_tmp), 2)) > 1E-15)
                N = aW / Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(Lat_Coord_tmp), 2));
            else
                N = aW / 1E-15;

            // ********************************************************************* N

            // XYZ *******************************************************************
            // m

            X = (N + H_Coord_tmp) * Math.Cos(Lat_Coord_tmp) * Math.Cos(Long_Coord_tmp);
            Y = (N + H_Coord_tmp) * Math.Cos(Lat_Coord_tmp) * Math.Sin(Long_Coord_tmp);
            Z = ((1 - e2_tmp) * N + H_Coord_tmp) * Math.Sin(Lat_Coord_tmp);

            //result.Item1 = (N + H_Coord_tmp) * Math.Cos(Lat_Coord_tmp) * Math.Cos(Long_Coord_tmp);
            //result.Item2 = (N + H_Coord_tmp) * Math.Cos(Lat_Coord_tmp) * Math.Sin(Long_Coord_tmp);
            //result.Item3 = ((1 - e2_tmp) * N + H_Coord_tmp) * Math.Sin(Lat_Coord_tmp);

            // ******************************************************************* XYZ

            //return result;

        } // Функция f_BLH_XYZ_84
        //**************************************************************************************


        //**************************************************************************************
        // Пересчет геодезических координат на эллипсоиде WGS84 (долгота, широта , высоту не 
        // учитываем) в пространственные геоцентрические координаты на эллипсоиде WGS84 (XYZ)

        // Входные параметры:
        // Lat - широта (град)
        // Long - долгота (град)

        // Выходные параметры:
        // X,Y,Z - Прямоугольные пространственные координаты для
        // выбранного эллипсоида (м)
        //**************************************************************************************
        public static void f_BLH_XYZ_84_1
            //public (double,double,double) f_BLH_XYZ_84

            (

                // Входные параметры (град)
                double Lat,   // широта
                double Long,  // долгота
                double H,

                // Прямоугольные пространственные координаты для выбранного эллипсоида
                // м
                ref double X,
                ref double Y,
                ref double Z
            )
        {
            double N = 0;      // Радиус кривизны первого вертикала
            double e2_tmp = 0; // Квадрат эксцентриситета

            double Lat_Coord_tmp = 0;
            double Long_Coord_tmp = 0;
            double H_Coord_tmp = 0;
            double a1W = 0; // Сжатие
            double aW = 0; // Большая полуось

            // Проверка на некорректный ввод ******************************************
            int fval = 0;

            fval = f_validate(ref Lat, ref Long);
            if (fval == -1)
            {
                MessageBox.Show("Incorrect input of latitude and longitude (latitude: -90...90  longitude: -180...180) 2");
                return;
            }
            // ****************************************** Проверка на некорректный ввод

            // Initial conditions ****************************************************
            // Initial conditions

            //var result = (0,0,0);

            // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
            a1W = 1 / 298.257223563; // Сжатие
            aW = 6378137;  // Большая полуось
            // **************************************************** Initial conditions

            // ...................................................................
            //H_Coord_tmp = 0;

            // grad->rad
            Lat_Coord_tmp = (Lat * Math.PI) / 180;
            Long_Coord_tmp = (Long * Math.PI) / 180;
            // ...................................................................

            // N *********************************************************************
            // Квадрат эксцентриситета
            e2_tmp = 2 * a1W - Math.Pow(a1W, 2);

            // Радиус кривизны первого вертикала
            // (aW-большая полуось эллипсоида WGS84)

            if (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(Lat_Coord_tmp), 2)) > 1E-15)
                N = aW / Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(Lat_Coord_tmp), 2));
            else
                N = aW / 1E-15;

            // ********************************************************************* N

            // XYZ *******************************************************************
            // m

            //X = (N + H_Coord_tmp) * Math.Cos(Lat_Coord_tmp) * Math.Cos(Long_Coord_tmp);
            //Y = (N + H_Coord_tmp) * Math.Cos(Lat_Coord_tmp) * Math.Sin(Long_Coord_tmp);
            //Z = ((1 - e2_tmp) * N + H_Coord_tmp) * Math.Sin(Lat_Coord_tmp);

            X = (N + H) * Math.Cos(Lat_Coord_tmp) * Math.Cos(Long_Coord_tmp);
            Y = (N + H) * Math.Cos(Lat_Coord_tmp) * Math.Sin(Long_Coord_tmp);
            Z = ((1 - e2_tmp) * N + H) * Math.Sin(Lat_Coord_tmp);



            //result.Item1 = (N + H_Coord_tmp) * Math.Cos(Lat_Coord_tmp) * Math.Cos(Long_Coord_tmp);
            //result.Item2 = (N + H_Coord_tmp) * Math.Cos(Lat_Coord_tmp) * Math.Sin(Long_Coord_tmp);
            //result.Item3 = ((1 - e2_tmp) * N + H_Coord_tmp) * Math.Sin(Lat_Coord_tmp);

            // ******************************************************************* XYZ

            //return result;

        } // Функция f_BLH_XYZ_84_1
        //**************************************************************************************


        //**************************************************************************************
        // Пересчет геодезических координат на эллипсоиде Красовского (долгота, широта ,
        // высоту не учитываем) в пространственные геоцентрические координаты на эллипсоиде
        // Красовского (XYZ)

        // Входные параметры:
        // Lat - широта (град)
        // Long - долгота (град)

        // Выходные параметры:
        // X,Y,Z - Прямоугольные пространственные координаты для
        // выбранного эллипсоида (м)
        // ***********************************************************************
        public static void f_BLH_XYZ_42
            (

                // Входные параметры (град)
                double Lat,   // широта
                double Long,  // долгота

                // Прямоугольные пространственные координаты для выбранного эллипсоида (м)
                ref double X,
                ref double Y,
                ref double Z

            )
        {

            double N = 0;      // Радиус кривизны первого вертикала
            double e2_tmp = 0; // Квадрат эксцентриситета

            double Lat_Coord_tmp = 0;
            double Long_Coord_tmp = 0;
            double H_Coord_tmp = 0;

            // Для эллипсоида Красовского
            double aP = 0;  // Большая полуось
            double a1P = 0; // Сжатие

            // Проверка на некорректный ввод ******************************************
            int fval = 0;

            fval = f_validate(ref Lat, ref Long);
            if (fval == -1)
            {
                MessageBox.Show("Incorrect input of latitude and longitude (latitude: -90...90  longitude: -180...180) 3");
                return;
            }
            // ****************************************** Проверка на некорректный ввод


            // Initial conditions ****************************************************
            // Initial conditions

            // Эллипсоид Красовского
            a1P = 1 / 298.3;  // Сжатие
            aP = 6378245;     // Большая полуось
            // **************************************************** Initial conditions

            // ...................................................................
            // Перевод в радианы 

            H_Coord_tmp = 0;

            // grad->rad
            Lat_Coord_tmp = (Lat * Math.PI) / 180;
            Long_Coord_tmp = (Long * Math.PI) / 180;
            // ...................................................................

            // N *********************************************************************
            // Квадрат эксцентриситета
            e2_tmp = 2 * a1P - Math.Pow(a1P, 2);

            // Радиус кривизны первого вертикала
            // (aP-большая полуось эллипсоида Красовского)
            if (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(Lat_Coord_tmp), 2)) > 1E-15)
                N = aP / Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(Lat_Coord_tmp), 2));
            else
                N = aP / 1E-15;

            // ********************************************************************* N

            // XYZ *******************************************************************
            // m

            X = (N + H_Coord_tmp) * Math.Cos(Lat_Coord_tmp) * Math.Cos(Long_Coord_tmp);

            Y = (N + H_Coord_tmp) * Math.Cos(Lat_Coord_tmp) * Math.Sin(Long_Coord_tmp);

            Z = ((1 - e2_tmp) * N + H_Coord_tmp) * Math.Sin(Lat_Coord_tmp);

            // ******************************************************************* XYZ


        } // Функция f_BLH_XYZ_42
        //**************************************************************************************

        //**************************************************************************************
        // Преобразование пространственных прямоугольных координат в геодезические
        // координаты для эллипсоида WGS84

        // Преобразование пространственных  геоцентрических координат на эллипсоиде WGS84 (XYZ)
        // в геодезические координаты на эллипсоиде WGS84 (BL)

        // Входные параметры:
        // X,Y,Z - Прямоугольные пространственные координаты для
        // выбранного эллипсоида (м)

        // Выходные параметры:
        // Lat - широта (град)
        // Long - долгота (град)

        //**************************************************************************************
        public static void f_XYZ_BLH_84
            (

                // Входные параметры: прямоугольные пространственные координаты
            // для выбранного эллипсоида (м)
                double X,
                double Y,
                double Z,

                // Выходные параметры (град)
                ref double Lat,   // широта
                ref double Long  // долгота

            )
        {

            double e2_tmp = 0; // Квадрат эксцентриситета

            // Вспомогательные величины
            double D_tmp = 0;
            double La_tmp = 0;
            double r_tmp = 0;
            double c_tmp = 0;
            double p_tmp = 0;
            double b_tmp = 0;
            double S1_tmp = 0;
            double S2_tmp = 0;
            double d_tmp = 0;

            double delt_d_tmp = 0;

            double X_Coord_tmp = 0;
            double Y_Coord_tmp = 0;
            double Z_Coord_tmp = 0;

            double a1W = 0; // Сжатие
            double aW = 0; // Большая полуось
            // Число угловых секунд в радиане
            double ro = 0;

            // Initial conditions ****************************************************
            // Initial conditions

            // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
            a1W = 1 / 298.257223563; // Сжатие
            aW = 6378137;  // Большая полуось
            // Число угловых секунд в радиане
            ro = 206264.8062;

            // **************************************************** Initial conditions

            // .......................................................................
            // m

            X_Coord_tmp = X;
            Y_Coord_tmp = Y;
            Z_Coord_tmp = Z;
            // .......................................................................

            // N *********************************************************************
            // Квадрат эксцентриситета

            e2_tmp = 2 * a1W - Math.Pow(a1W, 2);
            // ********************************************************************* N

            // Long ******************************************************************
            // Вычсление долготы -> анализ D

            D_tmp = Math.Sqrt(Math.Pow(X_Coord_tmp, 2) + Math.Pow(Y_Coord_tmp, 2));

            // .......................................................................
            // D=0

            if (((int)(D_tmp + 0.5)) == 0)
            {
                Long = 0;

                if (Z_Coord_tmp != 0)
                    Lat = (Math.PI / 2) * (Z_Coord_tmp / module(Z_Coord_tmp));
                else
                    Lat = 0;

                //H = Z_Coord_tmp * Math.Sin(Lat) -
                //          aW * Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(Lat), 2));

                return;

            } // D=0
            // .......................................................................
            // D>0

            else
            {

                La_tmp = Math.Asin(Y_Coord_tmp / D_tmp);
                Long = La_tmp;

                if ((Y_Coord_tmp < 0) && (X_Coord_tmp > 0))
                    Long = 2 * Math.PI - La_tmp;
                else if ((Y_Coord_tmp < 0) && (X_Coord_tmp < 0))
                    Long = 2 * Math.PI + La_tmp;
                else if ((Y_Coord_tmp > 0) && (X_Coord_tmp < 0))
                    Long = Math.PI - La_tmp;
                //else if ((Y_Coord > 0) && (X_Coord > 0))
                //    Long_Coord = La_tmp;

            } // D>0
            // .......................................................................

            // ***************************************************************** Long

            // Lat,H ****************************************************************
            // Вычисление широты, высоты -> анализ Z

            // .......................................................................
            // Z=0

            if (((int)(Z_Coord_tmp + 0.5)) == 0)
            {
                Lat = 0;
                //H = D_tmp - aW;

                return;

            } // Z=0
            // .......................................................................
            // Z!=0

            else
            {

                r_tmp = Math.Sqrt(Math.Pow(X_Coord_tmp, 2) + Math.Pow(Y_Coord_tmp, 2) +
                                  Math.Pow(Z_Coord_tmp, 2));

                c_tmp = Math.Asin(Z_Coord_tmp / r_tmp);

                p_tmp = (e2_tmp * aW) / (2 * r_tmp);

                S1_tmp = 0;

                // ????????????????????????????????
                // Количество рад для погрешности delt_d=0.0001угл.с
                delt_d_tmp = 0.0001 / ro;

                b_tmp = c_tmp + S1_tmp;

                if (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2)) > 1E-15)
                    S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2))));
                else
                    S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / 1E-15);

                d_tmp = module(S2_tmp - S1_tmp);

                while (module(d_tmp) >= delt_d_tmp)
                {

                    S1_tmp = S2_tmp;

                    b_tmp = c_tmp + S1_tmp;

                    if (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2)) > 1E-15)
                        S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2))));
                    else
                        S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / 1E-15);

                    d_tmp = module(S2_tmp - S1_tmp);

                }; // WHILE

                Lat = b_tmp;

                //H = D_tmp * Math.Cos(Lat) + Z_Coord_tmp * Math.Sin(Lat) -
                //          aW * Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(Lat), 2));


            } // Z!=0
            // .......................................................................

            // **************************************************************** Lat,H

            // ......................................................................
            // Перевод в градусы 

            Lat = (Lat * 180) / Math.PI;
            Long = (Long * 180) / Math.PI;
            // ......................................................................


        } // Функция f_XYZ_BLH_84
        //**************************************************************************************


        //**************************************************************************************
        public static void f_XYZ_BLH_84_1
            (

                // Входные параметры: прямоугольные пространственные координаты
            // для выбранного эллипсоида (м)
                double X,
                double Y,
                double Z,

                // Выходные параметры (град)
                ref double Lat,   // широта
                ref double Long,  // долгота
                ref double  H

            )
        {

            double e2_tmp = 0; // Квадрат эксцентриситета

            // Вспомогательные величины
            double D_tmp = 0;
            double La_tmp = 0;
            double r_tmp = 0;
            double c_tmp = 0;
            double p_tmp = 0;
            double b_tmp = 0;
            double S1_tmp = 0;
            double S2_tmp = 0;
            double d_tmp = 0;

            double delt_d_tmp = 0;

            double X_Coord_tmp = 0;
            double Y_Coord_tmp = 0;
            double Z_Coord_tmp = 0;

            double a1W = 0; // Сжатие
            double aW = 0; // Большая полуось
            // Число угловых секунд в радиане
            double ro = 0;

            // Initial conditions ****************************************************
            // Initial conditions

            // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
            a1W = 1 / 298.257223563; // Сжатие
            aW = 6378137;  // Большая полуось
            // Число угловых секунд в радиане
            ro = 206264.8062;

            // **************************************************** Initial conditions

            // .......................................................................
            // m

            X_Coord_tmp = X;
            Y_Coord_tmp = Y;
            Z_Coord_tmp = Z;
            // .......................................................................

            // N *********************************************************************
            // Квадрат эксцентриситета

            e2_tmp = 2 * a1W - Math.Pow(a1W, 2);
            // ********************************************************************* N

            // Long ******************************************************************
            // Вычсление долготы -> анализ D

            D_tmp = Math.Sqrt(Math.Pow(X_Coord_tmp, 2) + Math.Pow(Y_Coord_tmp, 2));

            // .......................................................................
            // D=0

            if (((int)(D_tmp + 0.5)) == 0)
            {
                Long = 0;

                if (Z_Coord_tmp != 0)
                    Lat = (Math.PI / 2) * (Z_Coord_tmp / module(Z_Coord_tmp));
                else
                    Lat = 0;

                H = Z_Coord_tmp * Math.Sin(Lat) -
                          aW * Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(Lat), 2));

                return;

            } // D=0
            // .......................................................................
            // D>0

            else
            {

                La_tmp = Math.Asin(Y_Coord_tmp / D_tmp);
                Long = La_tmp;

                if ((Y_Coord_tmp < 0) && (X_Coord_tmp > 0))
                    Long = 2 * Math.PI - La_tmp;
                else if ((Y_Coord_tmp < 0) && (X_Coord_tmp < 0))
                    Long = 2 * Math.PI + La_tmp;
                else if ((Y_Coord_tmp > 0) && (X_Coord_tmp < 0))
                    Long = Math.PI - La_tmp;

                // #@#
                else if ((Y_Coord_tmp > 0) && (X_Coord_tmp > 0))
                    Long = La_tmp;
                else if ((Y_Coord_tmp == 0) && (X_Coord_tmp > 0))
                    Long = 0;
                else if ((Y_Coord_tmp == 0) && (X_Coord_tmp < 0))
                    Long = Math.PI;

                //else if ((Y_Coord > 0) && (X_Coord > 0))
                //    Long_Coord = La_tmp;

            } // D>0
            // .......................................................................

            // ***************************************************************** Long

            // Lat,H ****************************************************************
            // Вычисление широты, высоты -> анализ Z

            // .......................................................................
            // Z=0

            if (((int)(Z_Coord_tmp + 0.5)) == 0)
            {
                Lat = 0;
                H = D_tmp - aW;

                return;

            } // Z=0
            // .......................................................................
            // Z!=0

            else
            {

                r_tmp = Math.Sqrt(Math.Pow(X_Coord_tmp, 2) + Math.Pow(Y_Coord_tmp, 2) +
                                  Math.Pow(Z_Coord_tmp, 2));

                c_tmp = Math.Asin(Z_Coord_tmp / r_tmp);

                p_tmp = (e2_tmp * aW) / (2 * r_tmp);

                S1_tmp = 0;

                // ????????????????????????????????
                // Количество рад для погрешности delt_d=0.0001угл.с
                delt_d_tmp = 0.0001 / ro;

                b_tmp = c_tmp + S1_tmp;

                if (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2)) > 1E-15)
                    S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2))));
                else
                    S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / 1E-15);

                d_tmp = module(S2_tmp - S1_tmp);

                while (module(d_tmp) >= delt_d_tmp)
                {

                    S1_tmp = S2_tmp;

                    b_tmp = c_tmp + S1_tmp;

                    if (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2)) > 1E-15)
                        S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2))));
                    else
                        S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / 1E-15);

                    d_tmp = module(S2_tmp - S1_tmp);

                }; // WHILE

                Lat = b_tmp;

                H = D_tmp * Math.Cos(Lat) + Z_Coord_tmp * Math.Sin(Lat) -
                          aW * Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(Lat), 2));


            } // Z!=0
            // .......................................................................

            // **************************************************************** Lat,H

            // ......................................................................
            // Перевод в градусы 

            Lat = (Lat * 180) / Math.PI;
            Long = (Long * 180) / Math.PI;
            // ......................................................................


        } // Функция f_XYZ_BLH_84
        //**************************************************************************************






        //**************************************************************************************
        // Преобразование пространственных  геоцентрических координат на эллипсоиде
        // Красовского (XYZ) в геодезические координаты на эллипсоиде Красовского (BL)

        // Входные параметры:
        // X,Y,Z - Прямоугольные пространственные координаты для
        // выбранного эллипсоида (м)

        // Выходные параметры:
        // Lat - широта (град)
        // Long - долгота (град)
        //**************************************************************************************
        public static void f_XYZ_BLH_42
            (

                // Входные параметры: прямоугольные пространственные координаты
            // для выбранного эллипсоида (м)
                double X,
                double Y,
                double Z,

                // Выходные параметры (град)
                ref double Lat,   // широта
                ref double Long  // долгота

            )
        {

            double e2_tmp = 0; // Квадрат эксцентриситета

            // Вспомогательные величины
            double D_tmp = 0;
            double La_tmp = 0;
            double r_tmp = 0;
            double c_tmp = 0;
            double p_tmp = 0;
            double b_tmp = 0;
            double S1_tmp = 0;
            double S2_tmp = 0;
            double d_tmp = 0;

            double delt_d_tmp = 0;

            double X_Coord_tmp = 0;
            double Y_Coord_tmp = 0;
            double Z_Coord_tmp = 0;

            // Эллипсоид Красовского
            double a1P;    // Сжатие
            double aP;     // Большая полуось
            // Число угловых секунд в радиане
            double ro;

            // Initial conditions ****************************************************
            // Initial conditions

            // Эллипсоид Красовского
            a1P = 1 / 298.3;  // Сжатие
            aP = 6378245;     // Большая полуось
            // Число угловых секунд в радиане
            ro = 206264.8062;

            // **************************************************** Initial conditions

            // .......................................................................
            // m

            X_Coord_tmp = X;
            Y_Coord_tmp = Y;
            Z_Coord_tmp = Z;
            // .......................................................................

            // N *********************************************************************
            // Квадрат эксцентриситета

            e2_tmp = 2 * a1P - Math.Pow(a1P, 2);
            // ********************************************************************* N

            // Long ******************************************************************
            // Вычсление долготы -> анализ D

            D_tmp = Math.Sqrt(Math.Pow(X_Coord_tmp, 2) + Math.Pow(Y_Coord_tmp, 2));

            // .......................................................................
            // D=0

            if (((int)(D_tmp + 0.5)) == 0)
            {
                Long = 0;

                if (Z_Coord_tmp != 0)
                    Lat = (Math.PI / 2) * (Z_Coord_tmp / module(Z_Coord_tmp));
                else
                    Lat = 0;

                //H = Z_Coord_tmp * Math.Sin(Lat) -
                //          aP * Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(Lat), 2));

                return;

            } // D=0
            // .......................................................................
            // D>0

            else
            {

                La_tmp = Math.Asin(Y_Coord_tmp / D_tmp);
                Long = La_tmp;

                if ((Y_Coord_tmp < 0) && (X_Coord_tmp > 0))
                    Long = 2 * Math.PI - La_tmp;
                else if ((Y_Coord_tmp < 0) && (X_Coord_tmp < 0))
                    Long = 2 * Math.PI + La_tmp;
                else if ((Y_Coord_tmp > 0) && (X_Coord_tmp < 0))
                    Long = Math.PI - La_tmp;
                //else if ((Y_Coord > 0) && (X_Coord > 0))
                //    Long_Coord = La_tmp;

            } // D>0
            // .......................................................................

            // ***************************************************************** Long

            // Lat,H ****************************************************************
            // Вычисление широты, высоты -> анализ Z

            // .......................................................................
            // Z=0

            if (((int)(Z_Coord_tmp + 0.5)) == 0)
            {
                Lat = 0;
                //H = D_tmp - aP;

                return;

            } // Z=0
            // .......................................................................
            // Z!=0

            else
            {

                r_tmp = Math.Sqrt(Math.Pow(X_Coord_tmp, 2) + Math.Pow(Y_Coord_tmp, 2) +
                                  Math.Pow(Z_Coord_tmp, 2));

                c_tmp = Math.Asin(Z_Coord_tmp / r_tmp);

                p_tmp = (e2_tmp * aP) / (2 * r_tmp);

                S1_tmp = 0;

                // ????????????????????????????????
                // Количество рад для погрешности delt_d=0.0001угл.с
                delt_d_tmp = 0.0001 / ro;

                b_tmp = c_tmp + S1_tmp;

                if (module(Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2))) > 1E-15)
                    S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2))));
                else
                    S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / 1E-15);

                d_tmp = module(S2_tmp - S1_tmp);

                while (module(d_tmp) >= delt_d_tmp)
                {

                    S1_tmp = S2_tmp;

                    b_tmp = c_tmp + S1_tmp;

                    if (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2)) > 1E-15)
                        S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / (Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(b_tmp), 2))));
                    else
                        S2_tmp = Math.Asin((p_tmp * Math.Sin(2 * b_tmp)) / 1E-15);

                    d_tmp = module(S2_tmp - S1_tmp);

                }; // WHILE

                Lat = b_tmp;

                //H = D_tmp * Math.Cos(Lat) + Z_Coord_tmp * Math.Sin(Lat) -
                //          aP * Math.Sqrt(1 - e2_tmp * Math.Pow(Math.Sin(Lat), 2));


            } // Z!=0
            // .......................................................................

            // **************************************************************** Lat,H

            // ......................................................................
            // Перевод в градусы

            Lat = (Lat * 180) / Math.PI;
            Long = (Long * 180) / Math.PI;
            // ......................................................................

        } // Функция f_XYZ_BLH_42
        //**************************************************************************************

        //**************************************************************************************
        // Преобразование геодезических координат на эллипсоиде Красовского (BL)
        // в картографическую проекцию Гаусса-Крюгера (XY)

        // Входные параметры:
        // Lat - широта (град)
        // Long - долгота (град)

        // Выходные параметры:
        // X, Y - плоские прямоугольные координаты (м)
        //**************************************************************************************
        public static void f_SK42_Krug
            (
            // Входные параметры (!!! град)
                double Lat,   // широта
                double Long,  // долгота

                // Выходные параметры (км)
                ref double X,
                ref double Y

            )
        {

            int No_tmp = 0;
            double Lo_tmp = 0;
            double Bo_tmp = 0;

            // Вспомогательные величины
            double Xa_tmp = 0;
            double Xb_tmp = 0;
            double Xc_tmp = 0;
            double Xd_tmp = 0;
            double Ya_tmp = 0;
            double Yb_tmp = 0;
            double Yc_tmp = 0;
            double sin2_B = 0;
            double sin4_B = 0;
            double sin6_B = 0;

            // Проверка на некорректный ввод ******************************************
            int fval = 0;

            fval = f_validate(ref Lat, ref Long);
            if (fval == -1)
            {
                MessageBox.Show("Incorrect input of latitude and longitude (latitude: -90...90  longitude: -180...180) 4");
                return;
            }
            // ****************************************** Проверка на некорректный ввод


            // No ********************************************************************
            // Номер шестиградусной зоны в проекци Гаусса-Крюгера (1,2,...)
            // !!! Long - в град

            No_tmp = (int)(6 + Long) / 6;

            // ******************************************************************** No

            // Lo ********************************************************************
            // Расстояние от определяемой точки до осевого меридиана зоны, рад

            Lo_tmp = (Long - (3 + 6 * (No_tmp - 1))) / 57.29577951;

            // ******************************************************************** Lo

            // Bo ********************************************************************
            // Переводим широту в рад

            Bo_tmp = (Lat * Math.PI) / 180;


            sin2_B = Math.Pow(Math.Sin(Bo_tmp), 2);
            sin4_B = Math.Pow(Math.Sin(Bo_tmp), 4);
            sin6_B = Math.Pow(Math.Sin(Bo_tmp), 6);

            // ******************************************************************** Bo

            // X *********************************************************************

            Xa_tmp = Math.Pow(Lo_tmp, 2) *
                     (109500 - 574700 * sin2_B + 863700 * sin4_B -
                      398600 * sin6_B);

            Xb_tmp = Math.Pow(Lo_tmp, 2) *
                     (278194 - 830174 * sin2_B + 572434 * sin4_B -
                      16010 * sin6_B + Xa_tmp);

            Xc_tmp = Math.Pow(Lo_tmp, 2) *
                    (672483.4 - 811219.9 * sin2_B + 5420 * sin4_B -
                     10.6 * sin6_B + Xb_tmp);

            Xd_tmp = Math.Pow(Lo_tmp, 2) *
                    (1594561.25 + 5336.535 * sin2_B + 26.79 * sin4_B +
                    0.149 * sin6_B + Xc_tmp);

            X = 6367558.4968 * Bo_tmp -
                         Math.Sin(Bo_tmp * 2) *
                         (16002.89 + 66.9607 * sin2_B + 0.3515 * sin4_B - Xd_tmp);

            // ********************************************************************* X

            // Y *********************************************************************

            Ya_tmp = Math.Pow(Lo_tmp, 2) *
                    (79690 - 866190 * sin2_B + 1730360 * sin4_B -
                     945460 * sin6_B);

            Yb_tmp = Math.Pow(Lo_tmp, 2) *
                    (270806 - 1523417 * sin2_B + 1327645 * sin4_B -
                     21701 * sin6_B + Ya_tmp);

            Yc_tmp = Math.Pow(Lo_tmp, 2) *
                     (1070204.16 - 2136826.66 * sin2_B + 17.98 * sin4_B -
                      11.99 * sin6_B + Yb_tmp);

            Y = (5 + 10 * No_tmp) * 100000 +
                         Lo_tmp * Math.Cos(Bo_tmp) *
                         (6378245 + 21346.1415 * sin2_B + 107.159 * sin4_B +
                          0.5977 * sin6_B + Yc_tmp);

            // ********************************************************************* Y


        } // Функция f_SK42_Krug
        //**************************************************************************************

        //**************************************************************************************
        // Преобразование картографической проекции Гаусса-Крюгера (XY) в
        // геодезические координаты на эллипсоиде Красовского (BL)

        // Входные параметры:
        // X, Y - плоские прямоугольные координаты (м)

        // Выходные параметры:
        // Lat - широта (град)
        // Long - долгота (град)

        //**************************************************************************************
        public static void f_Krug_SK42
            (
            // Входные параметры (м)
                double X,
                double Y,

                // Выходные параметры (град)
                ref double Lat,   // широта
                ref double Long   // долгота

            )
        {

            int No_tmp = 0;
            double Bo_tmp = 0;

            // Вспомогательные величины
            double Bi_tmp = 0;
            double Zo_tmp = 0;
            double Ba_tmp = 0;
            double Bb_tmp = 0;
            double Bc_tmp = 0;
            double dB_tmp = 0;
            double La_tmp = 0;
            double Lb_tmp = 0;
            double Lc_tmp = 0;
            double Ld_tmp = 0;
            double dL_tmp = 0;
            double sin2_B = 0;
            double sin4_B = 0;
            double sin6_B = 0;

            double X_Coord_Kr_tmp = 0;
            double Y_Coord_Kr_tmp = 0;

            // .......................................................................
            // м

            X_Coord_Kr_tmp = X;
            Y_Coord_Kr_tmp = Y;
            // .......................................................................


            // No ********************************************************************
            // Номер шестиградусной зоны в проекци Гаусса-Крюгера (1,2,...)

            No_tmp = (int)(Y_Coord_Kr_tmp * Math.Pow(10, -6));

            // ******************************************************************** No

            // Bi,Bo *****************************************************************
            // Bo->Геодезическая широта точки, абсцисса которой равнв абсциссе заданной
            // точки, а ордината равна нулю (рад)

            Bi_tmp = X_Coord_Kr_tmp / 6367558.4968;

            Bo_tmp = Bi_tmp + Math.Sin(Bi_tmp * 2) *
                     (0.00252588685 - 0.0000149186 * Math.Pow(Math.Sin(Bi_tmp), 2) +
                      0.00000011904 * Math.Pow(Math.Sin(Bi_tmp), 4));

            sin2_B = Math.Pow(Math.Sin(Bo_tmp), 2);
            sin4_B = Math.Pow(Math.Sin(Bo_tmp), 4);
            sin6_B = Math.Pow(Math.Sin(Bo_tmp), 6);

            // ***************************************************************** Bi,Bo

            // Bi,Zo *****************************************************************
            // Вспомогательная величина

            if (module(6378245 * Math.Cos(Bo_tmp)) > 1E-15)
                Zo_tmp = (Y_Coord_Kr_tmp - (10 * No_tmp + 5) * 100000) / (6378245 * Math.Cos(Bo_tmp));
            else
                Zo_tmp = (Y_Coord_Kr_tmp - (10 * No_tmp + 5) * 100000) / 1E-15;

            // ***************************************************************** Bi,Zo

            // Широта ****************************************************************

            Ba_tmp = Zo_tmp * Zo_tmp *
                     (0.01672 - 0.0063 * sin2_B + 0.01188 * sin4_B - 0.00328 * sin6_B);

            Bb_tmp = Zo_tmp * Zo_tmp *
                     (0.042858 - 0.025318 * sin2_B + 0.014346 * sin4_B - 0.001264 * sin6_B - Ba_tmp);

            Bc_tmp = Zo_tmp * Zo_tmp *
                    (0.10500614 - 0.04559916 * sin2_B + 0.00228901 * sin4_B -
                     0.00002987 * sin6_B - Bb_tmp);

            dB_tmp = -Zo_tmp * Zo_tmp * Math.Sin(Bo_tmp * 2) *
                     (0.251684631 - 0.003369263 * sin2_B + 0.000011276 * sin4_B - Bc_tmp);


            Lat = Bo_tmp + dB_tmp;  // rad
            // **************************************************************** Широта

            // Долгота ***************************************************************

            La_tmp = Zo_tmp * Zo_tmp *
                     (0.0038 + 0.0524 * sin2_B + 0.0482 * sin4_B + 0.0032 * sin6_B);

            Lb_tmp = Zo_tmp * Zo_tmp *
                    (0.01225 + 0.09477 * sin2_B + 0.03282 * sin4_B - 0.00034 * sin6_B - La_tmp);

            Lc_tmp = Zo_tmp * Zo_tmp *
                    (0.0420025 + 0.1487407 * sin2_B + 0.005942 * sin4_B - 0.000015 * sin6_B - Lb_tmp);

            Ld_tmp = Zo_tmp * Zo_tmp *
                     (0.16778975 + 0.16273586 * sin2_B - 0.0005249 * sin4_B -
                      0.00000846 * sin6_B - Lc_tmp);

            dL_tmp = Zo_tmp *
                     (1 - 0.0033467108 * sin2_B - 0.0000056002 * sin4_B -
                      0.0000000187 * sin6_B - Ld_tmp);

            Long = 6 * (No_tmp - 0.5) / 57.29577951 + dL_tmp; // rad
            // *************************************************************** Долгота

            // .......................................................................
            // Перевод в градусы

            Lat = (Lat * 180) / Math.PI;
            Long = (Long * 180) / Math.PI;
            // .......................................................................


        } // Функция f_Krug_SK42
        //**************************************************************************************

        //**************************************************************************************
        // Расчет приращения по долготе при преобразованиях координат WGS84 (эллипсоид)
        // <-> эллипсоид Красовского(Пулково-42)   (преобразования Молоденского) в
        //  угл.сек

        // Входные параметры:
        // Lat - широта (град)
        // Long - долгота (град)
        // dX,dY,dZ - DATUM

        // Выходные параметры:
        // dLong - приращение по долготе, !!! угловые секунды
        //**************************************************************************************
        public static void f_dLong
            (

                // Входные параметры (внутри функции переводятся в рад )
                double Lat,   // широта   (grad)
                double Long,  // долгота  (grad)
            //double H,     // высота (m)

                // DATUM
                double dX,
                double dY,
                double dZ,

                // Выходные параметры 
                ref double dLong   // приращение по долготе, угл.сек

            )
        {

            double N = 0;
            double Lat_Coord_8442_tmp = 0;
            double Long_Coord_8442_tmp = 0;
            double H_Coord_8442_tmp = 0;

            // Эллипсоид Красовского
            double a1P = 0; // Сжатие
            double e2P = 0; // Квадрат эксцентриситета
            double aP = 0;  // Большая полуось

            // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
            double a1W = 0; // Сжатие
            double e2W = 0;  // Квадрат эксцентриситета
            double aW = 0;  // Большая полуось

            double a = 0;
            double e2 = 0;
            double da = 0;
            double de2 = 0;

            // Число угловых секунд в радиане
            double ro = 0;

            // !!! Угловые элементы трансформирования в DATUM считаем =0
            double wx = 0;
            double wy = -0.35;
            double wz = -0.66;

            // Проверка на некорректный ввод ******************************************
            int fval = 0;

            fval = f_validate(ref Lat, ref Long);
            if (fval == -1)
            {
                MessageBox.Show("Incorrect input of latitude and longitude (latitude: -90...90  longitude: -180...180) 5");
                return;
            }
            // ****************************************** Проверка на некорректный ввод

            // Initial conditions ****************************************************
            // Initial conditions

            // Эллипсоид Красовского
            aP = 6378245;  // Большая полуось
            a1P = 1 / 298.3;         // Сжатие
            e2P = 2 * a1P - a1P * a1P; // Квадрат эксцентриситета

            // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
            aW = 6378137;  // Большая полуось
            a1W = 1 / 298.257223563; // Сжатие
            e2W = 2 * a1W - a1W * a1W;  // Квадрат эксцентриситета

            // Вспомогательные значения для преобразования эллипсоидов
            a = (aP + aW) / 2;
            e2 = (e2P + e2W) / 2;
            da = aW - aP;
            de2 = e2W - e2P;

            H_Coord_8442_tmp = 0;

            // Число угловых секунд в радиане
            ro = 206264.8062;

            // **************************************************** Initial conditions

            // ...................................................................
            // Перевод в радианы 

            // grad->rad
            Lat_Coord_8442_tmp = (Lat * Math.PI) / 180;
            Long_Coord_8442_tmp = (Long * Math.PI) / 180;
            // ...................................................................

            // Радиус кривизны первого вертикала
            N = a / Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp));

            dLong = ro / ((N + H_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp)) *
                         (-dX * Math.Sin(Long_Coord_8442_tmp) + dY * Math.Cos(Long_Coord_8442_tmp)) +
                        Math.Tan(Lat_Coord_8442_tmp) * (1 - e2) * (wx * Math.Cos(Long_Coord_8442_tmp) + wy * Math.Sin(Long_Coord_8442_tmp)) - wz;


        } // Функция f_dLong
        //**************************************************************************************

        //**************************************************************************************
        // Расчет приращения по широте при преобразованиях координат WGS84 (эллипсоид)
        // <-> эллипсоид Красовского(Пулково-42)   (преобразования Молоденского) в
        //  угл.сек

        // Выходные параметры:
        // dLat_Coord - приращение по широте, !!! угловые секунды


        // Входные параметры:
        // Lat - широта (град)
        // Long - долгота (град)
        // dX,dY,dZ - DATUM

        // Выходные параметры:
        // dLat - приращение по широте, !!! угловые секунды
        //**************************************************************************************
        public static void f_dLat
            (
            // Входные параметры (внутри функции переводятся в рад )
                double Lat,   // широта (grad)
                double Long,  // долгота (grad)

                // DATUM
                double dX,
                double dY,
                double dZ,

                // Выходные параметры 
                ref double dLat    // приращение по широте, угл.сек

            )
        {

            double N = 0;
            double M = 0;
            double Lat_Coord_8442_tmp = 0;
            double Long_Coord_8442_tmp = 0;
            double H_Coord_8442_tmp = 0;

            // Эллипсоид Красовского
            double a1P = 0; // Сжатие
            double e2P = 0; // Квадрат эксцентриситета
            double aP = 0;  // Большая полуось

            // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
            double a1W = 0; // Сжатие
            double e2W = 0;  // Квадрат эксцентриситета
            double aW = 0;  // Большая полуось

            double a = 0;
            double e2 = 0;
            double da = 0;
            double de2 = 0;

            // Число угловых секунд в радиане
            double ro = 0;

            // !!! Угловые элементы трансформирования в DATUM считаем =0
            double wx = 0;
            double wy = -0.35;
            double wz = -0.66;
            double ms = 0;

            // Проверка на некорректный ввод ******************************************
            int fval = 0;

            fval = f_validate(ref Lat, ref Long);
            if (fval == -1)
            {
                MessageBox.Show("Incorrect input of latitude and longitude (latitude: -90...90  longitude: -180...180) 6");
                return;
            }
            // ****************************************** Проверка на некорректный ввод

            // Initial conditions ****************************************************
            // Initial conditions


            // Эллипсоид Красовского
            aP = 6378245;  // Большая полуось
            a1P = 1 / 298.3;                 // Сжатие
            e2P = 2 * a1P - Math.Pow(a1P, 2); // Квадрат эксцентриситета


            // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
            aW = 6378137;  // Большая полуось
            a1W = 1 / 298.257223563; // Сжатие
            e2W = 2 * a1W - a1W * a1W;  // Квадрат эксцентриситета

            // Вспомогательные значения для преобразования эллипсоидов
            a = (aP + aW) / 2;
            e2 = (e2P + e2W) / 2;
            da = aW - aP;
            de2 = e2W - e2P;

            H_Coord_8442_tmp = 0;

            // Число угловых секунд в радиане
            ro = 206264.8062;

            // **************************************************** Initial conditions

            // ...................................................................
            // Перевод в радианы 

            // grad->rad
            Lat_Coord_8442_tmp = (Lat * Math.PI) / 180;
            Long_Coord_8442_tmp = (Long * Math.PI) / 180;
            // ...................................................................

            // Радиус кривизны первого вертикала
            N = a / Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp));

            // Радиус кривизны меридиана
            M = a * (1 - e2) *
                (1 / (Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp)) *
                    Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp)) *
                    Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp))));

            dLat = (ro / (M + H_Coord_8442_tmp)) *
                          ((N / a) * e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp) * da +
                          ((N * N) / (a * a) + 1) * N * Math.Sin(Lat_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp) * (de2 / 2) -
                          dX * Math.Cos(Long_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp) -
                          dY * Math.Sin(Long_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp) +
                          dZ * Math.Cos(Lat_Coord_8442_tmp)) -
                          wx * Math.Sin(Long_Coord_8442_tmp) * (1 + e2 * Math.Cos(2 * Lat_Coord_8442_tmp)) +
                          wy * Math.Cos(Long_Coord_8442_tmp) * (1 + e2 * Math.Cos(2 * Lat_Coord_8442_tmp)) -
                          ro * ms * e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp);


        } // Функция f_dLat
        //**************************************************************************************

        //**************************************************************************************
        // 1-й вариант

        // Преобразование геодезических координат на эллипсоиде WGS84 (BL) в
        // геодезические координаты на  эллипсоиде Красовского (BL)

        // Входные параметры:
        // Lat_WGS84 - широта (град)
        // Long_WGS84 - долгота (град)
        // dX,dY,dZ - DATUM (m)

        // Выходные параметры:
        // Lat_SK42 - широта (град)
        // Long_SK42 - долгота (град)

        //**************************************************************************************
        public static void f_WGS84_SK42_BL
               (
            // Входные параметры (grad)
                   double Lat_WGS84,   // широта
                   double Long_WGS84,  // долгота

                   // DATUM
                   double dX,
                   double dY,
                   double dZ,

                   // Выходные параметры (grad)
                   ref double Lat_SK42,   // широта
                   ref double Long_SK42   // долгота

               )
        {

            double dLat_Coord = 0;
            double dLong_Coord = 0;

            // Проверка на некорректный ввод ******************************************
            int fval = 0;

            fval = f_validate(ref Lat_WGS84, ref Long_WGS84);
            if (fval == -1)
            {
                MessageBox.Show("Incorrect input of latitude and longitude (latitude: -90...90  longitude: -180...180) 7");
                return;
            }
            // ****************************************** Проверка на некорректный ввод

            // .........................................................................
            // Приращение по долготе

            f_dLong
                (

                    // Входные параметры (внутри функции переводятся в рад )
                    Lat_WGS84,   // широта   (grad)
                    Long_WGS84,  // долгота  (grad)

                    // DATUM
                    dX,
                    dY,
                    dZ,

                    // Выходные параметры 
                    ref dLong_Coord   // приращение по долготе, угл.сек

                );
            // .........................................................................
            // Приращение по широте

            f_dLat
                (
                // Входные параметры (внутри функции переводятся в рад )
                    Lat_WGS84,   // широта   (grad)
                    Long_WGS84,  // долгота  (grad)

                    // DATUM
                    dX,
                    dY,
                    dZ,

                    // Выходные параметры 
                    ref dLat_Coord   // приращение по долготе, угл.сек

                );
            // .........................................................................

            // grad
            Lat_SK42 = Lat_WGS84 - dLat_Coord / 3600;
            Long_SK42 = Long_WGS84 - dLong_Coord / 3600;
            // .........................................................................


        } // Функция f_WGS84_SK42_BL
        //**************************************************************************************

        //**************************************************************************************
        // 2-й вариант

        // Преобразование геодезических координат на эллипсоиде WGS84 (BL) в
        // геодезические координаты на  эллипсоиде Красовского (BL)

        // Входные параметры:
        // Lat_WGS84 - широта (град)
        // Long_WGS84 - долгота (град)

        // Выходные параметры:
        // Lat_SK42 - широта (град)
        // Long_SK42 - долгота (град)

        // !!! Заложен DATUM ГОСТ 51794_2008
        //      dx=25m
        //      dy=-141m
        //      dz=-80m

        //**************************************************************************************
        public static void f_WGS84_SK42_BL
               (
            // Входные параметры (grad)
                   double Lat_WGS84,   // широта
                   double Long_WGS84,  // долгота

                   // Выходные параметры (grad)
                   ref double Lat_SK42,   // широта
                   ref double Long_SK42   // долгота

               )
        {

            double dLat_Coord = 0;
            double dLong_Coord = 0;

            // DATUM (m)
            double dX = 25;
            double dY = -141;
            double dZ = -80;

            // Проверка на некорректный ввод ******************************************
            int fval = 0;

            fval = f_validate(ref Lat_WGS84, ref Long_WGS84);
            if (fval == -1)
            {
                MessageBox.Show("Incorrect input of latitude and longitude (latitude: -90...90  longitude: -180...180 8)");
                return;
            }
            // ****************************************** Проверка на некорректный ввод

            // .........................................................................
            // Приращение по долготе

            f_dLong
                (

                    // Входные параметры (внутри функции переводятся в рад )
                    Lat_WGS84,   // широта   (grad)
                    Long_WGS84,  // долгота  (grad)

                    // DATUM
                    dX,
                    dY,
                    dZ,

                    // Выходные параметры 
                    ref dLong_Coord   // приращение по долготе, угл.сек

                );
            // .........................................................................
            // Приращение по широте

            f_dLat
                (
                // Входные параметры (внутри функции переводятся в рад )
                    Lat_WGS84,   // широта   (grad)
                    Long_WGS84,  // долгота  (grad)

                    // DATUM
                    dX,
                    dY,
                    dZ,

                    // Выходные параметры 
                    ref dLat_Coord   // приращение по долготе, угл.сек

                );
            // .........................................................................

            // grad
            Lat_SK42 = Lat_WGS84 - dLat_Coord / 3600;
            Long_SK42 = Long_WGS84 - dLong_Coord / 3600;
            // .........................................................................


        } // Функция f_WGS84_SK42_BL
        //**************************************************************************************

        //**************************************************************************************
        // 1-й вариант

        // Преобразование геодезических координат на эллипсоиде Красовского (BL)
        // в геодезические координаты на  эллипсоиде WGS84 (BL)

        // Входные параметры:
        // Lat_SK42 - широта (град)
        // Long_SK42 - долгота (град)
        // dX,dY,dZ - DATUM (m)

        // Выходные параметры:
        // Lat_WGS84 - широта (град)
        // Long_WGS84 - долгота (град)
        //**************************************************************************************
        public static void f_SK42_WGS84_BL
               (

                   // Входные параметры (grad)
                   double Lat_SK42,   // широта
                   double Long_SK42,  // долгота

                   // DATUM
                   double dX,
                   double dY,
                   double dZ,

                   // Выходные параметры (grad)
                   ref double Lat_WGS84,   // широта
                   ref double Long_WGS84   // долгота

               )
        {
            double dLat_Coord = 0;
            double dLong_Coord = 0;

            // Проверка на некорректный ввод ******************************************
            int fval = 0;

            fval = f_validate(ref Lat_SK42, ref Long_SK42);
            if (fval == -1)
            {
                MessageBox.Show("Incorrect input of latitude and longitude (latitude: -90...90  longitude: -180...180) 9");
                return;
            }
            // ****************************************** Проверка на некорректный ввод

            // .........................................................................
            // Приращение по долготе

            f_dLong
                (

                    // Входные параметры (внутри функции переводятся в рад )
                    Lat_SK42,   // широта   (grad)
                    Long_SK42,  // долгота  (grad)

                    // DATUM
                    dX,
                    dY,
                    dZ,

                    // Выходные параметры 
                    ref dLong_Coord   // приращение по долготе, угл.сек

                );
            // .........................................................................
            // Приращение по широте

            f_dLat
                (
                // Входные параметры (внутри функции переводятся в рад )
                    Lat_SK42,   // широта   (grad)
                    Long_SK42,  // долгота  (grad)

                    // DATUM
                    dX,
                    dY,
                    dZ,

                    // Выходные параметры 
                    ref dLat_Coord   // приращение по долготе, угл.сек

                );
            // .........................................................................
            // grad

            Lat_WGS84 = Lat_SK42 + dLat_Coord / 3600;
            Long_WGS84 = Long_SK42 + dLong_Coord / 3600;


        } // Функция f_SK42_WGS84_BL
        //**************************************************************************************

        //**************************************************************************************
        // 2-й вариант

        // Преобразование геодезических координат на эллипсоиде Красовского (BL)
        // в геодезические координаты на  эллипсоиде WGS84 (BL)

        // Входные параметры:
        // Lat_SK42 - широта (град)
        // Long_SK42 - долгота (град)

        // Выходные параметры:
        // Lat_WGS84 - широта (град)
        // Long_WGS84 - долгота (град)

        // !!! Заложен DATUM ГОСТ 51794_2008
        //      dx=25m
        //      dy=-141m
        //      dz=-80m

        //**************************************************************************************
        public static void f_SK42_WGS84_BL
               (

                   // Входные параметры (grad)
                   double Lat_SK42,   // широта
                   double Long_SK42,  // долгота

                   // Выходные параметры (grad)
                   ref double Lat_WGS84,   // широта
                   ref double Long_WGS84   // долгота

               )
        {
            double dLat_Coord = 0;
            double dLong_Coord = 0;

            // DATUM ГОСТ 51794 (м)
            double dX = 25;
            double dY = -141;
            double dZ = -80;

            // Проверка на некорректный ввод ******************************************
            int fval = 0;

            fval = f_validate(ref Lat_SK42, ref Long_SK42);
            if (fval == -1)
            {
                MessageBox.Show("Incorrect input of latitude and longitude (latitude: -90...90  longitude: -180...180) 10");
                return;
            }
            // ****************************************** Проверка на некорректный ввод

            // .........................................................................
            // Приращение по долготе

            f_dLong
                (

                    // Входные параметры (внутри функции переводятся в рад )
                    Lat_SK42,   // широта   (grad)
                    Long_SK42,  // долгота  (grad)

                    // DATUM
                    dX,
                    dY,
                    dZ,

                    // Выходные параметры 
                    ref dLong_Coord   // приращение по долготе, угл.сек

                );
            // .........................................................................
            // Приращение по широте

            f_dLat
                (
                // Входные параметры (внутри функции переводятся в рад )
                    Lat_SK42,   // широта   (grad)
                    Long_SK42,  // долгота  (grad)

                    // DATUM
                    dX,
                    dY,
                    dZ,

                    // Выходные параметры 
                    ref dLat_Coord   // приращение по долготе, угл.сек

                );
            // .........................................................................
            // grad

            Lat_WGS84 = Lat_SK42 + dLat_Coord / 3600;
            Long_WGS84 = Long_SK42 + dLong_Coord / 3600;


        } // Функция f_SK42_WGS84_BL
        //**************************************************************************************

        //**************************************************************************************
        // 1-й вариант

        // Преобразование геодезических координат на эллипсоиде WGS84 (BL) в картографическую 
        // проекцию Гаусса-Крюгера (XY)

        // Входные параметры:
        // Lat - широта (град)
        // Long - долгота (град)
        // dX,dY,dZ - DATUM (m)

        // Выходные параметры (m):
        // X
        // Y

        //**************************************************************************************
        public static void f_WGS84_Krug
               (
            // Входные параметры (grad)
                   double Lat,   // широта
                   double Long,  // долгота

                   // DATUM
                   double dX,
                   double dY,
                   double dZ,

                   // Выходные параметры (m)
                   ref double X,
                   ref double Y

               )
        {

            double Lat_SK42 = 0;
            double Long_SK42 = 0;

            // Проверка на некорректный ввод ******************************************
            int fval = 0;

            fval = f_validate(ref Lat, ref Long);
            if (fval == -1)
            {
                MessageBox.Show("Incorrect input of latitude and longitude (latitude: -90...90  longitude: -180...180 11)");
                return;
            }
            // ****************************************** Проверка на некорректный ввод

            // .........................................................................
            // Эллипсоид WGS84 -> эллипсоид Красовского

            f_WGS84_SK42_BL
               (
                // Входные параметры (grad)
                   Lat,   // широта
                   Long,  // долгота

                   // DATUM
                   dX,
                   dY,
                   dZ,

                   // Выходные параметры (grad)
                   ref Lat_SK42,   // широта
                   ref Long_SK42   // долгота

               );
            // .........................................................................
            // Преобразование геодезических координат на эллипсоиде Красовского (BL)
            // в картографическую проекцию Гаусса-Крюгера (XY)

            f_SK42_Krug
              (
                // Входные параметры (!!! град)
                Lat_SK42,   // широта
                Long_SK42,  // долгота

                // Выходные параметры (км)
                ref X,
                ref Y

              );
            // .........................................................................


        } // Функция f_WGS84_Krug
        //**************************************************************************************

        //**************************************************************************************
        // 2-й вариант

        // Преобразование геодезических координат на эллипсоиде WGS84 (BL) в картографическую 
        // проекцию Гаусса-Крюгера (XY)

        // Входные параметры:
        // Lat - широта (град)
        // Long - долгота (град)

        // Выходные параметры (m):
        // X
        // Y

        // !!! Заложен DATUM ГОСТ 51794_2008
        //      dx=25m
        //      dy=-141m
        //      dz=-80m
        //**************************************************************************************
        public static void f_WGS84_Krug
               (
            // Входные параметры (grad)
                   double Lat,   // широта
                   double Long,  // долгота

                   // Выходные параметры (m)
                   ref double X,
                   ref double Y

               )
        {

            double Lat_SK42 = 0;
            double Long_SK42 = 0;

            // DATUM
            double dX = 25;
            double dY = -141;
            double dZ = -80;

            // Проверка на некорректный ввод ******************************************
            int fval = 0;

            fval = f_validate(ref Lat, ref Long);
            if (fval == -1)
            {
                MessageBox.Show("Incorrect input of latitude and longitude (latitude: -90...90  longitude: -180...180) 12");
                return;
            }
            // ****************************************** Проверка на некорректный ввод

            // .........................................................................
            // Эллипсоид WGS84 -> эллипсоид Красовского

            f_WGS84_SK42_BL
               (
                // Входные параметры (grad)
                   Lat,   // широта
                   Long,  // долгота

                   // DATUM
                   dX,
                   dY,
                   dZ,

                   // Выходные параметры (grad)
                   ref Lat_SK42,   // широта
                   ref Long_SK42   // долгота

               );
            // .........................................................................
            // Преобразование геодезических координат на эллипсоиде Красовского (BL)
            // в картографическую проекцию Гаусса-Крюгера (XY)

            f_SK42_Krug
              (
                // Входные параметры (!!! град)
                Lat_SK42,   // широта
                Long_SK42,  // долгота

                // Выходные параметры (км)
                ref X,
                ref Y

              );
            // .........................................................................


        } // Функция f_WGS84_Krug
        //**************************************************************************************

        //**************************************************************************************
        // 1-й вариант

        // Преобразование картографической проекции Гаусса-Крюгера (XY) в геодезические 
        // координаты на эллипсоиде WGS84 

        // Входные параметры (m):
        // X
        // Y
        // dX,dY,dZ - DATUM (m)

        // Выходные параметры (m):
        // Lat - широта (град)
        // Long - долгота (град)

        //**************************************************************************************
        public static void f_Krug_WGS84
               (
            // Входные параметры (m)
                   double X,
                   double Y,

                   // DATUM
                   double dX,
                   double dY,
                   double dZ,

                   // Выходные параметры 
                   ref double Lat,   // широта
                   ref double Long   // долгота

               )
        {

            double Lat_SK42 = 0;
            double Long_SK42 = 0;
            // .........................................................................
            // Преобразование картографической проекции Гаусса-Крюгера (XY) в
            // геодезические координаты на эллипсоиде Красовского (BL)

            f_Krug_SK42
              (
                // Входные параметры (м)
                X,
                Y,

                // Выходные параметры (град)
                ref Lat_SK42,   // широта
                ref Long_SK42   // долгота

              );
            // .........................................................................
            // эллипсоид Красовского -> эллипсоид WGS84

            f_SK42_WGS84_BL
                (
                // Входные параметры (grad)
                    Lat_SK42,   // широта
                    Long_SK42,  // долгота

                    // DATUM
                    dX,
                    dY,
                    dZ,

                    // Выходные параметры (grad)
                    ref Lat,   // широта
                    ref Long   // долгота

                );
            // .........................................................................



        } // Функция f_Krug_WGS84
        //**************************************************************************************

        //**************************************************************************************
        // 2-й вариант

        // Преобразование картографической проекции Гаусса-Крюгера (XY) в геодезические 
        // координаты на эллипсоиде WGS84 

        // Входные параметры (m):
        // X
        // Y

        // Выходные параметры (m):
        // Lat - широта (град)
        // Long - долгота (град)

        // !!! Заложен DATUM ГОСТ 51794_2008
        //      dx=25m
        //      dy=-141m
        //      dz=-80m

        //**************************************************************************************
        public static void f_Krug_WGS84
               (
            // Входные параметры (m)
                   double X,
                   double Y,

                   // Выходные параметры 
                   ref double Lat,   // широта
                   ref double Long   // долгота

               )
        {

            double Lat_SK42 = 0;
            double Long_SK42 = 0;

            // DATUM ГОСТ 51794_2008
            double dX = 25;
            double dY = -141;
            double dZ = -80;

            // .........................................................................
            // Преобразование картографической проекции Гаусса-Крюгера (XY) в
            // геодезические координаты на эллипсоиде Красовского (BL)

            f_Krug_SK42
              (
                // Входные параметры (м)
                X,
                Y,

                // Выходные параметры (град)
                ref Lat_SK42,   // широта
                ref Long_SK42   // долгота

              );
            // .........................................................................
            // эллипсоид Красовского -> эллипсоид WGS84

            f_SK42_WGS84_BL
                (
                // Входные параметры (grad)
                    Lat_SK42,   // широта
                    Long_SK42,  // долгота

                    // DATUM
                    dX,
                    dY,
                    dZ,

                    // Выходные параметры (grad)
                    ref Lat,   // широта
                    ref Long   // долгота

                );
            // .........................................................................



        } // Функция f_Krug_WGS84
        //**************************************************************************************

        //**************************************************************************************
        // Преобразование dd.ddddd (grad) -> DD MM SS 
        // (дробное значение градусов -> в градусы(целое значение), минуты(целое значение),
        // секунды(дробное значение))

        // Входные параметры:
        // Grad_Dbl_Coord - градусы в дробном виде

        // Выходные параметры:
        // Grad_I_Coord - градусы (целое)
        // Min_Coord - минуты (целое)
        // Sec_Coord - секунды (дробное)
        //**************************************************************************************
        public static void f_Grad_GMS
            (

                // Входные параметры (grad)
                double Grad_Dbl_Coord,

                // Выходные параметры 
                ref int Grad_I_Coord,
                ref int Min_Coord,
                ref double Sec_Coord

            )
        {

            Grad_I_Coord = (int)(Grad_Dbl_Coord);

            Min_Coord = (int)((module(Grad_Dbl_Coord) - module(Grad_I_Coord)) * 60);

            Sec_Coord = ((module(Grad_Dbl_Coord) - module(Grad_I_Coord)) * 60 - Min_Coord) * 60;

        } // Функция f_Grad_GMS
        //**************************************************************************************

        //**************************************************************************************
        // Преобразование DD MM SS (градусы(целое значение), минуты(целое значение),
        // секунды(дробное значение))-> dd.ddddd (дробное значение градусов) 

        // Входные параметры:
        // Grad_I_Coord - градусы (целое)
        // Min_Coord - минуты (целое)
        // Sec_Coord - секунды (дробное)

        // Выходные параметры:
        // Grad_Dbl_Coord - градусы в дробном виде
        //**************************************************************************************
        public static void f_GMS_Grad
            (

                // Входные параметры 
                int Grad_I_Coord,
                int Min_Coord,
                double Sec_Coord,

                // Выходные параметры (grad)
                ref double Grad_Dbl_Coord

            )
        {

            double mm_tmp, ss_tmp;

            mm_tmp = ((double)(Min_Coord)) / 60;
            ss_tmp = Sec_Coord / 3600;


            if (Grad_I_Coord < 0)
            {
                Grad_Dbl_Coord = -ss_tmp - mm_tmp + Grad_I_Coord;
            }

            else
            {
                Grad_Dbl_Coord = Grad_I_Coord + mm_tmp + ss_tmp;
            }

        } // Функция f_GMS_Grad
        //**************************************************************************************

        //**************************************************************************************
        // Преобразование dd.ddddd (grad) -> DD MM 
        // (дробное значение градусов -> в градусы(целое значение), минуты(дробное значение),

        // Входные параметры:
        // Grad_Dbl_Coord - градусы в дробном виде

        // Выходные параметры:
        // Grad_I_Coord - градусы (целое)
        // Min_Coord - минуты (дробное)
        //**************************************************************************************
        public static void f_Grad_GM
            (

                // Входные параметры (grad)
                double Grad_Dbl_Coord,

                // Выходные параметры 
                ref int Grad_I_Coord,
                ref double Min_Coord

            )
        {

            Grad_I_Coord = (int)(Grad_Dbl_Coord);

            Min_Coord = (module(Grad_Dbl_Coord) - module(Grad_I_Coord)) * 60;

        } // Функция f_Grad_GM
        //**************************************************************************************

        //**************************************************************************************
        // Преобразование DD MM  (градусы(целое значение), минуты(дробное значение)) ->
        // dd.ddddd (дробное значение градусов) 

        // Входные параметры:
        // Grad_I_Coord - градусы (целое)
        // Min_Coord - минуты (дробное)

        // Выходные параметры:
        // Grad_Dbl_Coord - градусы в дробном виде
        //**************************************************************************************
        public static void f_GM_Grad
            (

                // Входные параметры 
                int Grad_I_Coord,
                double Min_Coord,

                // Выходные параметры (grad)
                ref double Grad_Dbl_Coord

            )
        {

            double mm_tmp;

            mm_tmp = Min_Coord / 60;


            if (Grad_I_Coord < 0)
            {
                Grad_Dbl_Coord = -mm_tmp + Grad_I_Coord;
            }

            else
            {
                Grad_Dbl_Coord = Grad_I_Coord + mm_tmp;
            }

        } // Функция f_GM_Grad
        //**************************************************************************************

        //**************************************************************************************
        // Преобразование DD MM  (градусы(целое значение), минуты(дробное значение))->
        // DD MM SS (градусы(целое значение), минуты(целое значение), секунды (дробное значение)) 

        // Входные параметры:
        // Grad_I_Coord - градусы (целое)
        // Min_Coord - минуты (дробное)

        // Выходные параметры:
        // Grad_I_Coord - градусы (целое)
        // Min_Coord - минуты (целое)
        // Sec_Coord - секунды (дробное)
        //**************************************************************************************
        public static void f_GM_GMS
            (

                // Входные параметры 
                int Grad_I_Coord,
                double Min_Coord,

                // Выходные параметры (grad)
                ref int Grad_I_Coord_Out,
                ref int Min_Coord_Out,
                ref double Sec_Coord

            )
        {

            double mm_tmp = 0;
            double grad = 0;

            // .........................................................................
            // GG MM->dd.ddddd

            mm_tmp = Min_Coord / 60;

            if (Grad_I_Coord < 0)
            {
                grad = -mm_tmp + Grad_I_Coord;
            }

            else
            {
                grad = Grad_I_Coord + mm_tmp;
            }
            // .........................................................................
            // dd.ddddd -> DD MM SS


            f_Grad_GMS
              (

                // Входные параметры (grad)
                grad,

                // Выходные параметры 
                ref Grad_I_Coord_Out,
                ref Min_Coord_Out,
                ref Sec_Coord

              );
            // .........................................................................


        } // Функция f_GM_GMS
        //**************************************************************************************

        //**************************************************************************************
        // Преобразование DD MM SS (градусы(целое значение), минуты(целое значение),
        // секунды(дробное значение))-> DD MM (градусы(целое значение), минуты(дробное значение)) 

        // Входные параметры:
        // Grad_I_Coord - градусы (целое)
        // Min_Coord - минуты (целое)
        // Sec_Coord - секунды (дробное)

        // Выходные параметры:
        // Grad__Coord - градусы (целое)

        //**************************************************************************************
        public static void f_GMS_GM
            (

                // Входные параметры 
                int Grad_I_Coord,
                int Min_Coord,
                double Sec_Coord,

                // Выходные параметры (grad)
                ref int Grad_Coord_Out,
                ref double Min_Coord_Out


            )
        {

            double grad = 0;
            // ...................................................................................
            // GG MM SS -> dd.ddddd

            f_GMS_Grad
                (

                    // Входные параметры 
                    Grad_I_Coord,
                    Min_Coord,
                    Sec_Coord,

                    // Выходные параметры (grad)
                    ref grad

                );
            // ...................................................................................
            // dd.ddddd->GG MM

            f_Grad_GM
                (

                    // Входные параметры (grad)
                    grad,

                    // Выходные параметры 
                    ref Grad_Coord_Out,
                    ref Min_Coord_Out

                );
            // ...................................................................................

        } // Функция f_GMS_GM
        //**************************************************************************************

        //**************************************************************************************
        //Преобразование геодезических координат на эллипсоиде WGS84  в формат UTM 
        // (универсальная поперечная проекция Меркатора)

        // Входные параметры:
        // Lat - широта (град)
        // Long - долгота (град)

        // Выходные параметры (m):
        // X
        // Y
        //**************************************************************************************
        public static void f_WGS84_Mercator
               (
            // Входные параметры (grad)
                   double Lat,   // широта
                   double Long,  // долгота

                   // Выходные параметры (m)
                   ref double X,
                   ref double Y

               )
        {

            double rLat = 0;
            double rLong = 0;
            double a = 0;
            double b = 0;
            double f = 0;
            double e = 0;

            // Проверка на некорректный ввод ******************************************
            int fval = 0;

            fval = f_validate(ref Lat, ref Long);
            if (fval == -1)
            {
                MessageBox.Show("Incorrect input of latitude and longitude (latitude: -90...90  longitude: -180...180) 13");
                return;
            }
            // ****************************************** Проверка на некорректный ввод

            // .........................................................................
            // Большая полуось эллипсоида WGS84
            a = 6378137;
            // Малая полуось эллипсоида WGS84
            b = 6356752.3142;
            // .........................................................................
            // degree -> rad

            rLat = Lat * Math.PI / 180;
            rLong = Long * Math.PI / 180;
            // .........................................................................
            f = (a - b) / a;
            e = Math.Sqrt(2 * f - f * f);
            // .........................................................................
            X = a * rLong;
            // .........................................................................
            Y = a * Math.Log(Math.Tan(Math.PI / 4 + rLat / 2) * Math.Pow((1 - e * Math.Sin(rLat)) / (1 + e * Math.Sin(rLat)), e / 2));
            // .........................................................................


        } // Функция f_WGS84_Mercator
        //**************************************************************************************

        //**************************************************************************************
        // Преобразование прямоугольных координат универсальной поперечной проекции Меркатора (XY) 
        // в геодезические координаты на эллипсоиде WGS84 (BL)

        // Входные параметры (m):
        // X,
        // Y

        // Выходные параметры (m):
        // Lat - широта (град)
        // Long - долгота (град)
        //**************************************************************************************
        public static void f_Mercator_WGS84
               (
            // Входные параметры (m)
                   double X,
                   double Y,

                   // Выходные параметры (degree)
                   ref double Lat,   // широта
                   ref double Long  // долгота

               )
        {

            double rLat = 0;
            double rLong = 0;
            double a = 0;
            double b = 0;

            double ratio = 0;
            double eccent = 0;
            double com = 0;
            double ts = 0;
            double con = 0;
            double phi = 0;
            double dphi = 0;

            int i = 0;
            // .........................................................................
            // Большая полуось эллипсоида WGS84
            a = 6378137;
            // Малая полуось эллипсоида WGS84
            b = 6356752.3142;
            // .........................................................................
            ratio = b / a;
            eccent = Math.Sqrt(1 - ratio * ratio);
            com = 0.5 * eccent;

            ts = Math.Exp(-Y / a);
            phi = Math.PI / 2 - 2 * Math.Atan(ts);
            dphi = 1;
            // .........................................................................
            while (
                  (Math.Abs(dphi) > 0.000000001) &&
                  (i < 15)
                 )
            {
                i += 1;

                con = eccent * Math.Sin(phi);
                dphi = Math.PI / 2 - 2 * Math.Atan(ts * Math.Pow((1 - con) / (1 + con), com)) - phi;
                phi += dphi;


            };  // WHILE
            // .........................................................................
            rLong = X / a; //rad
            // .........................................................................
            rLat = phi; // rad
            // .........................................................................
            // rad -> degree 

            Lat = rLat * 180 / Math.PI;
            Long = rLong * 180 / Math.PI;


        } // Функция f_Mercator_WGS84
        //**************************************************************************************

        //**************************************************************************************
        //Преобразование координат в формате UTM в геодезические координаты на эллипсоиде WGS84 (BL)

        // Входные параметры:
        // !!! utmZone - название зоны
        // X-восточное склонение (проекционное расстояние от центрального меридиана зоны долготы)
        // Y - северное склонение (проекционное расстояние от экватора)

        // Пример1:
        // utmZone="35V"
        // X=414668
        // Y=6812844
        // Соответствует 
        // Lat=61.44
        // Long=25.40

        // Пример2:
        // utmZone="18G"
        // X=615471
        // Y=4789269
        // Соответствует 
        // Lat=-47.04
        // Long=-73.48

        // Выходные параметры (degree)
        // Lat,   // широта
        // Long  // долгота

        //**************************************************************************************

        public static void f_UTM_WGS84(
            // Входные параметры
                               double utmX, // m
                               double utmY, // m
                               string utmZone,

                               // Выходные параметры
                               ref double Lat,
                               ref double Long
                              )
        {
            double latitude = 0;
            double longitude = 0;

            //***
            bool isNorthHemisphere = utmZone.Last() >= 'N';

            var diflat = -0.00066286966871111111111111111111111111;
            var diflon = -0.0003868060578;

            var zone = int.Parse(utmZone.Remove(utmZone.Length - 1));
            var c_sa = 6378137.000000;
            var c_sb = 6356752.314245;
            var e2 = Math.Pow((Math.Pow(c_sa, 2) - Math.Pow(c_sb, 2)), 0.5) / c_sb;
            var e2cuadrada = Math.Pow(e2, 2);
            var c = Math.Pow(c_sa, 2) / c_sb;
            var x = utmX - 500000;

            //***
            var y = isNorthHemisphere ? utmY : utmY - 10000000;
            //var y = utmY;

            var s = ((zone * 6.0) - 183.0);
            var lat = y / (6366197.724 * 0.9996); // Change c_sa for 6366197.724
            var v = (c / Math.Pow(1 + (e2cuadrada * Math.Pow(Math.Cos(lat), 2)), 0.5)) * 0.9996;
            var a = x / v;
            var a1 = Math.Sin(2 * lat);
            var a2 = a1 * Math.Pow((Math.Cos(lat)), 2);
            var j2 = lat + (a1 / 2.0);
            var j4 = ((3 * j2) + a2) / 4.0;
            var j6 = (5 * j4 + a2 * Math.Pow((Math.Cos(lat)), 2)) / 3.0; // saque a2 de multiplicar por el coseno de lat y elevar al cuadrado
            var alfa = (3.0 / 4.0) * e2cuadrada;
            var beta = (5.0 / 3.0) * Math.Pow(alfa, 2);
            var gama = (35.0 / 27.0) * Math.Pow(alfa, 3);
            var bm = 0.9996 * c * (lat - alfa * j2 + beta * j4 - gama * j6);
            var b = (y - bm) / v;
            var epsi = ((e2cuadrada * Math.Pow(a, 2)) / 2.0) * Math.Pow((Math.Cos(lat)), 2);
            var eps = a * (1 - (epsi / 3.0));
            var nab = (b * (1 - epsi)) + lat;
            var senoheps = (Math.Exp(eps) - Math.Exp(-eps)) / 2.0;
            var delt = Math.Atan(senoheps / (Math.Cos(nab)));
            var tao = Math.Atan(Math.Cos(delt) * Math.Tan(nab));

            longitude = (delt / Math.PI) * 180 + s;
            latitude = (((lat + (1 + e2cuadrada * Math.Pow(Math.Cos(lat), 2) - (3.0 / 2.0) * e2cuadrada * Math.Sin(lat) * Math.Cos(lat) * (tao - lat)) * (tao - lat))) / Math.PI) * 180; // era incorrecto el calculo

            //Console.WriteLine("Latitud: " + latitude.ToString() + "\nLongitud: " + longitude.ToString());


            //s1 = latitude.ToString();
            //s2 = longitude.ToString();
            Lat = latitude;
            Long = longitude;

        } // f_UTM_WGS84
        //**************************************************************************************

        //**************************************************************************************
        // Преобразование геодезических координат на эллипсоиде WGS84 (BL) в координаты в формате UTM :
        // - зона долготы (цифра 1...60)
        // - зона широты (буква)
        // -восточное склонение (проекционное расстояние от центрального меридиана зоны долготы в м)
        // - северное склонение (проекционное расстояние от экватора в м)

        // Пример1:
        // Lat=61.44
        // Long=25.40

        // Соответствует 
        // "35V  414668  6812844"

        // Пример2:
        // Lat=-47.04
        // Long=-73.48

        // Соответствует 
        // "18G  615471  4789269"

        //**************************************************************************************

        public static String f_WGS84_UTM(
            // Входные параметры
                                double latitude,
                                double longitude
                              )
        {
            String UTM = "";
            double lat = 0;
            double lon = 0;

            double a = 0;
            double b = 0;

            // eccentricity
            double e = 0;

            double rho = 0;
            double nu = 0;
            double p = 0;
            double S = 0;
            double K1 = 0;
            double K2 = 0;
            double K3 = 0;
            double K4 = 0;
            double K5 = 0;
            double A6 = 0;

            double A0 = 6367449.146;

            double B0 = 0;
            double C0 = 0;
            double D0 = 0;
            double E0 = 0;

            // scale factor
            double k0 = 0;

            double sin1 = 0;
            double e1sq = 0;

            char[] posLetters = { 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
                                 'X', 'Z' };

            int[] posDegrees = { 0, 8, 16, 24, 32, 40, 48, 56, 64, 72, 84 };

            char[] negLetters = { 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K',
                                          'L', 'M' };

            int[] negDegrees = { -90, -84, -72, -64, -56, -48, -40, -32, -24,
                                         -16, -8 };

            // Проверка на некорректный ввод ******************************************
            int fval = 0;

            fval = f_validate(ref latitude, ref longitude);
            if (fval == -1)
            {
                MessageBox.Show("Incorrect input of latitude and longitude (latitude: -90...90  longitude: -180...180 14)");
                return "";
            }
            // ****************************************** Проверка на некорректный ввод

            // .........................................................................
            // Большая полуось эллипсоида WGS84
            a = 6378137;
            // Малая полуось эллипсоида WGS84
            b = 6356752.3142;
            // eccentricity
            e = Math.Sqrt(1 - Math.Pow(b / a, 2));

            e1sq = e * e / (1 - e * e);

            A0 = 6367449.146;
            B0 = 16038.42955;
            C0 = 16.83261333;
            D0 = 0.021984404;
            E0 = 0.000312705;

            // scale factor
            k0 = 0.9996;

            sin1 = 4.84814E-06;

            // .........................................................................
            //setVariables(latitude, longitude);

            // degree -> rad
            lat = latitude * Math.PI / 180;

            rho = a * (1 - e * e) / Math.Pow(1 - Math.Pow(e * Math.Sin(lat), 2), 3 / 2.0);

            nu = a / Math.Pow(1 - Math.Pow(e * Math.Sin(lat), 2), (1 / 2.0));

            double var1;
            if (longitude < 0.0)
            {
                var1 = ((int)((180 + longitude) / 6.0)) + 1;
            }
            else
            {
                var1 = ((int)(longitude / 6)) + 31;
            }

            double var2 = (6 * var1) - 183;
            double var3 = longitude - var2;

            p = var3 * 3600 / 10000;

            S = A0 * lat - B0 * Math.Sin(2 * lat) + C0 * Math.Sin(4 * lat) -
                D0 * Math.Sin(6 * lat) + E0 * Math.Sin(8 * lat);

            K1 = S * k0;

            K2 = nu * Math.Sin(lat) * Math.Cos(lat) * Math.Pow(sin1, 2) * k0 * (100000000) / 2;

            K3 = ((Math.Pow(sin1, 4) * nu * Math.Sin(lat) * Math.Pow(Math.Cos(lat), 3)) / 24)
                * (5 - Math.Pow(Math.Tan(lat), 2) + 9 * e1sq * Math.Pow(Math.Cos(lat), 2) + 4
                    * Math.Pow(e1sq, 2) * Math.Pow(Math.Cos(lat), 4))
                * k0
                * (10000000000000000L);

            K4 = nu * Math.Cos(lat) * sin1 * k0 * 10000;

            K5 = Math.Pow(sin1 * Math.Cos(lat), 3) * (nu / 6)
                * (1 - Math.Pow(Math.Tan(lat), 2) + e1sq * Math.Pow(Math.Cos(lat), 2)) * k0
                * 1000000000000L;

            A6 = (Math.Pow(p * sin1, 6) * nu * Math.Sin(lat) * Math.Pow(Math.Cos(lat), 5) / 720)
                * (61 - 58 * Math.Pow(Math.Tan(lat), 2) + Math.Pow(Math.Tan(lat), 4) + 270
                    * e1sq * Math.Pow(Math.Cos(lat), 2) - 330 * e1sq
                    * Math.Pow(Math.Sin(lat), 2)) * k0 * (1E+24);

            // .................................................................................

            //String longZone = getLongZone(longitude);

            String longZone = "";

            double dlongZone = 0;

            if (longitude < 0.0)
            {
                dlongZone = ((180.0 + longitude) / 6) + 1;
            }
            else
            {
                dlongZone = (longitude / 6) + 31;
            }

            //String val = String.valueOf((int)dlongZone);
            longZone = ((int)dlongZone).ToString();

            if (longZone.Length == 1)
            {
                longZone = "0" + longZone;
            }

            // .................................................................................

            //LatZones latZones = new LatZones();
            //String latZone = latZones.getLatZone(latitude);

            String latZone = "";

            int latIndex = -2;
            int lat1 = (int)latitude;

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if (lat1 >= 0)
            {
                int len = posLetters.Length;
                for (int i = 0; i < len; i++)
                {
                    if (lat1 == posDegrees[i])
                    {
                        latIndex = i;
                        break;
                    }

                    if (lat1 > posDegrees[i])
                    {
                        continue;
                    }
                    else
                    {
                        latIndex = i - 1;
                        break;
                    }
                } // for
            } // if (lat1 >= 0)


            else
            {
                int len1 = negLetters.Length;
                for (int i1 = 0; i1 < len1; i1++)
                {
                    if (lat1 == negDegrees[i1])
                    {
                        latIndex = i1;
                        break;
                    }

                    if (lat1 < negDegrees[i1])
                    {
                        latIndex = i1 - 1;
                        break;
                    }
                    else
                    {
                        continue;
                    }

                }

            } // else
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if (latIndex == -1)
            {
                latIndex = 0;
            }

            if (lat1 >= 0)
            {
                if (latIndex == -2)
                {
                    latIndex = posLetters.Length - 1;
                }

                //return String.valueOf(posLetters[latIndex]);
                latZone = posLetters[latIndex].ToString();
            }
            else
            {
                if (latIndex == -2)
                {
                    latIndex = negLetters.Length - 1;
                }
                //return String.valueOf(negLetters[latIndex]);
                latZone = negLetters[latIndex].ToString();

            } // else

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // .................................................................................
            //double _easting = getEasting();

            double _easting = 0;

            _easting = 500000 + (K4 * p + K5 * Math.Pow(p, 3));
            // .................................................................................
            //double _northing = getNorthing(latitude);

            double _northing = 0;

            _northing = K1 + K2 * p * p + K3 * Math.Pow(p, 4);
            if (latitude < 0.0)
            {
                _northing = 10000000 + _northing;
            }

            // .................................................................................

            UTM = longZone + " " + latZone + " " + ((int)_easting) + " "
                + ((int)_northing);
            // UTM = longZone + " " + latZone + " " + decimalFormat.format(_easting) +
            // " "+ decimalFormat.format(_northing);

            return UTM;

        } // f_WGS84_UTM
        //**************************************************************************************

        //**************************************************************************************
        //Преобразование координат в формате MGRS в геодезические координаты на эллипсоиде WGS84 (BL)

        // Входные параметры:
        // sMGRS - строка координат в формате MGRS:
        // - зона долготы (две цифры)
        // - зона широты (буква)
        // - восточно-западная позиция квадрата 100x100км в зоне долготы (буква)
        // - северно-южная позиция (буква)
        // - значения восточного и северного склонений (10 символов для точности 1 м)


        // Пример:
        // MGRS: "35VMJ1466812844"

        // Соответствует:
        // UTM: "35V 414668 6812844"
        // Lat=61.44 Long=25.40 degree


        // Выходные параметры (degree)
        // Lat,   // широта
        // Long  // долгота

        //**************************************************************************************

        public static void f_MGRS_WGS84(
            // Входные параметры
                               String sMGRS,

                               // Выходные параметры (degree)
                               ref double Lat,
                               ref double Long
                              )
        {
            // ..................................................................
            double easting = 0;
            double northing = 0;
            int arrayLength = 0;
            double zoneCM = 0;

            double b = 0;
            double a = 0;
            double e = 0;
            double e1sq = 0;
            double k0 = 0;

            // ..................................................................
            double arc = 0;
            double mu = 0;
            double ei = 0;
            double ca = 0;
            double cb = 0;
            double cc = 0;
            double cd = 0;
            double n0 = 0;
            double r0 = 0;
            double _a1 = 0;
            double dd0 = 0;
            double t0 = 0;
            double Q0 = 0;
            double lof1 = 0;
            double lof2 = 0;
            double lof3 = 0;
            double _a2 = 0;
            double phi1 = 0;
            double fact1 = 0;
            double fact2 = 0;
            double fact3 = 0;
            double fact4 = 0;
            double _a3 = 0;
            // ..................................................................
            char[] letters = { 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K',
                 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Z' };

            int[] degrees = { -90, -84, -72, -64, -56, -48, -40, -32, -24, -16,
                                     -8, 0, 8, 16, 24, 32, 40, 48, 56, 64, 72, 84 };

            String[] digraph2Array = { "V", "A", "B", "C", "D", "E", "F", "G",
                    "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V" };

            String[] digraph1Array = { "A", "B", "C", "D", "E", "F", "G", "H",
            "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X",
            "Y", "Z" };

            // ..................................................................
            arrayLength = 22;

            b = 6356752.314;

            a = 6378137;

            e = 0.081819191;

            e1sq = 0.006739497;

            k0 = 0.9996;
            // ..................................................................
            // 02CNR0634657742
            // !!! 2-й аргумент -длина
            int zone = Convert.ToInt32(sMGRS.Substring(0, 2));
            //String latZone = sMGRS.Substring(2, 3);
            String latZone = sMGRS.Substring(2, 1);

            //String digraph1 = sMGRS.Substring(3, 4);
            //String digraph2 = sMGRS.Substring(4, 5);
            String digraph1 = sMGRS.Substring(3, 1);
            String digraph2 = sMGRS.Substring(4, 1);

            //easting = Convert.ToDouble(sMGRS.Substring(5, 10));
            //northing = Convert.ToDouble(sMGRS.Substring(10, 15));
            easting = Convert.ToDouble(sMGRS.Substring(5, 5));
            northing = Convert.ToDouble(sMGRS.Substring(10, 5));

            // ..................................................................

            //LatZones lz = new LatZones();
            //double latZoneDegree = lz.getLatZoneDegree(latZone);

            double latZoneDegree = 0;

            char[] arr;
            arr = latZone.ToCharArray(0, 1);

            char ltr = arr[0];

            int fff = 0;
            for (int i = 0; i < arrayLength; i++)
            {
                if (letters[i] == ltr)
                {
                    latZoneDegree = (double)degrees[i];
                    fff = 1;
                }
            }
            if (fff == 0)
                latZoneDegree = -100;
            // ..................................................................
            double a1 = latZoneDegree * 40000000 / 360.0;
            double a2 = 2000000 * Math.Floor(a1 / 2000000.0);
            // ..................................................................
            //Digraphs digraphs = new Digraphs();
            //double digraph2Index = digraphs.getDigraph2Index(digraph2);

            double digraph2Index = 0;

            int fff1 = 0;
            for (int i1 = 0; i1 < digraph2Array.Length; i1++)
            {
                //if (digraph2Array[i1].equals(digraph2))
                if (String.Compare(digraph2Array[i1], digraph2) == 0)
                {
                    digraph2Index = (double)i1;
                    fff1 = 1;
                }
            }
            if (fff1 == 0)
                digraph2Index = -1;

            // ..................................................................
            double startindexEquator = 1;

            if ((1 + zone % 2) == 1)
            {
                startindexEquator = 6;
            }
            // ..................................................................
            double a3 = a2 + (digraph2Index - startindexEquator) * 100000;
            if (a3 <= 0)
            {
                a3 = 10000000 + a3;
            }
            northing = a3 + northing;
            // ..................................................................
            zoneCM = -183 + 6 * zone;
            // ..................................................................
            //double digraph1Index = digraphs.getDigraph1Index(digraph1);

            double digraph1Index = 0;

            int fff2 = 0;
            for (int i2 = 0; i2 < digraph1Array.Length; i2++)
            {
                if (String.Compare(digraph1Array[i2], digraph1) == 0)
                {
                    digraph1Index = (double)(i2 + 1);
                    fff2 = 1;
                }
            }

            if (fff2 == 0)
                digraph1Index = -1;

            // ..................................................................
            int a5 = 1 + zone % 3;
            double[] a6 = { 16, 0, 8 };
            double a7 = 100000 * (digraph1Index - a6[a5 - 1]);
            easting = easting + a7;
            // ..................................................................
            //setVariables();

            arc = northing / k0;

            mu = arc
                / (a * (1 - Math.Pow(e, 2) / 4.0 - 3 * Math.Pow(e, 4) / 64.0 - 5 * Math.Pow(e, 6) / 256.0));

            ei = (1 - Math.Pow((1 - e * e), (1 / 2.0)))
                / (1 + Math.Pow((1 - e * e), (1 / 2.0)));

            ca = 3 * ei / 2 - 27 * Math.Pow(ei, 3) / 32.0;

            cb = 21 * Math.Pow(ei, 2) / 16 - 55 * Math.Pow(ei, 4) / 32;

            cc = 151 * Math.Pow(ei, 3) / 96;

            cd = 1097 * Math.Pow(ei, 4) / 512;

            phi1 = mu + ca * Math.Sin(2 * mu) + cb * Math.Sin(4 * mu) + cc * Math.Sin(6 * mu) + cd
                * Math.Sin(8 * mu);

            n0 = a / Math.Pow((1 - Math.Pow((e * Math.Sin(phi1)), 2)), (1 / 2.0));

            r0 = a * (1 - e * e) / Math.Pow((1 - Math.Pow((e * Math.Sin(phi1)), 2)), (3 / 2.0));

            fact1 = n0 * Math.Tan(phi1) / r0;

            _a1 = 500000 - easting;

            dd0 = _a1 / (n0 * k0);

            fact2 = dd0 * dd0 / 2;

            t0 = Math.Pow(Math.Tan(phi1), 2);

            Q0 = e1sq * Math.Pow(Math.Cos(phi1), 2);

            fact3 = (5 + 3 * t0 + 10 * Q0 - 4 * Q0 * Q0 - 9 * e1sq) * Math.Pow(dd0, 4)
                / 24;

            fact4 = (61 + 90 * t0 + 298 * Q0 + 45 * t0 * t0 - 252 * e1sq - 3 * Q0
                * Q0)
                * Math.Pow(dd0, 6) / 720;

            //
            lof1 = _a1 / (n0 * k0);

            lof2 = (1 + 2 * t0 + Q0) * Math.Pow(dd0, 3) / 6.0;

            lof3 = (5 - 2 * Q0 + 28 * t0 - 3 * Math.Pow(Q0, 2) + 8 * e1sq + 24 * Math.Pow(t0, 2))
                * Math.Pow(dd0, 5) / 120;

            _a2 = (lof1 - lof2 + lof3) / Math.Cos(phi1);

            _a3 = _a2 * 180 / Math.PI;
            // ..................................................................
            // latitude, degree

            double latitude = 0;
            latitude = 180 * (phi1 - fact1 * (fact2 + fact3 + fact4)) / Math.PI;

            if (latZoneDegree < 0)
            {
                latitude = 90 - latitude;
            }
            // ..................................................................
            // longitude, degree

            double d = _a2 * 180 / Math.PI;
            double longitude = zoneCM - d;
            // ..................................................................

            //if (getHemisphere(latZone).equals("S"))
            //{
            //    latitude = -latitude;
            //}
            String southernHemisphere = "ACDEFGHJKLM";

            String tmps = "";

            String hemisphere = "N";
            if (southernHemisphere.IndexOf(latZone) > -1)
            {
                hemisphere = "S";
            }
            tmps = hemisphere;

            if (String.Compare(tmps, "S") == 0)
            {
                latitude = -latitude;
            }

            // ..................................................................
            Lat = latitude;
            Long = longitude;
            // ..................................................................


        } // f_MGRS_WGS84
        //**************************************************************************************

        //**************************************************************************************
        // Преобразование геодезических координаты на эллипсоиде WGS84 (BL) в координаты в 
        // формате MGRS  

        // Входные параметры (град)
        // latitude,   // широта
        // longitude  // долгота

        // Выходные параметры: строка координат в формате MGRS:
        // - зона долготы (две цифры)
        // - зона широты (буква)
        // - восточно-западная позиция квадрата 100x100км в зоне долготы (буква)
        // - северно-южная позиция (буква)
        // - значения восточного и северного склонений (10 символов для точности 1 м)

        // Пример:
        // Lat=61.44 Long=25.40 degree

        // Соответствует:
        // MGRS: "35V MJ 14668 12844"
        //**************************************************************************************

        public static String f_WGS84_MGRS(
            // Входные параметры (ddegree)
                               double latitude,
                               double longitude

                              )
        {

            // ..................................................................
            String mgrUTM = "";

            double lat = 0;
            double lon = 0;

            double a = 0;
            double b = 0;

            // eccentricity
            double e = 0;

            double rho = 0;
            double nu = 0;
            double p = 0;
            double S = 0;
            double K1 = 0;
            double K2 = 0;
            double K3 = 0;
            double K4 = 0;
            double K5 = 0;
            double A6 = 0;

            double A0 = 6367449.146;

            double B0 = 0;
            double C0 = 0;
            double D0 = 0;
            double E0 = 0;

            // scale factor
            double k0 = 0;

            double sin1 = 0;
            double e1sq = 0;

            char[] posLetters = { 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
                                 'X', 'Z' };

            int[] posDegrees = { 0, 8, 16, 24, 32, 40, 48, 56, 64, 72, 84 };

            char[] negLetters = { 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K',
                                          'L', 'M' };

            int[] negDegrees = { -90, -84, -72, -64, -56, -48, -40, -32, -24,
                                         -16, -8 };

            // ..................................................................

            int[] m1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };
            String[] m2 = { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

            int[] m3 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
            String[] m5 = { "V", "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V" };


            // Проверка на некорректный ввод ******************************************
            int fval = 0;

            fval = f_validate(ref latitude, ref longitude);
            if (fval == -1)
            {
                MessageBox.Show("Incorrect input of latitude and longitude (latitude: -90...90  longitude: -180...180) 15");
                return "";
            }
            // ****************************************** Проверка на некорректный ввод

            // ..................................................................
            // Большая полуось эллипсоида WGS84
            a = 6378137;
            // Малая полуось эллипсоида WGS84
            b = 6356752.3142;
            // eccentricity
            e = Math.Sqrt(1 - Math.Pow(b / a, 2));

            e1sq = e * e / (1 - e * e);

            A0 = 6367449.146;
            B0 = 16038.42955;
            C0 = 16.83261333;
            D0 = 0.021984404;
            E0 = 0.000312705;

            // scale factor
            k0 = 0.9996;

            sin1 = 4.84814E-06;

            // ..................................................................
            //setVariables(latitude, longitude);

            // degree -> rad
            lat = latitude * Math.PI / 180;

            rho = a * (1 - e * e) / Math.Pow(1 - Math.Pow(e * Math.Sin(lat), 2), 3 / 2.0);

            nu = a / Math.Pow(1 - Math.Pow(e * Math.Sin(lat), 2), (1 / 2.0));

            double var1;
            if (longitude < 0.0)
            {
                var1 = ((int)((180 + longitude) / 6.0)) + 1;
            }
            else
            {
                var1 = ((int)(longitude / 6)) + 31;
            }

            double var2 = (6 * var1) - 183;
            double var3 = longitude - var2;

            p = var3 * 3600 / 10000;

            S = A0 * lat - B0 * Math.Sin(2 * lat) + C0 * Math.Sin(4 * lat) -
                D0 * Math.Sin(6 * lat) + E0 * Math.Sin(8 * lat);

            K1 = S * k0;

            K2 = nu * Math.Sin(lat) * Math.Cos(lat) * Math.Pow(sin1, 2) * k0 * (100000000) / 2;

            K3 = ((Math.Pow(sin1, 4) * nu * Math.Sin(lat) * Math.Pow(Math.Cos(lat), 3)) / 24)
                * (5 - Math.Pow(Math.Tan(lat), 2) + 9 * e1sq * Math.Pow(Math.Cos(lat), 2) + 4
                    * Math.Pow(e1sq, 2) * Math.Pow(Math.Cos(lat), 4))
                * k0
                * (10000000000000000L);

            K4 = nu * Math.Cos(lat) * sin1 * k0 * 10000;

            K5 = Math.Pow(sin1 * Math.Cos(lat), 3) * (nu / 6)
                * (1 - Math.Pow(Math.Tan(lat), 2) + e1sq * Math.Pow(Math.Cos(lat), 2)) * k0
                * 1000000000000L;

            A6 = (Math.Pow(p * sin1, 6) * nu * Math.Sin(lat) * Math.Pow(Math.Cos(lat), 5) / 720)
                * (61 - 58 * Math.Pow(Math.Tan(lat), 2) + Math.Pow(Math.Tan(lat), 4) + 270
                    * e1sq * Math.Pow(Math.Cos(lat), 2) - 330 * e1sq
                    * Math.Pow(Math.Sin(lat), 2)) * k0 * (1E+24);

            // ..................................................................
            //String longZone = getLongZone(longitude);

            String longZone = "";

            double dlongZone = 0;

            if (longitude < 0.0)
            {
                dlongZone = ((180.0 + longitude) / 6) + 1;
            }
            else
            {
                dlongZone = (longitude / 6) + 31;
            }

            //String val = String.valueOf((int)dlongZone);
            longZone = ((int)dlongZone).ToString();

            if (longZone.Length == 1)
            {
                longZone = "0" + longZone;
            }

            // ..................................................................
            //LatZones latZones = new LatZones();
            //String latZone = latZones.getLatZone(latitude);

            String latZone = "";

            int latIndex = -2;
            int lat1 = (int)latitude;

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if (lat1 >= 0)
            {
                int len = posLetters.Length;
                for (int i = 0; i < len; i++)
                {
                    if (lat1 == posDegrees[i])
                    {
                        latIndex = i;
                        break;
                    }

                    if (lat1 > posDegrees[i])
                    {
                        continue;
                    }
                    else
                    {
                        latIndex = i - 1;
                        break;
                    }
                } // for
            } // if (lat1 >= 0)


            else
            {
                int len1 = negLetters.Length;
                for (int i1 = 0; i1 < len1; i1++)
                {
                    if (lat1 == negDegrees[i1])
                    {
                        latIndex = i1;
                        break;
                    }

                    if (lat1 < negDegrees[i1])
                    {
                        latIndex = i1 - 1;
                        break;
                    }
                    else
                    {
                        continue;
                    }

                }

            } // else
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if (latIndex == -1)
            {
                latIndex = 0;
            }

            if (lat1 >= 0)
            {
                if (latIndex == -2)
                {
                    latIndex = posLetters.Length - 1;
                }

                //return String.valueOf(posLetters[latIndex]);
                latZone = posLetters[latIndex].ToString();
            }
            else
            {
                if (latIndex == -2)
                {
                    latIndex = negLetters.Length - 1;
                }
                //return String.valueOf(negLetters[latIndex]);
                latZone = negLetters[latIndex].ToString();

            } // else

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // ..................................................................
            //double _easting = getEasting();

            double _easting = 0;

            _easting = 500000 + (K4 * p + K5 * Math.Pow(p, 3));
            // ..................................................................
            //double _northing = getNorthing(latitude);

            double _northing = 0;

            _northing = K1 + K2 * p * p + K3 * Math.Pow(p, 4);
            if (latitude < 0.0)
            {
                _northing = 10000000 + _northing;
            }

            // ..................................................................
            //Digraphs digraphs = new Digraphs();
            //String digraph1 = digraphs.getDigraph1(Integer.parseInt(longZone),
            //_easting);
            //String digraph2 = digraphs.getDigraph2(Integer.parseInt(longZone),
            //_northing);

            String digraph1 = "";
            int longZone1 = 0;

            longZone1 = Convert.ToInt32(longZone);

            int a1 = longZone1;
            double a2 = 8 * ((a1 - 1) % 3) + 1;

            double a3 = _easting;
            double a4 = a2 + ((int)(a3 / 100000)) - 1;

            int if1 = (int)Math.Floor(a4);
            //return (String) digraph1.get(new Integer((int) Math.floor(a4)));
            //digraph1 = Convert.ToString(if1);

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            int indff1 = 0;
            int flf1 = 0;
            for (int iff1 = 0; iff1 < 24; iff1++)
            {
                if (m1[iff1] == if1)
                {
                    indff1 = iff1;
                    flf1 = 1;
                    break;
                }
            }

            if (flf1 == 1)
                digraph1 = m2[indff1];
            else
                digraph1 = "#";

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // ..................................................................
            String digraph2 = "";

            int a1_1 = longZone1;
            double a2_1 = 1 + 5 * ((a1_1 - 1) % 2);
            double a3_1 = _northing;
            double a4_1 = (a2_1 + ((int)(a3_1 / 100000)));
            a4_1 = (a2_1 + ((int)(a3_1 / 100000.0))) % 20;
            a4_1 = Math.Floor(a4_1);
            if (a4_1 < 0)
            {
                a4_1 = a4_1 + 19;
            }

            //return (String) digraph2.get(new Integer((int) Math.floor(a4)));
            int if2 = (int)Math.Floor(a4_1);
            //digraph2 = Convert.ToString(if2);

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            int indff2 = 0;
            int flf2 = 0;
            for (int iff2 = 0; iff2 <= 20; iff2++)
            {
                if (m3[iff2] == if2)
                {
                    indff2 = iff2;
                    flf2 = 1;
                    break;
                }
            }

            if (flf2 == 1)
                digraph2 = m5[indff2];
            else
                digraph1 = "#";

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // ..................................................................
            //String easting = String.valueOf((int) _easting);
            String easting = "";
            easting = ((int)_easting).ToString();

            if (easting.Length < 5)
            {
                easting = "00000" + easting;
            }
            easting = easting.Substring(easting.Length - 5);

            // ..................................................................
            String northing = "";
            //northing = String.valueOf((int) _northing);
            northing = ((int)_northing).ToString();

            if (northing.Length < 5)
            {
                northing = "0000" + northing;
            }
            northing = northing.Substring(northing.Length - 5);

            // ..................................................................
            mgrUTM = longZone + latZone + " " + digraph1 + digraph2 + " " + easting + " " + northing;

            // ..................................................................

            return mgrUTM;



        } // f_WGS84_MGRS
        //**************************************************************************************







        //**************************************************************************************

        // !!! Тестирование LatLong -> MGRS

        //lat = 61.44; // "35VMJ1466812844"
        //lon = 25.40;
        //lat = 51.172; //"30UWB8535869660"
        //lon = -1.779;
        //lat = 23.3663169379037; //"39Q XF 74256 85100"
        //lon = 52.7048315965327;
        //**************************************************************************************








        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!






        /*
        unction MGRSString (Lat, Long)
        { 
        if (Lat < -80) return 'Too far South' ; if (Lat > 84) return 'Too far North' ;
        var c = 1 + Math.floor ((Long+180)/6);
        var e = c*6 - 183 ;
        var k = Lat*Math.PI/180;
        var l = Long*Math.PI/180;
        var m = e*Math.PI/180;
        var n = Math.cos (k);
        var o = 0.006739496819936062*Math.pow (n,2);
        var p = 40680631590769/(6356752.314*Math.sqrt(1 + o));
        var q = Math.tan (k);
        var r = q*q;
        var s = (r*r*r) - Math.pow (q,6);
        var t = l - m;
        var u = 1.0 - r + o;
        var v = 5.0 - r + 9*o + 4.0*(o*o);
        var w = 5.0 - 18.0*r + (r*r) + 14.0*o - 58.0*r*o;
        var x = 61.0 - 58.0*r + (r*r) + 270.0*o - 330.0*r*o;
        var y = 61.0 - 479.0*r + 179.0*(r*r) - (r*r*r);
        var z = 1385.0 - 3111.0*r + 543.0*(r*r) - (r*r*r);
        var aa = p*n*t + (p/6.0*Math.pow (n,3)*u*Math.pow (t,3)) + (p/120.0*Math.pow (n,5)*w*Math.pow (t,5)) + (p/5040.0*Math.pow (n,7)*y*Math.pow (t,7));
        var ab = 6367449.14570093*(k - (0.00251882794504*Math.sin (2*k)) + (0.00000264354112*Math.sin (4*k)) - (0.00000000345262*Math.sin (6*k)) + (0.000000000004892*Math.sin (8*k))) + (q/2.0*p*Math.pow (n,2)*Math.pow (t,2)) + (q/24.0*p*Math.pow (n,4)*v*Math.pow (t,4)) + (q/720.0*p*Math.pow (n,6)*x*Math.pow (t,6)) + (q/40320.0*p*Math.pow (n,8)*z*Math.pow (t,8));
        aa = aa*0.9996 + 500000.0;
        ab = ab*0.9996; if (ab < 0.0) ab += 10000000.0;
        var ad = 'CDEFGHJKLMNPQRSTUVWXX'.charAt (Math.floor (Lat/8 + 10));
        var ae = Math.floor (aa/100000);
        var af = ['ABCDEFGH','JKLMNPQR','STUVWXYZ'][(c-1)%3].charAt (ae-1);
        var ag = Math.floor (ab/100000)%20;
        var ah = ['ABCDEFGHJKLMNPQRSTUV','FGHJKLMNPQRSTUVABCDE'][(c-1)%2].charAt (ag);
        function pad (val) {if (val < 10) {val = '0000' + val} else if (val < 100) {val = '000' + val} else if (val < 1000) {val = '00' + val} else if (val < 10000) {val = '0' + val};return val};
        aa = Math.floor (aa%100000); aa = pad (aa);
        ab = Math.floor (ab%100000); ab = pad (ab);
        return c + ad + ' ' + af + ah + ' ' + aa + ' ' + ab;
        };

        */




    }  // Class
}
